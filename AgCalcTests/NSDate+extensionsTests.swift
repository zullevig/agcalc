//
//  NSDate+extensionsTests.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 7/12/15.
//  Copyright © 2015 Zachary Ullevig. All rights reserved.
//

import XCTest

@testable import AgCalc


class NSDate_extensionsTests: XCTestCase {
    
    let kitcoDateStrings:(good:String,bad:String) = ("Jul 10, 2015 at 17:15.04", "Jul 10,2015 at 17:15.04")
    let londonFixDateStrings:(good:String,bad:String) = ("Jul 10,2015", "7 10, 2015")
    let timestampDateStrings:(good:String,bad:String) = ("5:27PM 07/12/15", "5:27 07/12/15")

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testKitcoDateString() {
        let now = NSDate()
        var parsedDate = NSDate(kitcoDateString:kitcoDateStrings.good)
        XCTAssert(parsedDate.timeIntervalSinceReferenceDate > 0)
        XCTAssert(parsedDate.timeIntervalSinceReferenceDate < now.timeIntervalSinceReferenceDate)
        
        parsedDate = NSDate(kitcoDateString:kitcoDateStrings.bad)
        XCTAssert(parsedDate.timeIntervalSinceReferenceDate == 0)
    }

    func testLondonFixDateString() {
        let now = NSDate()
        var parsedDate = NSDate(londonFixDateString:londonFixDateStrings.good)
        XCTAssert(parsedDate.timeIntervalSinceReferenceDate > 0)
        XCTAssert(parsedDate.timeIntervalSinceReferenceDate < now.timeIntervalSinceReferenceDate)
        
        parsedDate = NSDate(londonFixDateString:londonFixDateStrings.bad)
        XCTAssert(parsedDate.timeIntervalSinceReferenceDate == 0)
    }

    func testTimestampDateString() {
        let now = NSDate()
        var parsedDate = NSDate(timestampDateString:timestampDateStrings.good)
        XCTAssert(parsedDate.timeIntervalSinceReferenceDate > 0)
        XCTAssert(parsedDate.timeIntervalSinceReferenceDate < now.timeIntervalSinceReferenceDate)
        
        parsedDate = NSDate(timestampDateString:timestampDateStrings.bad)
        XCTAssert(parsedDate.timeIntervalSinceReferenceDate == 0)
    }
}
