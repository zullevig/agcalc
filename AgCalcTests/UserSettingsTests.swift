//
//  UserSettingsTests.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 7/12/15.
//  Copyright © 2015 Zachary Ullevig. All rights reserved.
//

import XCTest

@testable import AgCalc


class UserSettingsTests: XCTestCase {
    let silverRange:(min:Float, max:Float) = (4, 100)
    let goldRange:(min:Float, max:Float) = (500, 3000)
    let platinumRange:(min:Float, max:Float) = (500, 3000)
    let palladiumRange:(min:Float, max:Float) = (250, 1500)
    
    let manualSilverPrice:Float = 42
    let manualGoldPrice:Float = 1042
    let manualPlatinumPrice:Float = 1142
    let manualPalladiumPrice:Float = 742
    
    // load a test coin from core data when first requested
    lazy var coin:Coin? = {
        appDelegate.reloadCoinData()
        let coin:Coin? = appDelegate.coin(1)
        return coin
    } ()
    
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testUserSettingsSingleton() {
        let settings = UserSettings.sharedInstance;
        settings.reset()
        
        // check existence
        XCTAssertNotNil(settings)
        
        // check for empty NSUserDefaults
        var setting = NSUserDefaults.standardUserDefaults().objectForKey(AgEnumSetting.PriceUpdateMode.rawValue) as? String
        XCTAssert(setting == nil, "Expected nil instead of: \(setting)")

        setting = NSUserDefaults.standardUserDefaults().objectForKey(AgEnumSetting.MediumOfExchange.rawValue) as? String
        XCTAssert(setting == nil, "Expected nil instead of: \(setting)")

        setting = NSUserDefaults.standardUserDefaults().objectForKey(AgEnumSetting.UnitOfMass.rawValue) as? String
        XCTAssert(setting == nil, "Expected nil instead of: \(setting)")

        // check expected defaults
        XCTAssertEqual(settings.priceUpdateMode, PriceUpdateMode.SpotBid)
        XCTAssertEqual(settings.mediumOfExchange, MediumOfExchange.USDollar)
        XCTAssertEqual(settings.unitOfMass, UnitOfMass.TroyOunce)
    }

    func testUserSettingTweaking() {
        let settings = UserSettings.sharedInstance;
        
        // check existence
        XCTAssertNotNil(settings)
        
        // check expected defaults
        XCTAssertEqual(settings.priceUpdateMode, PriceUpdateMode.SpotBid)
        XCTAssertEqual(settings.mediumOfExchange, MediumOfExchange.USDollar)
        XCTAssertEqual(settings.unitOfMass, UnitOfMass.TroyOunce)
        
        // change settings
        settings.priceUpdateMode = PriceUpdateMode.SpotAsk
        settings.mediumOfExchange = MediumOfExchange.Silver
        settings.unitOfMass = UnitOfMass.Gram
        
        // check NSUserDefaults
        var setting = NSUserDefaults.standardUserDefaults().objectForKey(AgEnumSetting.PriceUpdateMode.rawValue) as? String
        XCTAssert(setting != nil, "Expected value instead of nil")
        XCTAssertEqual(setting!, PriceUpdateMode.SpotAsk.rawValue)

        setting = NSUserDefaults.standardUserDefaults().objectForKey(AgEnumSetting.MediumOfExchange.rawValue) as? String
        XCTAssert(setting != nil, "Expected value instead of nil")
        XCTAssertEqual(setting!, MediumOfExchange.Silver.rawValue)

        setting = NSUserDefaults.standardUserDefaults().objectForKey(AgEnumSetting.UnitOfMass.rawValue) as? String
        XCTAssert(setting != nil, "Expected value instead of nil")
        XCTAssertEqual(setting!, UnitOfMass.Gram.rawValue)
        
        // check stored settings
        XCTAssertEqual(settings.priceUpdateMode, PriceUpdateMode.SpotAsk)
        XCTAssertEqual(settings.mediumOfExchange, MediumOfExchange.Silver)
        XCTAssertEqual(settings.unitOfMass, UnitOfMass.Gram)
        
        // reset settings
        settings.reset()
        
        // Prices should still be nil
        XCTAssert(settings.spotPrices == nil)
        XCTAssert(settings.londonFixedPrices == nil)
        XCTAssert(settings.manualPrices == nil)

        // check for empty NSUserDefaults
        setting = NSUserDefaults.standardUserDefaults().objectForKey(AgEnumSetting.PriceUpdateMode.rawValue) as? String
        XCTAssert(setting == nil, "Expected nil instead of: \(setting)")
        
        setting = NSUserDefaults.standardUserDefaults().objectForKey(AgEnumSetting.MediumOfExchange.rawValue) as? String
        XCTAssert(setting == nil, "Expected nil instead of: \(setting)")
        
        setting = NSUserDefaults.standardUserDefaults().objectForKey(AgEnumSetting.UnitOfMass.rawValue) as? String
        XCTAssert(setting == nil, "Expected nil instead of: \(setting)")
        
        // check expected defaults
        XCTAssertEqual(settings.priceUpdateMode, PriceUpdateMode.SpotBid)
        XCTAssertEqual(settings.mediumOfExchange, MediumOfExchange.USDollar)
        XCTAssertEqual(settings.unitOfMass, UnitOfMass.TroyOunce)
    }

    func testPriceLoad() {
        let settings = UserSettings.sharedInstance;
        settings.reset()

        let expectation = expectationWithDescription("Prices Refreshed")

        settings.refreshPrices { (errors:[ErrorType]?) -> () in
            XCTAssert(errors == nil, "Error during price refresh")
            expectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(10) { error in
            XCTAssertNil(error,"Timeout while waiting for refresh prices: \(error)")
        }
        
        // Should have spot and fixed prices loaded now
        XCTAssert(settings.spotPrices != nil)
        XCTAssert(settings.londonFixedPrices != nil)
        
        // Manual prices should still be nil
        XCTAssert(settings.manualPrices == nil)
        
        var spotPrices:[CommodityType:SpotPrice]? = settings.spotPrices
        XCTAssert(spotPrices != nil, "Error accessing spotPrices dictionary")

        // test silver
        var commodityPrice:SpotPrice? = spotPrices![CommodityType.Silver]
        XCTAssert(commodityPrice != nil, "Error accessing silver CommodityPrice")
        XCTAssert(commodityPrice!.type == CommodityType.Silver.managedType())
        XCTAssert(commodityPrice!.bid > silverRange.min && commodityPrice!.bid < silverRange.max, "Unexpected silver bid price")
        XCTAssert(commodityPrice!.ask < silverRange.max && commodityPrice!.ask > commodityPrice!.bid, "Unexpected silver ask price")
        XCTAssert(commodityPrice!.price > commodityPrice!.bid && commodityPrice!.price < commodityPrice!.ask, "Unexpected silver mid price")
        
        var currentTime:NSTimeInterval = NSDate().timeIntervalSinceReferenceDate
        XCTAssert(currentTime > commodityPrice!.timestamp && commodityPrice!.timestamp > 0, "Invalid silver timestamp")
                
        var timestampDate = NSDate(timestampDateString: commodityPrice!.timestampString())
        XCTAssert(timestampDate.timeIntervalSinceReferenceDate > 0, "timestampString unexpected format")

        // test gold
        commodityPrice = spotPrices![CommodityType.Gold]
        XCTAssert(commodityPrice != nil, "Error accessing gold CommodityPrice")
        XCTAssert(commodityPrice!.type == CommodityType.Gold.managedType())
        XCTAssert(commodityPrice!.bid > goldRange.min && commodityPrice!.bid < goldRange.max, "Unexpected gold bid price")
        XCTAssert(commodityPrice!.ask < goldRange.max && commodityPrice!.ask > commodityPrice!.bid, "Unexpected gold ask price")
        XCTAssert(commodityPrice!.price > commodityPrice!.bid && commodityPrice!.price < commodityPrice!.ask, "Unexpected gold mid price")
        
        currentTime = NSDate().timeIntervalSinceReferenceDate
        XCTAssert(currentTime > commodityPrice!.timestamp && commodityPrice!.timestamp > 0, "Invalid gold timestamp")
        
        timestampDate = NSDate(timestampDateString: commodityPrice!.timestampString())
        XCTAssert(timestampDate.timeIntervalSinceReferenceDate > 0, "timestampString unexpected format")
        
        // test platinum
        commodityPrice = spotPrices![CommodityType.Platinum]
        XCTAssert(commodityPrice != nil, "Error accessing platinum CommodityPrice")
        XCTAssert(commodityPrice!.type == CommodityType.Platinum.managedType())
        XCTAssert(commodityPrice!.bid > platinumRange.min && commodityPrice!.bid < platinumRange.max, "Unexpected platinum bid price")
        XCTAssert(commodityPrice!.ask < platinumRange.max && commodityPrice!.ask > commodityPrice!.bid, "Unexpected platinum ask price")
        XCTAssert(commodityPrice!.price > commodityPrice!.bid && commodityPrice!.price < commodityPrice!.ask, "Unexpected platinum mid price")
        
        currentTime = NSDate().timeIntervalSinceReferenceDate
        XCTAssert(currentTime > commodityPrice!.timestamp && commodityPrice!.timestamp > 0, "Invalid platinum timestamp")
        
        timestampDate = NSDate(timestampDateString: commodityPrice!.timestampString())
        XCTAssert(timestampDate.timeIntervalSinceReferenceDate > 0, "timestampString unexpected format")
        
        // test palladium
        commodityPrice = spotPrices![CommodityType.Palladium]
        XCTAssert(commodityPrice != nil, "Error accessing palladium CommodityPrice")
        XCTAssert(commodityPrice!.type == CommodityType.Palladium.managedType())
        XCTAssert(commodityPrice!.bid > palladiumRange.min && commodityPrice!.bid < palladiumRange.max, "Unexpected palladium bid price")
        XCTAssert(commodityPrice!.ask < palladiumRange.max && commodityPrice!.ask > commodityPrice!.bid, "Unexpected palladium ask price")
        XCTAssert(commodityPrice!.price > commodityPrice!.bid && commodityPrice!.price < commodityPrice!.ask, "Unexpected palladium mid price")
        
        currentTime = NSDate().timeIntervalSinceReferenceDate
        XCTAssert(currentTime > commodityPrice!.timestamp && commodityPrice!.timestamp > 0, "Invalid palladium timestamp")
        
        timestampDate = NSDate(timestampDateString: commodityPrice!.timestampString())
        XCTAssert(timestampDate.timeIntervalSinceReferenceDate > 0, "timestampString unexpected format")
    
    
        let londonFixedPrices:[CommodityType:FixPrice]? = settings.londonFixedPrices
        XCTAssert(londonFixedPrices != nil, "Error accessing londonFixedPrices dictionary")
        
        // Fixed Silver
//        var fixedPrice:FixPrice? = londonFixedPrices![CommodityType.Silver]
//        XCTAssert(fixedPrice != nil, "Error accessing silver FixedPrice")
//        XCTAssert(fixedPrice!.type == CommodityType.Silver)
//        XCTAssert(fixedPrice!.price > silverRange.min && fixedPrice!.price < silverRange.max, "Unexpected silver fixed price")
//        
//        currentTime = NSDate().timeIntervalSinceReferenceDate
//        XCTAssert(currentTime > fixedPrice!.timestamp && fixedPrice!.timestamp > 0, "Invalid silver timestamp")
//                
//        timestampDate = NSDate(timestampDateString: fixedPrice!.timestampString())
//        XCTAssert(timestampDate.timeIntervalSinceReferenceDate > 0, "timestampString unexpected format")
//    
//        // Fixed Gold
//        fixedPrice = londonFixedPrices![CommodityType.Gold]
//        XCTAssert(fixedPrice != nil, "Error accessing gold FixedPrice")
//        XCTAssert(fixedPrice!.type == CommodityType.Gold)
//        XCTAssert(fixedPrice!.price > goldRange.min && fixedPrice!.price < goldRange.max, "Unexpected gold fixed price")
//        
//        currentTime = NSDate().timeIntervalSinceReferenceDate
//        XCTAssert(currentTime > fixedPrice!.timestamp && fixedPrice!.timestamp > 0, "Invalid gold timestamp")
//        
//        timestampDate = NSDate(timestampDateString: fixedPrice!.timestampString())
//        XCTAssert(timestampDate.timeIntervalSinceReferenceDate > 0, "timestampString unexpected format")
//        
//        // Fixed Platinum
//        fixedPrice = londonFixedPrices![CommodityType.Platinum]
//        XCTAssert(fixedPrice != nil, "Error accessing platinum FixedPrice")
//        XCTAssert(fixedPrice!.type == CommodityType.Platinum)
//        XCTAssert(fixedPrice!.price > platinumRange.min && fixedPrice!.price < platinumRange.max, "Unexpected platinum fixed price")
//        
//        currentTime = NSDate().timeIntervalSinceReferenceDate
//        XCTAssert(currentTime > fixedPrice!.timestamp && fixedPrice!.timestamp > 0, "Invalid platinum timestamp")
//        
//        timestampDate = NSDate(timestampDateString: fixedPrice!.timestampString())
//        XCTAssert(timestampDate.timeIntervalSinceReferenceDate > 0, "timestampString unexpected format")
//
//        // Fixed Palladium
//        fixedPrice = londonFixedPrices![CommodityType.Palladium]
//        XCTAssert(fixedPrice != nil, "Error accessing palladium FixedPrice")
//        XCTAssert(fixedPrice!.type == CommodityType.Palladium)
//        XCTAssert(fixedPrice!.price > palladiumRange.min && fixedPrice!.price < palladiumRange.max, "Unexpected palladium fixed price")
//        
//        currentTime = NSDate().timeIntervalSinceReferenceDate
//        XCTAssert(currentTime > fixedPrice!.timestamp && fixedPrice!.timestamp > 0, "Invalid palladium timestamp")
//        
//        timestampDate = NSDate(timestampDateString: fixedPrice!.timestampString())
//        XCTAssert(timestampDate.timeIntervalSinceReferenceDate > 0, "timestampString unexpected format")
    }
    
    func testManualPriceSettings() {
//        let settings = UserSettings.sharedInstance;
//        settings.reset()
//        
//        // Prices should still be nil
//        XCTAssert(settings.spotPrices == nil)
//        XCTAssert(settings.londonFixedPrices == nil)
//        XCTAssert(settings.manualPrices == nil)
//        
//        UserSettings.sharedInstance.manualPrices = [CommodityType.Silver : FixedPrice(_type: CommodityType.Silver, _price: manualSilverPrice, _timestamp:0)]
//        XCTAssert(UserSettings.sharedInstance.manualPrices?.count == 1)
//        
//        UserSettings.sharedInstance.manualPrices = [CommodityType.Silver : FixedPrice(_type: CommodityType.Silver, _price: manualSilverPrice, _timestamp:0), CommodityType.Gold : FixedPrice(_type: CommodityType.Gold, _price: manualGoldPrice, _timestamp:0)]
//        XCTAssert(UserSettings.sharedInstance.manualPrices?.count == 2)
//        
//        let manualPrices = UserSettings.sharedInstance.manualPrices
//        XCTAssert(manualPrices != nil, "Error accessing manualPrices silver")
//
//        let commodityPrice = manualPrices![CommodityType.Silver]
//        XCTAssert(commodityPrice != nil, "Error accessing manual commodityPrice silver")
//        XCTAssert(commodityPrice!.type == CommodityType.Silver)
//        XCTAssert(commodityPrice!.price == manualSilverPrice, "Unexpected silver manual price")
//        
//        // test removing manual prices
//        UserSettings.sharedInstance.manualPrices = nil
//
//        // Prices should still be nil
//        XCTAssert(settings.spotPrices == nil)
//        XCTAssert(settings.londonFixedPrices == nil)
//        XCTAssert(settings.manualPrices == nil)
//
    }
    
    func testMeltValueCalculations() {
        // reset user settings to defaults
        let settings = UserSettings.sharedInstance;
        settings.reset()
        
        let expectation = expectationWithDescription("Prices Refreshed")
        
        settings.refreshPrices { (errors:[ErrorType]?) -> () in
            XCTAssert(errors == nil, "Error during price refresh")
            expectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(10) { error in
            XCTAssertNil(error,"Timeout while waiting for refresh prices: \(error)")
        }

        // Should have spot and fixed prices loaded now
        XCTAssert(settings.spotPrices != nil)
        XCTAssert(settings.londonFixedPrices != nil)

        // check meltValueLabel return values for USDollar medium of exchange
        XCTAssert(self.coin != nil)
        UserSettings.sharedInstance.mediumOfExchange = MediumOfExchange.USDollar
        var (text, font) = UserSettings.sharedInstance.meltValueLabel(self.coin!)
        XCTAssert(font != nil)
        XCTAssert(text.hasPrefix("$"))
        var sliced:NSString = String(text.characters.dropFirst()) as NSString
        XCTAssert(sliced.floatValue > 0)
        
        // check meltValueLabel return values for Silver medium of exchange
        UserSettings.sharedInstance.mediumOfExchange = MediumOfExchange.Silver
        UserSettings.sharedInstance.unitOfMass = UnitOfMass.TroyOunce
        (text, font) = UserSettings.sharedInstance.meltValueLabel(self.coin!)
        XCTAssert(font != nil)
        XCTAssert(text.hasSuffix(" oz t"))
        var range = text.rangeOfString(" oz t")
        sliced = text.substringToIndex(range!.startIndex)
        XCTAssert(sliced.floatValue > 0)

        UserSettings.sharedInstance.unitOfMass = UnitOfMass.Gram
        (text, font) = UserSettings.sharedInstance.meltValueLabel(self.coin!)
        XCTAssert(font != nil)
        XCTAssert(text.hasSuffix(" g"))
        range = text.rangeOfString(" g")
        sliced = text.substringToIndex(range!.startIndex)
        XCTAssert(sliced.floatValue > 0)
    }
    
    func testPriceForCalculation() {
        let settings = UserSettings.sharedInstance;
        settings.reset()
        
        let expectation = expectationWithDescription("Prices Refreshed")
        
        settings.refreshPrices { (errors:[ErrorType]?) -> () in
            XCTAssert(errors == nil, "Error during price refresh")
            expectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(10) { error in
            XCTAssertNil(error,"Timeout while waiting for refresh prices: \(error)")
        }
        
        // Should have spot and fixed prices loaded now
        XCTAssert(settings.spotPrices != nil)
        XCTAssert(settings.londonFixedPrices != nil)
        
        let spotPrices:[CommodityType:SpotPrice]? = settings.spotPrices
        XCTAssert(spotPrices != nil, "Error accessing spotPrices dictionary")

//        let fixPrices:[CommodityType:FixedPrice]? = settings.londonFixedPrices
//        XCTAssert(fixPrices != nil, "Error accessing fixedPrices dictionary")
//
//        UserSettings.sharedInstance.manualPrices = [
//            CommodityType.Silver : FixedPrice(_type: CommodityType.Silver, _price: manualSilverPrice, _timestamp:0),
//            CommodityType.Gold : FixedPrice(_type: CommodityType.Gold, _price: manualGoldPrice, _timestamp:0),
//            CommodityType.Platinum : FixedPrice(_type: CommodityType.Platinum, _price: manualPlatinumPrice, _timestamp:0),
//            CommodityType.Palladium : FixedPrice(_type: CommodityType.Palladium, _price: manualPalladiumPrice, _timestamp:0)
//        ]
//        XCTAssert(UserSettings.sharedInstance.manualPrices?.count == 4)
//        let manualPrices:[CommodityType:FixedPrice]? = settings.manualPrices
//        XCTAssert(manualPrices != nil, "Error accessing manual prices dictionary")
//        
//        // -- test silver --
//        var commodityPrice:SpotPrice? = spotPrices![CommodityType.Silver]
//        var fixPrice:FixedPrice? = fixPrices![CommodityType.Silver]
//        var manualPrice:FixedPrice? = manualPrices![CommodityType.Silver]
//
//        // bid
//        UserSettings.sharedInstance.priceUpdateMode = PriceUpdateMode.SpotBid
//        var (price,time) = UserSettings.sharedInstance.priceForCalculation(.Silver)
//        XCTAssert(!time.isEmpty)
//        XCTAssert(price == commodityPrice?.bid)
//        
//        // ask
//        UserSettings.sharedInstance.priceUpdateMode = PriceUpdateMode.SpotAsk
//        (price,time) = UserSettings.sharedInstance.priceForCalculation(.Silver)
//        XCTAssert(!time.isEmpty)
//        XCTAssert(price == commodityPrice?.ask)
//
//        // mid
//        UserSettings.sharedInstance.priceUpdateMode = PriceUpdateMode.SpotMid
//        (price,time) = UserSettings.sharedInstance.priceForCalculation(.Silver)
//        XCTAssert(!time.isEmpty)
//        XCTAssert(price == commodityPrice?.price)
//
//        // London Fix
//        UserSettings.sharedInstance.priceUpdateMode = PriceUpdateMode.LondonFix
//        (price,time) = UserSettings.sharedInstance.priceForCalculation(.Silver)
//        XCTAssert(!time.isEmpty)
//        XCTAssert(price == fixPrice?.price)
//        
//        // Manual Price
//        UserSettings.sharedInstance.priceUpdateMode = PriceUpdateMode.Manual
//        (price,time) = UserSettings.sharedInstance.priceForCalculation(.Silver)
//        XCTAssert(!time.isEmpty)
//        XCTAssert(price == manualPrice?.price)
//        
//        
//        // -- test gold --
//        commodityPrice = spotPrices![CommodityType.Gold]
//        fixPrice = fixPrices![CommodityType.Gold]
//        manualPrice = manualPrices![CommodityType.Gold]
//        
//        // bid
//        UserSettings.sharedInstance.priceUpdateMode = PriceUpdateMode.SpotBid
//        (price,time) = UserSettings.sharedInstance.priceForCalculation(.Gold)
//        XCTAssert(!time.isEmpty)
//        XCTAssert(price == commodityPrice?.bid)
//        
//        // ask
//        UserSettings.sharedInstance.priceUpdateMode = PriceUpdateMode.SpotAsk
//        (price,time) = UserSettings.sharedInstance.priceForCalculation(.Gold)
//        XCTAssert(!time.isEmpty)
//        XCTAssert(price == commodityPrice?.ask)
//        
//        // mid
//        UserSettings.sharedInstance.priceUpdateMode = PriceUpdateMode.SpotMid
//        (price,time) = UserSettings.sharedInstance.priceForCalculation(.Gold)
//        XCTAssert(!time.isEmpty)
//        XCTAssert(price == commodityPrice?.price)
//        
//        // London Fix
//        UserSettings.sharedInstance.priceUpdateMode = PriceUpdateMode.LondonFix
//        (price,time) = UserSettings.sharedInstance.priceForCalculation(.Gold)
//        XCTAssert(!time.isEmpty)
//        XCTAssert(price == fixPrice?.price)
//        
//        // Manual Price
//        UserSettings.sharedInstance.priceUpdateMode = PriceUpdateMode.Manual
//        (price,time) = UserSettings.sharedInstance.priceForCalculation(.Gold)
//        XCTAssert(!time.isEmpty)
//        XCTAssert(price == manualPrice?.price)
//
//        
//        // -- test platinum --
//        commodityPrice = spotPrices![CommodityType.Platinum]
//        fixPrice = fixPrices![CommodityType.Platinum]
//        manualPrice = manualPrices![CommodityType.Platinum]
//        
//        // bid
//        UserSettings.sharedInstance.priceUpdateMode = PriceUpdateMode.SpotBid
//        (price,time) = UserSettings.sharedInstance.priceForCalculation(.Platinum)
//        XCTAssert(!time.isEmpty)
//        XCTAssert(price == commodityPrice?.bid)
//        
//        // ask
//        UserSettings.sharedInstance.priceUpdateMode = PriceUpdateMode.SpotAsk
//        (price,time) = UserSettings.sharedInstance.priceForCalculation(.Platinum)
//        XCTAssert(!time.isEmpty)
//        XCTAssert(price == commodityPrice?.ask)
//        
//        // mid
//        UserSettings.sharedInstance.priceUpdateMode = PriceUpdateMode.SpotMid
//        (price,time) = UserSettings.sharedInstance.priceForCalculation(.Platinum)
//        XCTAssert(!time.isEmpty)
//        XCTAssert(price == commodityPrice?.price)
//        
//        // London Fix
//        UserSettings.sharedInstance.priceUpdateMode = PriceUpdateMode.LondonFix
//        (price,time) = UserSettings.sharedInstance.priceForCalculation(.Platinum)
//        XCTAssert(!time.isEmpty)
//        XCTAssert(price == fixPrice?.price)
//        
//        // Manual Price
//        UserSettings.sharedInstance.priceUpdateMode = PriceUpdateMode.Manual
//        (price,time) = UserSettings.sharedInstance.priceForCalculation(.Platinum)
//        XCTAssert(!time.isEmpty)
//        XCTAssert(price == manualPrice?.price)
//        
//        
//        // -- test palladium --
//        commodityPrice = spotPrices![CommodityType.Palladium]
//        fixPrice = fixPrices![CommodityType.Palladium]
//        manualPrice = manualPrices![CommodityType.Palladium]
//        
//        // bid
//        UserSettings.sharedInstance.priceUpdateMode = PriceUpdateMode.SpotBid
//        (price,time) = UserSettings.sharedInstance.priceForCalculation(.Palladium)
//        XCTAssert(!time.isEmpty)
//        XCTAssert(price == commodityPrice?.bid)
//        
//        // ask
//        UserSettings.sharedInstance.priceUpdateMode = PriceUpdateMode.SpotAsk
//        (price,time) = UserSettings.sharedInstance.priceForCalculation(.Palladium)
//        XCTAssert(!time.isEmpty)
//        XCTAssert(price == commodityPrice?.ask)
//        
//        // mid
//        UserSettings.sharedInstance.priceUpdateMode = PriceUpdateMode.SpotMid
//        (price,time) = UserSettings.sharedInstance.priceForCalculation(.Palladium)
//        XCTAssert(!time.isEmpty)
//        XCTAssert(price == commodityPrice?.price)
//        
//        // London Fix
//        UserSettings.sharedInstance.priceUpdateMode = PriceUpdateMode.LondonFix
//        (price,time) = UserSettings.sharedInstance.priceForCalculation(.Palladium)
//        XCTAssert(!time.isEmpty)
//        XCTAssert(price == fixPrice?.price)
//        
//        // Manual Price
//        UserSettings.sharedInstance.priceUpdateMode = PriceUpdateMode.Manual
//        (price,time) = UserSettings.sharedInstance.priceForCalculation(.Palladium)
//        XCTAssert(!time.isEmpty)
//        XCTAssert(price == manualPrice?.price)
    }
    
    func testPricesForCalculations() {
        let settings = UserSettings.sharedInstance;
        settings.reset()
        
        let expectation = expectationWithDescription("Prices Refreshed")
        
        settings.refreshPrices { (errors:[ErrorType]?) -> () in
            XCTAssert(errors == nil, "Error during price refresh")
            expectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(10) { error in
            XCTAssertNil(error,"Timeout while waiting for refresh prices: \(error)")
        }
        
        // Should have spot and fixed prices loaded now
        XCTAssert(settings.spotPrices != nil)
        XCTAssert(settings.londonFixedPrices != nil)
        
        let spotPrices:[CommodityType:SpotPrice]? = settings.spotPrices
        XCTAssert(spotPrices != nil, "Error accessing spotPrices dictionary")
        
//        let londonFixedPrices:[CommodityType:FixedPrice]? = settings.londonFixedPrices
//        XCTAssert(londonFixedPrices != nil, "Error accessing fixedPrices dictionary")
//        
//        UserSettings.sharedInstance.manualPrices = [
//            CommodityType.Silver : FixedPrice(_type: CommodityType.Silver, _price: manualSilverPrice, _timestamp:0),
//            CommodityType.Gold : FixedPrice(_type: CommodityType.Gold, _price: manualGoldPrice, _timestamp:0),
//            CommodityType.Platinum : FixedPrice(_type: CommodityType.Platinum, _price: manualPlatinumPrice, _timestamp:0),
//            CommodityType.Palladium : FixedPrice(_type: CommodityType.Palladium, _price: manualPalladiumPrice, _timestamp:0)
//        ]
//        XCTAssert(UserSettings.sharedInstance.manualPrices?.count == 4)
//        let manualPrices:[CommodityType:FixedPrice]? = settings.manualPrices
//        XCTAssert(manualPrices != nil, "Error accessing manual prices dictionary")
//        
//
//        // bid
//        UserSettings.sharedInstance.priceUpdateMode = PriceUpdateMode.SpotBid
//        var prices:[CommodityType:Price]? = UserSettings.sharedInstance.pricesForCalculations()
//        XCTAssert(prices != nil)
//        for key in prices!.keys {
//            XCTAssert(prices![key]?.price == spotPrices![key]?.price)
//        }
//
//        // ask
//        UserSettings.sharedInstance.priceUpdateMode = PriceUpdateMode.SpotAsk
//        prices = UserSettings.sharedInstance.pricesForCalculations()
//        XCTAssert(prices != nil)
//        for key in prices!.keys {
//            XCTAssert(prices![key]?.price == spotPrices![key]?.price)
//        }
//        
//        // mid
//        UserSettings.sharedInstance.priceUpdateMode = PriceUpdateMode.SpotMid
//        prices = UserSettings.sharedInstance.pricesForCalculations()
//        XCTAssert(prices != nil)
//        for key in prices!.keys {
//            XCTAssert(prices![key]?.price == spotPrices![key]?.price)
//        }
//        
//        // London Fixed
//        UserSettings.sharedInstance.priceUpdateMode = PriceUpdateMode.LondonFix
//        var fixedPrices:[CommodityType:Price]? = UserSettings.sharedInstance.pricesForCalculations()
//        XCTAssert(fixedPrices != nil)
//        for key in fixedPrices!.keys {
//            XCTAssert(fixedPrices![key]?.price == londonFixedPrices![key]?.price)
//        }
//
//        // Manual
//        UserSettings.sharedInstance.priceUpdateMode = PriceUpdateMode.Manual
//        fixedPrices = UserSettings.sharedInstance.pricesForCalculations()
//        XCTAssert(fixedPrices != nil)
//        for key in fixedPrices!.keys {
//            XCTAssert(fixedPrices![key]?.price == manualPrices![key]?.price)
//        }
    }

}
