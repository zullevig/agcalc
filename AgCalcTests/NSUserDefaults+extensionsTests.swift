//
//  NSUserDefaults+extensionsTests.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 7/12/15.
//  Copyright © 2015 Zachary Ullevig. All rights reserved.
//

import XCTest

@testable import AgCalc


class NSUserDefaults_extensionsTests: XCTestCase {
    
    let floatValue:Float = 42

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testAddRemove() {
        NSUserDefaults.remove(AgEnumSetting.allSettings)
        
        for setting in AgEnumSetting.allSettings {
            let stringDefault = NSUserDefaults.getRawString(setting)
            XCTAssert(stringDefault == nil)

            let floatDefault = NSUserDefaults.getFloat(setting)
            XCTAssert(floatDefault == nil)
        }
        
        NSUserDefaults.set(AgEnumSetting.MediumOfExchange, value: MediumOfExchange.USDollar)
        var stringDefault = NSUserDefaults.getRawString(AgEnumSetting.MediumOfExchange)
        XCTAssert(stringDefault != nil)
        var floatDefault = NSUserDefaults.getFloat(AgEnumSetting.MediumOfExchange)
        XCTAssert(floatDefault == nil)
        
        NSUserDefaults.remove(AgEnumSetting.allSettings)

        NSUserDefaults.set(AgEnumSetting.ManualSilverPrice, value: floatValue)
        stringDefault = NSUserDefaults.getRawString(AgEnumSetting.ManualSilverPrice)
        XCTAssert(stringDefault == nil)
        floatDefault = NSUserDefaults.getFloat(AgEnumSetting.ManualSilverPrice)
        XCTAssert(floatDefault != nil)
        
        NSUserDefaults.remove(AgEnumSetting.ManualSilverPrice)
        
        for setting in AgEnumSetting.allSettings {
            let stringDefault = NSUserDefaults.getRawString(setting)
            XCTAssert(stringDefault == nil)
            
            let floatDefault = NSUserDefaults.getFloat(setting)
            XCTAssert(floatDefault == nil)
        }
    }
}
