//
//  CoinStackTests.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 7/14/15.
//  Copyright © 2015 Zachary Ullevig. All rights reserved.
//

import XCTest

@testable import AgCalc



class CoinStackTests: XCTestCase {
    enum TestCoins:Int {
        case MercuryDime = 2
        case BarberQuarter = 4
        case FranklinHalfDollar = 9
        case MorganDollar = 11
        case KennedyHalfDollar = 13
        case EisenhowerDollar = 14
        case SilverEagle = 16
    }
    
    // load a test coin from core data when first requested
    lazy var coins:[TestCoins:Coin] = {
        var result:[TestCoins:Coin] = [:]
        
        appDelegate.reloadCoinData()
        
        if let coin:Coin = appDelegate.coin(TestCoins.MercuryDime.rawValue) {
            result[TestCoins.MercuryDime] = coin
        }
        if let coin:Coin = appDelegate.coin(TestCoins.BarberQuarter.rawValue) {
            result[TestCoins.BarberQuarter] = coin
        }
        if let coin:Coin = appDelegate.coin(TestCoins.FranklinHalfDollar.rawValue) {
            result[TestCoins.FranklinHalfDollar] = coin
        }
        if let coin:Coin = appDelegate.coin(TestCoins.MorganDollar.rawValue) {
            result[TestCoins.MorganDollar] = coin
        }
        if let coin:Coin = appDelegate.coin(TestCoins.KennedyHalfDollar.rawValue) {
            result[TestCoins.KennedyHalfDollar] = coin
        }
        if let coin:Coin = appDelegate.coin(TestCoins.EisenhowerDollar.rawValue) {
            result[TestCoins.EisenhowerDollar] = coin
        }
        if let coin:Coin = appDelegate.coin(TestCoins.SilverEagle.rawValue) {
            result[TestCoins.SilverEagle] = coin
        }
        
        return result
    } ()
    
    let silverRange:(min:Float, max:Float) = (4, 100)
    let goldRange:(min:Float, max:Float) = (500, 3000)
    let platinumRange:(min:Float, max:Float) = (500, 3000)
    let palladiumRange:(min:Float, max:Float) = (250, 1500)

    let morganGrams:Float = 26.73
    let morganSilverFraction:Float = 0.9

    let gramPerTroyOz:Float = 31.1034768
    let troyOzPerGram:Float = 1.0 / 31.1034768
    func gramsToTroyOz(grams:Float) -> Float {
        return grams * self.troyOzPerGram
    }

    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCoinStack() {
        var stack = CoinStack()
        
        // add a coin
        stack.addCoin(coins[TestCoins.MercuryDime]!)
        var numCoins = stack.countForCoin(coins[TestCoins.MercuryDime]!)
        XCTAssert(numCoins == 1)

        // add it again
        stack.addCoin(coins[TestCoins.MercuryDime]!)
        numCoins = stack.countForCoin(coins[TestCoins.MercuryDime]!)
        XCTAssert(numCoins == 2)

        // remove coin that isn't there
        stack.removeCoin(coins[TestCoins.MorganDollar]!)
        numCoins = stack.countForCoin(coins[TestCoins.MercuryDime]!)
        XCTAssert(numCoins == 2)
        numCoins = stack.countForCoin(coins[TestCoins.MorganDollar]!)
        XCTAssert(numCoins == 0)

        // remove coin that is there
        stack.removeCoin(coins[TestCoins.MercuryDime]!)
        numCoins = stack.countForCoin(coins[TestCoins.MercuryDime]!)
        XCTAssert(numCoins == 1)

        // remove coin that is there
        stack.removeCoin(coins[TestCoins.MercuryDime]!)
        numCoins = stack.countForCoin(coins[TestCoins.MercuryDime]!)
        XCTAssert(numCoins == 0)
        
        // Add a bunch of coins
        stack.setCountForCoin(coins[TestCoins.MercuryDime]!, count: 2)
        stack.setCountForCoin(coins[TestCoins.BarberQuarter]!, count: 4)
        stack.setCountForCoin(coins[TestCoins.FranklinHalfDollar]!, count: 8)
        stack.setCountForCoin(coins[TestCoins.MorganDollar]!, count: 16)
        stack.setCountForCoin(coins[TestCoins.KennedyHalfDollar]!, count: 32)
        stack.setCountForCoin(coins[TestCoins.EisenhowerDollar]!, count: 64)
        stack.setCountForCoin(coins[TestCoins.SilverEagle]!, count: 128)
        
        // check added counts
        numCoins = stack.countForCoin(coins[TestCoins.MercuryDime]!)
        XCTAssert(numCoins == 2)
        numCoins = stack.countForCoin(coins[TestCoins.BarberQuarter]!)
        XCTAssert(numCoins == 4)
        numCoins = stack.countForCoin(coins[TestCoins.FranklinHalfDollar]!)
        XCTAssert(numCoins == 8)
        numCoins = stack.countForCoin(coins[TestCoins.MorganDollar]!)
        XCTAssert(numCoins == 16)
        numCoins = stack.countForCoin(coins[TestCoins.KennedyHalfDollar]!)
        XCTAssert(numCoins == 32)
        numCoins = stack.countForCoin(coins[TestCoins.EisenhowerDollar]!)
        XCTAssert(numCoins == 64)
        numCoins = stack.countForCoin(coins[TestCoins.SilverEagle]!)
        XCTAssert(numCoins == 128)

        // change some counts
        stack.setCountForCoin(coins[TestCoins.FranklinHalfDollar]!, count: 88)
        stack.setCountForCoin(coins[TestCoins.EisenhowerDollar]!, count: 0)

        // check changed counts
        numCoins = stack.countForCoin(coins[TestCoins.FranklinHalfDollar]!)
        XCTAssert(numCoins == 88)
        numCoins = stack.countForCoin(coins[TestCoins.EisenhowerDollar]!)
        XCTAssert(numCoins == 0)

        // remove coins with counts manually set
        stack.removeCoin(coins[TestCoins.FranklinHalfDollar]!)
        numCoins = stack.countForCoin(coins[TestCoins.FranklinHalfDollar]!)
        XCTAssert(numCoins == 87)
        stack.removeCoin(coins[TestCoins.EisenhowerDollar]!)
        numCoins = stack.countForCoin(coins[TestCoins.EisenhowerDollar]!)
        XCTAssert(numCoins == 0)
        
        // remove all coins in stack
        stack.removeAllCoins()
        numCoins = stack.countForCoin(coins[TestCoins.MercuryDime]!)
        XCTAssert(numCoins == 0)
        numCoins = stack.countForCoin(coins[TestCoins.BarberQuarter]!)
        XCTAssert(numCoins == 0)
        numCoins = stack.countForCoin(coins[TestCoins.FranklinHalfDollar]!)
        XCTAssert(numCoins == 0)
        numCoins = stack.countForCoin(coins[TestCoins.MorganDollar]!)
        XCTAssert(numCoins == 0)
        numCoins = stack.countForCoin(coins[TestCoins.KennedyHalfDollar]!)
        XCTAssert(numCoins == 0)
        numCoins = stack.countForCoin(coins[TestCoins.EisenhowerDollar]!)
        XCTAssert(numCoins == 0)
        numCoins = stack.countForCoin(coins[TestCoins.SilverEagle]!)
        XCTAssert(numCoins == 0)

        // test weight calculations
        stack.addCoin(coins[TestCoins.MorganDollar]!)
        var stackWeight = stack.stackGrams(CommodityType.Silver)
        XCTAssert(stackWeight == morganGrams * morganSilverFraction, "\(stackWeight) != \(morganGrams * morganSilverFraction)")
        stackWeight = stack.stackOunces(CommodityType.Silver)
        XCTAssert(stackWeight == self.gramsToTroyOz(morganGrams * morganSilverFraction), "\(stackWeight) != \(self.gramsToTroyOz(morganGrams * morganSilverFraction))")
        
        // test value calculation
        UserSettings.sharedInstance.refreshPrices { (error) -> () in
            XCTAssert(error == nil)
            UserSettings.sharedInstance.priceUpdateMode = PriceUpdateMode.SpotBid
            if let prices = UserSettings.sharedInstance.pricesForCalculations() {
                let value = stack.stackValue(prices)
                XCTAssert(value / stackWeight >= self.silverRange.min)
                XCTAssert(value / stackWeight <= self.silverRange.max)
            }
        }
    }
}
