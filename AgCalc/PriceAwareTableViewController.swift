//
//  PriceAwareTableViewController.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 12/7/14.
//  Copyright (c) 2014 Zachary Ullevig. All rights reserved.
//

import UIKit

class PriceAwareTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        NSNotificationCenter.defaultCenter().addObserver(self, selector:#selector(PriceAwareTableViewController.priceUpdated(_:)), name:KitcoPricesUpdatedNotification, object:nil)

        self.updatePriceControls()
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updatePriceControls() {
    }
    
    func priceUpdated(notification: NSNotification) {
        self.updatePriceControls()
    }
    
}
