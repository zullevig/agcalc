//
//  RestorePurchasesHeaderView.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 7/19/15.
//  Copyright © 2015 Zachary Ullevig. All rights reserved.
//

import UIKit

class RestorePurchasesHeaderView: UIView {

    weak var productController:PurchaseViewController? = nil

    @IBAction func restorePurchases(sender: AnyObject) {
        self.productController?.restorePurchases()
    }

}
