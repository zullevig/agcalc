//
//  CommodityPrice.swift
//  
//
//  Created by Zachary Ullevig on 5/8/16.
//
//

import Foundation
import CoreData


enum CommodityType:Int {
    case Unknown = 0
    case Silver = 1
    case Gold = 2
    case Platinum = 3
    case Palladium = 4
    
    static let all:[CommodityType] = [Silver, Gold, Platinum, Palladium]
    func name() -> String {
        switch self {
        case Silver:
            return "Silver"
        case Gold:
            return "Gold"
        case Platinum:
            return "Platinum"
        case Palladium:
            return "Palladium"
        default:
            return "Unknown"
        }
    }
    
    func managedType() -> Int32 {
        return Int32(self.rawValue)
    }
}

protocol Price {
    var price: Float { get }
}

class CommodityPrice: NSManagedObject, Price {

    // Insert code here to add functionality to your managed object subclass
    
    var price: Float {
        get {
            return 0
        }
    }
    
    var commodityType: CommodityType {
        get {
            let commodityType:CommodityType
            if let _type:CommodityType = CommodityType(rawValue:Int(self.type)) {
                commodityType = _type
            }
            else {
                commodityType = CommodityType.Unknown
            }
            return commodityType
        }
    }
    
    func timestampString() -> String {
        let date = NSDate(timeIntervalSinceReferenceDate: self.timestamp)
        return date.timestampDisplayString() as String
    }
}
