//
//  CommodityMarket+CoreDataProperties.swift
//  
//
//  Created by Zachary Ullevig on 5/8/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension CommodityMarket {

    @NSManaged var close: NSTimeInterval
    @NSManaged var name: String?
    @NSManaged var open: NSTimeInterval
    @NSManaged var type: Int32
    @NSManaged var prices: NSSet?

}
