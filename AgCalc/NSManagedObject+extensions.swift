//
//  NSManagedObject+extensions.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 5/10/16.
//  Copyright © 2016 Zachary Ullevig. All rights reserved.
//

import Foundation
import CoreData

// TODO: add to ZACoreDataStack
extension NSManagedObject {
    class var entityName: String {
        get {
            return String(self)
        }
    }
}

