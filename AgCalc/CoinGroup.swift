//
//  CoinGroup.swift
//  
//
//  Created by Zachary Ullevig on 5/8/16.
//
//

import Foundation
import CoreData

@objc(CoinGroup)
class CoinGroup: NSManagedObject {

// Insert code here to add functionality to your managed object subclass
    func sortedCoins() -> [Coin] {
        let result:[Coin]
        if var allCoins = self.coins?.allObjects as? [Coin] {
            allCoins.sortInPlace { (firstCoin:Coin, secondCoin:Coin) -> Bool in
                // sort by silver content first and first issue year second
                if firstCoin.grams(.Silver) < secondCoin.grams(.Silver) {
                    return true
                }
                else if firstCoin.grams(.Silver) == secondCoin.grams(.Silver) {
                    if firstCoin.firstYear.integerValue < secondCoin.firstYear.integerValue {
                        return true
                    }
                    else {
                        return false
                    }
                }
                else {
                    return false
                }
            }
            result = allCoins
        }
        else {
            result = []
        }
        
        return result
    }
    
    
    func activeCoins() -> [Coin] {
        let allCoins = self.sortedCoins()
        let filteredCoins = allCoins.filter { $0.active.boolValue == true }
        return filteredCoins
    }
    
    func load(json:NSDictionary) {
        if self.managedObjectContext != nil {
            if let idValue: AnyObject = json["@id"] {
                self.groupId = NSNumber(integer:idValue.integerValue)
            }
            
            if let name: String = json["@name"] as? String {
                self.name = name
            }
            
            if let sequence: AnyObject = json["@sequence"] {
                self.sequence = NSNumber(integer:sequence.integerValue)
            }
            
            // Parse Coins
            if let coinsJSONArray: NSArray = json["coins"] as? NSArray {
                self.loadCoins(coinsJSONArray, existingCoins: nil)
            }
        }
    }
    
    func loadCoins(jsonArray:NSArray, existingCoins:[Coin]?) {
        var resultingCoins:[Coin] = []
        
        if let existing = existingCoins {
            resultingCoins = existing
        }
        
        if let managedContext = self.managedObjectContext {
            let coinSet = NSMutableSet()
            
            for existingCoin in resultingCoins {
                coinSet.addObject(existingCoin)
            }
            
            for coinDictionary in jsonArray {
                
                if let idValue: AnyObject = coinDictionary["@id"] {
                    let filteredCoins:[Coin]? = resultingCoins.filter({$0.coinId == idValue.integerValue})
                    if filteredCoins?.count == 0 {
                        let coin:Coin = NSEntityDescription.insertNewObjectForEntityForName("Coin", inManagedObjectContext: managedContext) as! Coin
                        coin.load(coinDictionary as! NSDictionary, coinGroup:self)
                        coinSet.addObject(coin)
                    }
                    else {
                        // TODO: update existing coin
                    }
                }
            }
            self.coins = coinSet
        }
    }
}
