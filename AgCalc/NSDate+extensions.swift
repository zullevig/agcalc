//
//  NSDate+extensions.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 12/7/14.
//  Copyright (c) 2014 Zachary Ullevig. All rights reserved.
//

import Foundation


extension NSDate
{
    convenience init(kitcoDateString:String) {
        let dateStringFormatter = NSDateFormatter()
        dateStringFormatter.dateFormat = "MMM dd, yyyy 'at' H:mm.ss'\r\n'"
        dateStringFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        dateStringFormatter.timeZone = NSTimeZone(name: "America/New_York")
        if let d = dateStringFormatter.dateFromString(kitcoDateString) {
            self.init(timeIntervalSinceReferenceDate: d.timeIntervalSinceReferenceDate)
        }
        else {
            self.init(timeIntervalSinceReferenceDate: 0)
        }
    }
    
    convenience init(kitcoShortDateString:String) {
        let stringArray = kitcoShortDateString.componentsSeparatedByString("\r")
        let dateString = stringArray[0]
        
        let dateStringFormatter = NSDateFormatter()
        dateStringFormatter.dateFormat = "MM/dd/yyyy H:mm"
        dateStringFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        dateStringFormatter.timeZone = NSTimeZone(name: "America/New_York")
        if let d = dateStringFormatter.dateFromString(dateString) {
            self.init(timeIntervalSinceReferenceDate: d.timeIntervalSinceReferenceDate)
        }
        else {
            self.init(timeIntervalSinceReferenceDate: 0)
        }
    }
    
    convenience init(londonFixDateString:String) {
        let dateStringFormatter = NSDateFormatter()
        dateStringFormatter.dateFormat = "MMM dd,yyyy"
        dateStringFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        dateStringFormatter.timeZone = NSTimeZone(name: "Europe/London")
        if let d = dateStringFormatter.dateFromString(londonFixDateString) {
            self.init(timeIntervalSinceReferenceDate: d.timeIntervalSinceReferenceDate)
        }
        else {
            self.init(timeIntervalSinceReferenceDate: 0)
        }
    }

    convenience init(timestampDateString:String) {
        let dateStringFormatter = NSDateFormatter()
        dateStringFormatter.dateFormat = "H:mma MM/dd/yy"
        dateStringFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        dateStringFormatter.timeZone = NSTimeZone(name: "America/New_York")
        if let d = dateStringFormatter.dateFromString(timestampDateString) {
            self.init(timeIntervalSinceReferenceDate: d.timeIntervalSinceReferenceDate)
        }
        else {
            self.init(timeIntervalSinceReferenceDate: 0)
        }
    }
    
    func timestampDisplayString() -> (NSString) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "h:mma MM/dd/yy"
        let dateString = dateFormatter.stringFromDate(self)
        return dateString
    }
}

