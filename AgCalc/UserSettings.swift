//
//  UserSettings.swift
//  AgCalc
//
//  Created by Zach Ullevig on 1/8/15.
//  Copyright (c) 2015 Zachary Ullevig. All rights reserved.
//

import UIKit
import CoreData


// MARK: - Enumerated NSUserDefaults

// MARK: AgEnumSetting
enum AgEnumSetting:String, StringDefaultKey {
    case MediumOfExchange = "MediumOfExchange"
    case UnitOfMass = "UnitOfMass"
    case PriceUpdateMode = "PriceUpdateMode"
    case ManualSilverPrice = "ManualSilverPrice"
    case ManualGoldPrice = "ManualGoldPrice"
    case AdMode = "AdMode"
    
    static let allSettings:[StringDefaultKey] = [MediumOfExchange, UnitOfMass, PriceUpdateMode, ManualSilverPrice, ManualGoldPrice, AdMode]
}

// MARK: MediumOfExchange
enum MediumOfExchange:String, StringEnum {
	case Silver = "Silver"
	case USDollar = "USDollar"
}

// MARK: UnitOfMass
enum UnitOfMass:String, StringEnum {
	case TroyOunce = "TroyOunce"
	case Gram = "Gram"
}

// MARK: PriceUpdateMode
enum PriceUpdateMode:String, StringEnum {
	case SpotBid = "SpotBid"
    case SpotAsk = "SpotAsk"
	case SpotMid = "SpotMid"
    case LondonFix = "LondonFix"
	case Manual = "Manual"
}

enum AdMode:String, StringEnum {
    case On = "On"
    case Off = "Off"
}


// MARK: -

class UserSettings {
    static let sharedInstance = UserSettings()
    
    // TODO: do real errors
    enum UserSettingsError: ErrorType {
        case GenericError
    }

    var adBannerViewController:AdBannerViewController? = nil
    
    // MARK: Price Dictionaries
    
	var spotPrices: [CommodityType:SpotPrice]? = nil
    var londonFixedPrices: [CommodityType:FixPrice]? = nil
    var manualPrice: ManualPrice? = nil
	
    var manualPrices:[CommodityType:ManualPrice]? {
        get {
            let result:[CommodityType:ManualPrice]?
            if let manualPrice = self.manualPrice {
                result =  [.Silver:manualPrice]
            }
            else {
                result = nil
            }
            return result
        }
    }

    // MARK: User Settings
    
	var mediumOfExchange:MediumOfExchange {
		get {
            // Default to USDollar
			var result: MediumOfExchange = .USDollar
            
            // check for existing value to return instead
            if let defaultValue =  NSUserDefaults.getRawString(AgEnumSetting.MediumOfExchange) {
                if let mediumOfExchange = MediumOfExchange( rawValue:defaultValue ) {
                    result = mediumOfExchange
                }
            }
            
			return result
		}
		
		set (newValue) {
            NSUserDefaults.set(AgEnumSetting.MediumOfExchange, value: newValue)
		}
	}
	
	var unitOfMass:UnitOfMass {
		get {
            // Default to Troy Ounces
			var result: UnitOfMass = .TroyOunce
            
            // check for existing value to return instead
            if let defaultValue =  NSUserDefaults.getRawString(AgEnumSetting.UnitOfMass) {
                if let unitOfMass = UnitOfMass( rawValue:defaultValue ) {
                    result = unitOfMass
                }
            }
            
			return result
		}
		
		set (newValue) {
            NSUserDefaults.set(AgEnumSetting.UnitOfMass, value: newValue)
		}
	}
	
	var priceUpdateMode:PriceUpdateMode {
		get {
            // Default to using the Spot Bid prices
			var result: PriceUpdateMode = .SpotBid
            
            // check for existing value to return instead
            if let defaultValue =  NSUserDefaults.getRawString(AgEnumSetting.PriceUpdateMode) {
                if let priceUpdateMode = PriceUpdateMode( rawValue:defaultValue ) {
                    result = priceUpdateMode
                }
            }
            
			return result
		}
		
		set (newValue) {
            NSUserDefaults.set(AgEnumSetting.PriceUpdateMode, value: newValue)
		}
	}

    var adMode:AdMode {
        get {
            // Default to ads running
            var result: AdMode = .On
            
            // check for existing value to return instead
            if let defaultValue =  NSUserDefaults.getRawString(AgEnumSetting.AdMode) {
                if let mode = AdMode( rawValue:defaultValue ) {
                    result = mode
                }
            }
            
            return result
        }
        
        set (newValue) {
            NSUserDefaults.set(AgEnumSetting.AdMode, value: newValue)
            if newValue == AdMode.Off {
                self.adBannerViewController?.disableAds()
            }
        }
    }
    
    init() {
        self.reloadManualSilverPrice()
    }
    
    func reset() {
        NSUserDefaults.remove(AgEnumSetting.allSettings)
        
        self.spotPrices = nil
        self.londonFixedPrices = nil
        self.manualPrice = nil
    }
    
    
    // MARK: -
    
    func reloadManualSilverPrice() {
        let request = NSFetchRequest(entityName: ManualPrice.entityName)
        request.predicate = NSPredicate(format: "type == \(CommodityType.Silver.managedType())")
        request.sortDescriptors = [NSSortDescriptor(key: "timestamp", ascending: false)]
        
        let managedObjectContext = coreDataStack.currentManagedObjectContext()
        do {
            guard let fetchResults = try managedObjectContext.executeFetchRequest(request) as? [ManualPrice] else {
                // TODO: make real error
                self.manualPrice = nil
                throw UserSettingsError.GenericError
            }
            
            if fetchResults.count > 0 {
                self.manualPrice = fetchResults[0]
            }
            else {
                self.manualPrice = nil
            }
            
        }
        catch {
            // TODO: handle error
            self.manualPrice = nil
        }
    }

    
    func refreshPrices(completionHandler:((error:[ErrorType]?) -> ())?) {
        // get latest metal prices
        let priceRequest = KitcoPriceRequest()
        priceRequest.requestCurrentPrices( { (prices:[CommodityPrice]?, errors:[ErrorType]?) -> () in
            var londonPrices:[CommodityType:FixPrice] = [:]
            var spotPrices:[CommodityType:SpotPrice] = [:]
            
            // sort out prices into proper categories
            // could likely use more swifty approach, but having trouble with the Price protocol as a generic type in dictionaries
            if let priceArray = prices {
                for price in priceArray {
                    if let londonFixedPrice:FixPrice = price as? FixPrice {
                        londonPrices[londonFixedPrice.commodityType] = londonFixedPrice
                    }
                    else if let spotPrice:SpotPrice = price as? SpotPrice {
                        spotPrices[spotPrice.commodityType] = spotPrice
                    }
                }
            }
            
            self.londonFixedPrices = nil
            self.spotPrices = nil

            if londonPrices.count > 0 {
                self.londonFixedPrices = londonPrices
            }
            
            if spotPrices.count > 0 {
                self.spotPrices = spotPrices
            }

            completionHandler?(error: errors)
        })
    }
    
    func pricesForCalculations() -> [CommodityType:CommodityPrice]? {
        var result:[CommodityType:CommodityPrice] = [:]
        
        switch priceUpdateMode {
            case .SpotBid, .SpotAsk, .SpotMid:
                if let latestPrices = self.spotPrices {
                    for key in latestPrices.keys {
                        result[key] = latestPrices[key]
                    }
                }
                
            case .LondonFix:
                if let latestPrices = self.londonFixedPrices {
                    for key in latestPrices.keys {
                        result[key] = latestPrices[key]
                    }
                }
            
            case .Manual:
                result[.Silver] = self.manualPrice
        }
        
        return result
    }

    func priceForCalculation(type:CommodityType) -> (price:Float,timestamp:String) {
        var result:(Float,String) = (0, "")
        
        switch priceUpdateMode {
            case .SpotBid:
                if let prices = self.spotPrices, price = prices[type] {
                    result = (price.bid, price.timestampString())
                }
                
            case .SpotAsk:
                if let prices = self.spotPrices, price = prices[type] {
                    result = (price.ask, price.timestampString())
                }
                
            case .SpotMid:
                if let prices = self.spotPrices, price = prices[type] {
                    result = (price.price, price.timestampString())
                }
                
            case .LondonFix:
                if let prices = self.londonFixedPrices, price = prices[type] {
                    result = (price.price, price.timestampString())
                }

            case .Manual:
                if let price = self.manualPrice {
                    result = (price.price, price.timestampString())
                }
        }
        
        return result
    }

    func meltValueLabel(coin:Coin) -> (text:String, font:UIFont?) {
        return self.meltValueLabel(coin, mediumOfExchange: self.mediumOfExchange)
    }

    func meltValueLabel(coin:Coin, mediumOfExchange:MediumOfExchange) -> (text:String, font:UIFont?) {
        let errorFont = UIFont(name: "Gill Sans", size: 9)
        let successFont = UIFont(name: "Courier New", size: 13)
        var resultFont = errorFont
        var resultString = "Value Unavailable"
        
        if self.priceForCalculation(.Silver).price > 0 {
            resultFont = successFont
        }
        else {
            resultFont = errorFont
        }
        
        switch mediumOfExchange {
        case .USDollar:
            let value = coin.troyOz(.Silver) * self.priceForCalculation(.Silver).price
            let formattedValue = String(format:"%.2f", value)
            resultString = "$\(formattedValue)"
            
        case .Silver:
            switch self.unitOfMass {
            case .Gram:
                let formattedValue = String(format:"%.2f", coin.grams(.Silver))
                resultString = "\(formattedValue) g"
                
            case .TroyOunce:
                let formattedValue = String(format:"%.2f", coin.troyOz(.Silver))
                resultString = "\(formattedValue) oz t"
            }
        }
        
        return (resultString, resultFont)
    }
}
