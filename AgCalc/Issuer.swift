//
//  Issuer.swift
//  
//
//  Created by Zachary Ullevig on 5/8/16.
//
//

import Foundation
import CoreData

@objc(Issuer)
class Issuer: NSManagedObject {

    // TODO: do real errors
    enum AgError: ErrorType {
        case AgGenericError
    }
    
    lazy var coinGroups:[CoinGroup]? = {
        var fetchedArray: [CoinGroup]? = nil
        let request = NSFetchRequest(entityName: "CoinGroup")
        request.predicate = NSPredicate(format: "ANY coins.issuer.issuerID == \(self.issuerID)")
        
        do {
            guard let fetchResults = try self.managedObjectContext!.executeFetchRequest(request) as? [CoinGroup] else {
                // TODO: throw real error
                throw AgError.AgGenericError
            }
            fetchedArray = fetchResults
            
            fetchedArray?.sortInPlace { (first:CoinGroup, second:CoinGroup) -> Bool in
                // sort by silver content first and first issue year second
                if first.sequence.integerValue < second.sequence.integerValue {
                    return true
                }
                else {
                    return false
                }
            }
        }
        catch {
            // TODO: handle error
        }
        
        return fetchedArray
    }()
    
    
    func load(json:NSDictionary) {
        if let idValue: AnyObject = json["@id"] {
            self.issuerID = NSNumber(integer:idValue.integerValue)
        }
        
        if let active: AnyObject = json["@active"] {
            self.active = NSNumber(integer:active.integerValue)
        }
        
        if let name: String = json["@name"] as? String {
            self.name = name
        }
        
        if let image: String = json["@image"] as? String {
            self.image = image
        }
        
        if let releaseState: String = json["@release"] as? String {
            self.releaseState = releaseState
        }
        
        if let sequence: AnyObject = json["@sequence"] {
            self.sequence = NSNumber(integer:sequence.integerValue)
        }
    }

}
