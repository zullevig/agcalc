//
//  SpotPriceTableViewCell.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 5/2/16.
//  Copyright © 2016 Zachary Ullevig. All rights reserved.
//

import UIKit

class SpotPriceTableViewCell: UITableViewCell {

    @IBOutlet weak var metalNameLabel: UILabel!
    @IBOutlet weak var bidLabel: UILabel!
    @IBOutlet weak var askLabel: UILabel!
    @IBOutlet weak var averageLabel: UILabel!
    
    var commodity:CommodityType? = nil {
        didSet {
            if let commodityType = self.commodity {
                metalNameLabel.text = commodityType.name()
                
                if let spotPrices = UserSettings.sharedInstance.spotPrices, let price = spotPrices[commodityType] {
                    bidLabel.text = String(format:"$%.2f", price.bid)
                    askLabel.text = String(format:"$%.2f", price.ask)
                    averageLabel.text = String(format:"$%.2f", ((price.bid + price.ask) / 2))
                }
                else {
                    bidLabel.text = ""
                    askLabel.text = ""
                    averageLabel.text = ""
                }
            }
        }
    }
}
