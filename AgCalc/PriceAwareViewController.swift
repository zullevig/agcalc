//
//  PriceAwareViewController.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 12/7/14.
//  Copyright (c) 2014 Zachary Ullevig. All rights reserved.
//

import UIKit

class PriceAwareViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()

        NSNotificationCenter.defaultCenter().addObserver(self, selector:#selector(PriceAwareViewController.priceUpdated(_:)), name:KitcoPricesUpdatedNotification, object:nil)

        self.updatePriceControls()

        UserSettings.sharedInstance.refreshPrices(nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.applyStyle()
    }
    
    func updatePriceControls() {
    }
    
    func priceUpdated(notification: NSNotification) {
        self.updatePriceControls()
    }
}
