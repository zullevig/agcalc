//
//  CalculatorCollectionViewCell.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 12/25/14.
//  Copyright (c) 2014 Zachary Ullevig. All rights reserved.
//

import UIKit

class CalculatorCollectionViewCell: ZASCollectionViewCell, UITextFieldDelegate {

	// MARK: - private attributes
	
	private var coin:Coin? = nil
	private var parentViewController:CalculatorViewController? = nil
	private var indexPath:NSIndexPath? = nil
	
	
    // MARK: - IBOutlets
    
    @IBOutlet weak var coinNameLabel: UILabel!
    @IBOutlet weak var coinImageView: UIImageView!
    @IBOutlet weak var quantityField: UITextField!
    @IBOutlet weak var coinValueLabel: UILabel!
    @IBOutlet weak var quantityStepper: UIStepper!
    @IBOutlet weak var issueDatesLabel: UILabel!
    @IBOutlet weak var coinHeaderView: UIView!
    
    
	// MARK: - initialization
	
	func configureCell(coin:Coin, quantity:Int, parent:CalculatorViewController, indexPath:NSIndexPath) {
		self.coin = coin
		self.parentViewController = parent
		self.indexPath = indexPath
		
		self.coinNameLabel.text = coin.name
        if coin.lastYear.integerValue > 0 {
            self.issueDatesLabel.text = "Issued: \(coin.firstYear) - \(coin.lastYear)"
        }
        else if coin.firstYear.integerValue > 0 {
            self.issueDatesLabel.text = "Issued: \(coin.firstYear) - present"
        }
        else {
            self.issueDatesLabel.text = ""
        }
        
        if let image = coin.image {
            self.coinImageView.image = UIImage(named:image)
        }
		
        self.quantityField.text = "\(quantity)"
        
        if let stack = self.parentViewController?.coinStack {
            self.quantityStepper.value = Double(stack.countForCoin(coin))
        }
        else {
            self.quantityStepper.value = 0.0
        }
		
        if let cellCoin = self.coin {
            let labelDetails:(text:String,font:UIFont?) = UserSettings.sharedInstance.meltValueLabel(cellCoin)
            self.coinValueLabel.text = labelDetails.text
            self.coinValueLabel.font = labelDetails.font
        }
	}

    
	// MARK: - IBActions

	@IBAction func stepQuantity(sender: UIStepper) {
		if let coin = self.coin {
			self.quantityField.text = "\(Int(sender.value))"
			self.parentViewController?.coinStack.setCountForCoin(coin, count: Int(sender.value))
			self.parentViewController?.updateTotalValue()
		}
	}
	
	@IBAction func updateFieldValue(sender: UITextField) {
		if let coin = self.coin {
			if let text = sender.text, let textInt = Int(text) {
				self.quantityStepper.value = Double(textInt)
				self.parentViewController?.coinStack.setCountForCoin(coin, count: textInt)
				self.parentViewController?.updateTotalValue()
			}
			else if sender.text == "" {
				self.quantityStepper.value = 0
				self.parentViewController?.coinStack.setCountForCoin(coin, count: 0)
				self.parentViewController?.updateTotalValue()
			}
		}
	}

	
    // MARK: - Text field delegate methods

    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        var result:Bool = false
        if range.location == 0 && string.characters.count == 0 {
            result = true
        }
        else if let fieldText:NSString = textField.text {
            let fullString:NSString = fieldText.stringByReplacingCharactersInRange(range, withString: string)
            
			if let replaceNumber = NSNumberFormatter().numberFromString(fullString as String) {
				if replaceNumber.floatValue < 100000 {
					result = true
				}
			}
        }
        
        return result
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        if textField.text == "0" {
            textField.text = ""
        }
        
        if let indexPath = self.indexPath {
            self.parentViewController?.coinCollectionView.scrollToItemAtIndexPath(indexPath, atScrollPosition: UICollectionViewScrollPosition.Top, animated: true)
        }
		
        return true
    }
	
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        if textField.text == "" {
            textField.text = "0"
        }
        
		if let coin = self.coin, let text = textField.text, let textInt = Int(text) {
            self.quantityStepper.value = Double(textInt)
            self.parentViewController?.coinStack.setCountForCoin(coin, count: textInt)
            self.parentViewController?.updateTotalValue()
		}
		
        return true
    }
}
