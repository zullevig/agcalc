//
//  NSUserDefaults+extensions.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 7/12/15.
//  Copyright © 2015 Zachary Ullevig. All rights reserved.
//

import Foundation

protocol StringEnum {
    var rawValue:String {get}
}

protocol StringDefaultKey {
    var rawValue:String {get}
    static var allSettings:[StringDefaultKey] {get}
}

extension NSUserDefaults {
    // MARK: - String value types
    static func set(key:StringDefaultKey, value:StringEnum) {
        NSUserDefaults.standardUserDefaults().setObject(value.rawValue, forKey:key.rawValue)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    static func getRawString(key:StringDefaultKey) -> String? {
        if let setting = NSUserDefaults.standardUserDefaults().objectForKey(key.rawValue) as? String {
            return setting
        }
        return nil
    }
    
    // MARK: - Float value types
    static func set(key:StringDefaultKey, value:Float) {
        NSUserDefaults.standardUserDefaults().setFloat(value, forKey:key.rawValue)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    static func getFloat(key:StringDefaultKey) -> Float? {
        if let setting = NSUserDefaults.standardUserDefaults().objectForKey(key.rawValue) as? Float {
            return setting
        }
        return nil
    }
    
    // MARK: - All value types
    static func remove(key:StringDefaultKey) {
        NSUserDefaults.standardUserDefaults().removeObjectForKey(key.rawValue)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    static func remove(keys:[StringDefaultKey]) {
        for key in keys {
            NSUserDefaults.standardUserDefaults().removeObjectForKey(key.rawValue)
        }
        NSUserDefaults.standardUserDefaults().synchronize()
    }
}

