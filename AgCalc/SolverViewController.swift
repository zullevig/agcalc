//
//  SolverViewController.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 12/6/14.
//  Copyright (c) 2014 Zachary Ullevig. All rights reserved.
//

import UIKit


class SolverViewController: PriceAwareViewController {
    
    let headerReuseIdentifier = "coinGroupSectionIdentifier"
    var coinStack:CoinStack = CoinStack()
    var selectedCoins:Set<Coin> = Set()
    var targetValue:Float = 0

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var refreshButton: UIButton!
    @IBOutlet weak var refreshSpinner: UIActivityIndicatorView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var timestampLabel: UILabel!
    @IBOutlet weak var amountField: UITextField!
    @IBOutlet weak var unitsLabel: UILabel!
    @IBOutlet weak var changeLabel: UILabel!
    @IBOutlet weak var changeAmountLabel: UILabel!
    @IBOutlet weak var coinCollectionView: UICollectionView!
    @IBOutlet weak var gridCollectionViewLayout: GridCollectionViewLayout!
    @IBOutlet weak var footerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let headerViewNIB = UINib(nibName: "CoinGroupCollectionHeaderView", bundle: nil)
        self.coinCollectionView?.registerNib(headerViewNIB, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerReuseIdentifier)
        
        if self.gridCollectionViewLayout != nil {
            self.gridCollectionViewLayout!.cellHeight = 105.0
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.solveCoinStack()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: -
    override func updatePriceControls() {
        // allow triggering UI updates from any thread
        dispatch_async(dispatch_get_main_queue(),{            
            let (silverPrice, timestampString) = UserSettings.sharedInstance.priceForCalculation(.Silver)
            if silverPrice > 0 {
                self.priceLabel.text = String(format:"$%.2f / oz t", silverPrice)
                if UserSettings.sharedInstance.priceUpdateMode == .Manual {
                    self.timestampLabel.text = "Manual Price Assigned"
                }
                else {
                    self.timestampLabel.text = "@\(timestampString)"
                }
            }
            else {
                // no price available
                if UserSettings.sharedInstance.priceUpdateMode == PriceUpdateMode.Manual {
                    self.priceLabel.text = "Price Not Set"
                }
                else {
                    self.priceLabel.text = "Price Unavailable"
                }
                self.timestampLabel.text = ""
            }
            
            self.coinCollectionView.reloadData()
            
            self.refreshSpinner.stopAnimating()
            self.refreshButton.hidden = false
        });
    }
    
    func solveCoinStack() {
        var change:Float = self.targetValue

        var coinArray:[Coin] = [Coin]()
        for coin in self.selectedCoins {
            coinArray.append(coin)
        }
        let sortedCoins:[Coin] = coinArray.sort({ $0.troyOz(.Silver) > $1.troyOz(.Silver) })


        // build coin stack
        self.coinStack.removeAllCoins()

        var unitsString:String? = nil
        switch UserSettings.sharedInstance.mediumOfExchange {
        case .USDollar:
            unitsString = "US$"
            let (price,_) = UserSettings.sharedInstance.priceForCalculation(.Silver)
            for coin in sortedCoins {
                let meltValue = price * coin.troyOz(.Silver)
                while change > meltValue {
                    self.coinStack.addCoin(coin)
                    change -= meltValue;
                }
            }
            
        
        case .Silver:
            switch UserSettings.sharedInstance.unitOfMass {
            case .Gram:
                unitsString = "g"
                for coin in sortedCoins {
                    while change > coin.grams(.Silver) {
                        self.coinStack.addCoin(coin)
                        change -= coin.grams(.Silver);
                    }
                }
                
            case .TroyOunce:
                unitsString = "oz"
                for coin in sortedCoins {
                    while change > coin.troyOz(.Silver) {
                        self.coinStack.addCoin(coin)
                        change -= coin.troyOz(.Silver);
                    }
                }
            }
        }
        
        if unitsString != nil {
            self.unitsLabel.text = unitsString
            let formattedChange = String(format:"%.2f", change)
            self.changeAmountLabel.text = "\(formattedChange) \(unitsString!)"
        }

        self.coinCollectionView.reloadData()
    }

    func toggleCoinSelection(coin:Coin?) {
        if let toggledCoin = coin {
            if self.coinIsSelected(toggledCoin) {
                self.selectedCoins.remove(toggledCoin)
            }
            else {
                self.selectedCoins.insert(toggledCoin)
            }
        }
        
        self.solveCoinStack()
    }
    
    func coinIsSelected(coin:Coin) -> Bool {
        var result = false
        if self.selectedCoins.contains(coin) {
            result = true
        }
        return result
    }
    
    
    // MARK: - UICollectionView delegate methods
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return appDelegate.activeCoinGroups.count;
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var result = 0
		if section < appDelegate.activeCoinGroups.count {
            result = appDelegate.activeCoinGroups[section].activeCoins().count
        }
        return result;
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("solverCoinCellIdentifier", forIndexPath: indexPath) as! SolverCollectionViewCell
		
		if indexPath.section < appDelegate.activeCoinGroups.count {
            let coins = appDelegate.activeCoinGroups[indexPath.section].activeCoins()
			let coin = coins[indexPath.item]
			cell.configureCell(coin, parent: self, indexPath: indexPath)
            cell.applyStyle()
		}
		
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> CoinGroupCollectionHeaderView {
        let view = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionHeader, withReuseIdentifier: headerReuseIdentifier, forIndexPath: indexPath) as! CoinGroupCollectionHeaderView
		if indexPath.section < appDelegate.activeCoinGroups.count {
            let coins = appDelegate.activeCoinGroups[indexPath.section].activeCoins()
            if let issuer = coins[0].issuer {
                view.configure(issuer)
            }
            view.coinGroupNameLabel.text = appDelegate.activeCoinGroups[indexPath.section].name
        }
		
        return view
    }

    
    // MARK: - Text field delegate methods
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        var result:Bool = false
        if range.location == 0 && string.characters.count == 0 {
            self.targetValue = 0
            self.solveCoinStack()
            result = true
        }
        else if let fieldText:NSString = textField.text {
            let fullString:NSString = fieldText.stringByReplacingCharactersInRange(range, withString: string)
            
            if let replaceNumber = NSNumberFormatter().numberFromString(fullString as String) where replaceNumber.floatValue < 100000 {
                self.targetValue = replaceNumber.floatValue
                self.solveCoinStack()
                result = true
            }
        }
        
        return result
    }

    // MARK: - IBActions
    
    @IBAction func refreshPrices(sender: UIButton) {
        self.refreshButton.hidden = true
        self.refreshSpinner.startAnimating()
        UserSettings.sharedInstance.refreshPrices(nil)
    }
}

