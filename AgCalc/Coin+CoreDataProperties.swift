//
//  Coin+CoreDataProperties.swift
//  
//
//  Created by Zachary Ullevig on 5/8/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Coin {

    @NSManaged var active: NSNumber
    @NSManaged var coinId: NSNumber
    @NSManaged var coinWeight: NSNumber
    @NSManaged var firstYear: NSNumber
    @NSManaged var goldFraction: NSNumber
    @NSManaged var image: String?
    @NSManaged var lastYear: NSNumber
    @NSManaged var name: String?
    @NSManaged var silverFraction: NSNumber
    @NSManaged var coinGroup: CoinGroup?
    @NSManaged var issuer: Issuer?

}
