//
//  CoinGroupCollectionHeaderView.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 12/24/14.
//  Copyright (c) 2014 Zachary Ullevig. All rights reserved.
//

import UIKit

class CoinGroupCollectionHeaderView: UICollectionReusableView {
        
    @IBOutlet weak var issuerImageView: UIImageView!
    @IBOutlet weak var coinGroupNameLabel: UILabel!
    
    func configure(issuer:Issuer) {
        if let image = issuer.image {
            self.issuerImageView.image = UIImage(named:image)
        }
    }
}
