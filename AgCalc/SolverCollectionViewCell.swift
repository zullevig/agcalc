//
//  SolverCollectionViewCell.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 12/28/14.
//  Copyright (c) 2014 Zachary Ullevig. All rights reserved.
//

import UIKit

class SolverCollectionViewCell: ZASCollectionViewCell {

    @IBOutlet weak var coinNameLabel: UILabel!
    @IBOutlet weak var coinImageView: UIImageView!
    @IBOutlet weak var coinCountLabel: UILabel!
    @IBOutlet weak var coinValueLabel: UILabel!
    @IBOutlet weak var selectedCheckBox: UIButton!
    @IBOutlet weak var issueDatesLabel: UILabel!
    @IBOutlet weak var coinHeaderView: UIView!

    var parentViewController:SolverViewController? = nil
    var coin:Coin? = nil
    var indexPath:NSIndexPath? = nil

    func configureCell(coin:Coin, parent:SolverViewController, indexPath:NSIndexPath) {
        self.coin = coin
        self.parentViewController = parent
        self.indexPath = indexPath
        self.coinNameLabel.text = coin.name
        if coin.lastYear.integerValue > 0 {
            self.issueDatesLabel.text = "Issued: \(coin.firstYear) - \(coin.lastYear)"
        }
        else if coin.firstYear.integerValue > 0 {
            self.issueDatesLabel.text = "Issued: \(coin.firstYear) - present"
        }
        else {
            self.issueDatesLabel.text = ""
        }
        
        if let image = coin.image {
            self.coinImageView.image = UIImage(named:image)    
        }

        self.coinCountLabel.text = "\(parent.coinStack.countForCoin(coin)) x"
                
        if let cellCoin = self.coin {
            let labelDetails:(text:String,font:UIFont?) = UserSettings.sharedInstance.meltValueLabel(cellCoin)
            self.coinValueLabel.text = labelDetails.text
            self.coinValueLabel.font = labelDetails.font
        }
        
//        self.applyGradients(self.frame.width)
        self.updateSelectionCheckbox()
    }

    // MARK: -
    // TODO: move to a generic location - duplicated across app
//    let backgroundGradient = CAGradientLayer()
//    func applyGradients(width:CGFloat) {
//        if width > 0 {
//            backgroundGradient.removeFromSuperlayer()
//            
//            var currentFrame = self.coinHeaderView.frame
//            currentFrame.size.width = width
//            
//            // apply gradient to title bar
//            let colorTop = UIColor.whiteColor().CGColor
//            let colorBottom = UIColor(red: 225.0/255.0, green: 225.0/255.0, blue: 225.0/255.0, alpha: 1.0).CGColor
//            backgroundGradient.colors = [colorTop, colorBottom]
//            backgroundGradient.frame = currentFrame
//            self.coinHeaderView.layer.insertSublayer(backgroundGradient, atIndex: 0)
//        }
//    }
    
  
    func updateSelectionCheckbox() {
        if let parent = self.parentViewController {
            if let cellCoin = self.coin {
                if parent.coinIsSelected(cellCoin) {
                    self.selectedCheckBox.selected = true
                }
                else {
                    self.selectedCheckBox.selected = false
                }
            }
        }
    }

    @IBAction func toggleSelection(sender: AnyObject) {
        self.parentViewController?.toggleCoinSelection(self.coin)
        self.updateSelectionCheckbox()
    }
}
