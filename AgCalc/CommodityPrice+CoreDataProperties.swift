//
//  CommodityPrice+CoreDataProperties.swift
//  
//
//  Created by Zachary Ullevig on 5/8/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension CommodityPrice {
    @NSManaged var timestamp: NSTimeInterval
    @NSManaged var market: CommodityMarket?
    @NSManaged var type: Int32

}
