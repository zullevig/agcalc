//
//  IssuersTableViewCell.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 12/9/14.
//  Copyright (c) 2014 Zachary Ullevig. All rights reserved.
//

import UIKit

class IssuersTableViewCell: UITableViewCell {

    @IBOutlet weak var issuerImageView: UIImageView!
    @IBOutlet weak var issuerSelectView: UIView!
    @IBOutlet weak var selectCoinsView: UIView!
    
    @IBOutlet weak var issuerLabel: UILabel!
    @IBOutlet weak var activeSwitch: UISwitch!
    @IBOutlet weak var dividerView: UIView!
    
    weak var tableView: UITableView?
    weak var issuer:Issuer? = nil
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    @IBAction func toggleActive(sender: AnyObject) {
        if issuer != nil {
            issuer!.active = NSNumber(bool:!issuer!.active.boolValue)
            self.tableView?.reloadData()
        }
    }
}