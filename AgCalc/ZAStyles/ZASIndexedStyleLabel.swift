//
//  ZASIndexedStyleLabel.swift
//  ZAStyles
//
//  Created by Zachary Ullevig on 7/11/15.
//  Copyright (c) 2015 Zachary Ullevig. All rights reserved.
//

import UIKit

/**
	Defines a label representing an element from and indexed array with the ability to assign unique colors to each item's label
*/
class ZASIndexedStyleLabel: ZASLabel {
	
	var colors:[HexColor]?
	
	/**
	Checks for a color hexValue assigned at the given array index, and if found, assigns that color to the view's background
	
	:param: index An index into the view's backgroundColors array.  If index is greater than the array size, the index will loop around the array to the beginning as necessary
	*/
	func setColorIndex(index:Int) {
		if let hexColorArray = self.colors {
			let colorIndex = index % hexColorArray.count
			let hexColor = hexColorArray[colorIndex]
			self.textColor = UIColor(hexColor: hexColor)
		}
	}
	
	override func applyStyle() {
		super.applyStyle()
		
        if let styles = self.assignedSyles() {
			for styleDictionary:[ZASStyleKeys:Any] in styles {
				processStyleDictionary(styleDictionary)
			}
		}
	}
	
	private func processStyleDictionary(styleDictionary:[ZASStyleKeys:Any]) {
		if let arrayBackgroundColor:[HexColor] = styleDictionary[ZASStyleKeys.ArrayBackgroundColor] as? [HexColor] {
			self.colors = arrayBackgroundColor
			self.setColorIndex(0)
		}
	}
	
}
