//
//  UINavigationBar+ZAStyles.swift
//  ZAStyles
//
//  Created by Zachary Ullevig on 7/11/15.
//  Copyright (c) 2015 Zachary Ullevig. All rights reserved.
//

import UIKit


extension UINavigationBar {
	override func applyStyle() {
		super.applyStyle()
		
        if let styles = self.assignedSyles() {
			for styleDictionary:[ZASStyleKeys:Any] in styles {
				processStyleDictionary(styleDictionary)
			}
		}
	}
	
    override func clearStyle() {
        super.clearStyle()
        
        var attributeDict:[String:AnyObject] = [:]
        if let font:UIFont = UIFont.systemFontOfSize(UIFont.systemFontSize()) {
            attributeDict[NSFontAttributeName] = font
        }
        self.tintColor = UIColor.blackColor()
        attributeDict[NSForegroundColorAttributeName] = UIColor.blackColor()
        self.titleTextAttributes = attributeDict

        self.barTintColor = UIColor.clearColor()
        
        if let font:UIFont = UIFont.systemFontOfSize(UIFont.systemFontSize()) {
            attributeDict[NSFontAttributeName] = font
        }
        
        self.tintColor = UIColor.blackColor()
        
        if let items = self.items {
            for item in items {
                if let rightBarButtonItems = item.rightBarButtonItems {
                    for barButtonItem in rightBarButtonItems {
                        barButtonItem.setTitleTextAttributes(nil, forState: UIControlState.Normal)
                    }
                }
                if let leftBarButtonItems = item.leftBarButtonItems {
                    for barButtonItem in leftBarButtonItems {
                        barButtonItem.setTitleTextAttributes(nil, forState: UIControlState.Normal)
                    }
                }
            }
        }
    }
    
	// TODO: if this can be made an override without getting a not yet supported error, the applyStyle method can be collapsed into the superclass
	private func processStyleDictionary(styleDictionary:[ZASStyleKeys:Any]) {
		var attributeDict:[String:AnyObject] = [:]
		
		if let fontName = styleDictionary[ZASStyleKeys.FontName] as? String, let fontSize = styleDictionary[ZASStyleKeys.FontSize] as? CGFloat {
			if let font:UIFont = UIFont(name: fontName, size: fontSize) {
				attributeDict[NSFontAttributeName] = font
			}
		}
		
		if let textColor = styleDictionary[ZASStyleKeys.TextColor] as? HexColor {
			self.tintColor = UIColor(hexColor: textColor)
			attributeDict[NSForegroundColorAttributeName] = UIColor(hexColor: textColor)
		}
		
		self.titleTextAttributes = attributeDict
		
        if let gradientColors:[HexColor] = styleDictionary[ZASStyleKeys.GradientColors] as? [HexColor] {
            if gradientColors.count > 1 && self.frame.width > 0 {
                
                if self.backgroundGradient == nil {
                    self.backgroundGradient = CAGradientLayer()
                    
                    let colorTop = UIColor(hexColor: gradientColors[0]).CGColor
                    let colorBottom = UIColor(hexColor: gradientColors[1]).CGColor
                    
                    // apply gradient to title bar
                    self.backgroundGradient!.colors = [colorTop, colorBottom]
                    self.backgroundGradient!.frame = self.bounds
                    
                    UIGraphicsBeginImageContext(self.bounds.size);
                    if let context = UIGraphicsGetCurrentContext() {
                        self.backgroundGradient!.renderInContext(context)
                        let gradientImage = UIGraphicsGetImageFromCurrentImageContext();
                        UIGraphicsEndImageContext();
                        self.setBackgroundImage(gradientImage, forBarMetrics: UIBarMetrics.Default)
                    }
                }
            }
        }
        else {
            if self.backgroundGradient != nil {
                self.backgroundGradient!.removeFromSuperlayer()
                self.backgroundGradient = nil
            }
            
            self.setBackgroundImage(nil, forBarMetrics: UIBarMetrics.Default)
            
            if let tintColor:HexColor = styleDictionary[ZASStyleKeys.BarTintColor] as? HexColor {
                self.barTintColor = UIColor(hexColor: tintColor)
            }
        }

        
        var barButtonAttributeDict:[String:AnyObject] = [:]

        if let fontName = styleDictionary[ZASStyleKeys.BarButtonFontName] as? String, let fontSize = styleDictionary[ZASStyleKeys.BarButtonFontSize] as? CGFloat {
            if let font:UIFont = UIFont(name: fontName, size: fontSize) {
                barButtonAttributeDict[NSFontAttributeName] = font
            }
        }
        
        if let textColor = styleDictionary[ZASStyleKeys.BarButtonTextColor] as? HexColor {
            barButtonAttributeDict[NSForegroundColorAttributeName] = UIColor(hexColor: textColor)
        }
        
        if let items = self.items {
            for item in items {
                if let rightBarButtonItems = item.rightBarButtonItems {
                    for barButtonItem in rightBarButtonItems {
                        barButtonItem.setTitleTextAttributes(barButtonAttributeDict, forState: UIControlState.Normal)
                    }
                }
                if let leftBarButtonItems = item.leftBarButtonItems {
                    for barButtonItem in leftBarButtonItems {
                        barButtonItem.setTitleTextAttributes(barButtonAttributeDict, forState: UIControlState.Normal)
                    }
                }
            }
        }
	}
}
