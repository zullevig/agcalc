//
//  UISegmentedControl+ZAStyles.swift
//  ZAStyles
//
//  Created by Zachary Ullevig on 7/11/15.
//  Copyright (c) 2015 Zachary Ullevig. All rights reserved.
//

import UIKit


// goofy Obj-C nothing pointer for hacking a property into the UIView extension
private var mcFontSizePropertyKey: UInt8 = 0
private var mcFontNamePropertyKey: UInt8 = 0


extension UISegmentedControl {
	// recursively give all the views with a style assigned the style elements from our styles dictionary
	override func applyStyle() {
		super.applyStyle()
		
        if let styles = self.assignedSyles() {
			for styleDictionary:[ZASStyleKeys:Any] in styles {
				processStyleDictionary(styleDictionary)
			}
		}
	}
		
    override func clearStyle() {
        super.clearStyle()
        
        var attributeDictionary:[String:AnyObject] = [:]
        attributeDictionary[NSFontAttributeName] = UIFont.systemFontOfSize(UIFont.systemFontSize())
        attributeDictionary[NSForegroundColorAttributeName] = UIColor(hexColor: ZASSystemBlueHexColor)
        self.setTitleTextAttributes(attributeDictionary, forState: UIControlState.Normal)
    }
    
	// TODO: if this can be made an override without getting a not yet supported error, the applyStyle method can be collapsed into the superclass
	private func processStyleDictionary(styleDictionary:[ZASStyleKeys:Any]) {
		var attributeDictionary:[String:AnyObject] = [:]
		
		if let name = styleDictionary[ZASStyleKeys.FontName] as? ZASFontName, let size = styleDictionary[ZASStyleKeys.FontSize] as? CGFloat {
			if let font = UIFont(name: name.rawValue, size: size) {
				attributeDictionary[NSFontAttributeName] = font
			}
		}
		
		if let color = styleDictionary[ZASStyleKeys.TextColor] as? HexColor {
			attributeDictionary[NSForegroundColorAttributeName] = UIColor(hexColor: color)
		}

		if attributeDictionary.count > 0 {
			self.setTitleTextAttributes(attributeDictionary, forState: UIControlState.Normal)
		}
	}
}
