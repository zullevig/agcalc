//
//  ZASImageView.swift
//  ZAStyles
//
//  Created by Zachary Ullevig on 7/15/15.
//  Copyright © 2015 Zachary Ullevig. All rights reserved.
//

import UIKit

@IBDesignable
class ZASImageView: UIImageView {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.applyStyle()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.applyStyle()
    }
}
