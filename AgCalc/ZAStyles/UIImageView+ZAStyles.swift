//
//  UIImageView+ZAStyles.swift
//  ZAStyles
//
//  Created by Zachary Ullevig on 11/8/15.
//  Copyright © 2015 Zachary Ullevig. All rights reserved.
//

import UIKit


extension UIImageView {
    override func applyStyle() {
        super.applyStyle()
        
        if let styles = self.assignedSyles() {
            for styleDictionary:[ZASStyleKeys:Any] in styles {
                processStyleDictionary(styleDictionary)
            }
        }
    }
    
    override func clearStyle() {
        super.clearStyle()
    }
    
    // TODO: if this can be made an override without getting a not yet supported error, the applyStyle method can be collapsed into the superclass
    private func processStyleDictionary(styleDictionary:[ZASStyleKeys:Any]) {
        if let cornerRadius = styleDictionary[ZASStyleKeys.BorderCornerRadius] as? CGFloat where cornerRadius > 0 {
            self.layer.masksToBounds = true;
        }
    }
}

