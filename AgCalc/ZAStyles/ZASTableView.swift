//
//  ZASTableView.swift
//  ZAStyles
//
//  Created by Zachary Ullevig on 7/11/15.
//  Copyright (c) 2015 Zachary Ullevig. All rights reserved.
//

import UIKit

@IBDesignable
class ZASTableView: UITableView {
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.applyStyle()
	}
	
//	override init(frame: CGRect) {
//		super.init(frame: frame)
//		self.applyStyle()
//	}
}
