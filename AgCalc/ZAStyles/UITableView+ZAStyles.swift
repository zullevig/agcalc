//
//  UITable+ZAStyles.swift
//  ZAStyles
//
//  Created by Zachary Ullevig on 7/11/15.
//  Copyright (c) 2015 Zachary Ullevig. All rights reserved.
//

import UIKit


// buttons have a couple unique styles to apply at the UIButton class level
extension UITableView {
	override func applyStyle() {
		super.applyStyle()
		
        if let styles = self.assignedSyles() {
			for styleDictionary:[ZASStyleKeys:Any] in styles {
				processStyleDictionary(styleDictionary)
			}
		}
	}
	
    override func clearStyle() {
        super.clearStyle()        
    }
    
	// TODO: if this can be made an override without getting a not yet supported error, the applyStyle method can be collapsed into the superclass
	private func processStyleDictionary(styleDictionary:[ZASStyleKeys:Any]) {
		if let indexColor:HexColor = styleDictionary[ZASStyleKeys.IndexColor] as? HexColor {
			self.sectionIndexColor = UIColor(hexColor: indexColor)
		}
	}
}
