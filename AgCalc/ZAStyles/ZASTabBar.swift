//
//  ZASTabBar.swift
//  ZAStyles
//
//  Created by Zachary Ullevig on 11/9/15.
//  Copyright © 2015 Zachary Ullevig. All rights reserved.
//

import UIKit

@IBDesignable
class ZASTabBar: UITabBar {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.applyStyle()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.applyStyle()
    }
}

