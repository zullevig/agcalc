//
//  UILabel+ZAStyles.swift
//  ZAStyles
//
//  Created by Zachary Ullevig on 7/11/15.
//  Copyright (c) 2015 Zachary Ullevig. All rights reserved.
//

import UIKit


extension UILabel {
	override func applyStyle() {
		super.applyStyle()
		
        if let styles = self.assignedSyles() {
			for styleDictionary:[ZASStyleKeys:Any] in styles {
				processStyleDictionary(styleDictionary)
			}
		}
	}
	
    override func clearStyle() {
        super.clearStyle()
        self.backgroundColor = UIColor.clearColor()
        self.font = UIFont.systemFontOfSize(UIFont.systemFontSize())
        self.textColor = UIColor.blackColor()
    }
    
	// TODO: if this can be made an override without getting a not yet supported error, the applyStyle method can be collapsed into the superclass
	private func processStyleDictionary(styleDictionary:[ZASStyleKeys:Any]) {
		
		if let name = styleDictionary[ZASStyleKeys.FontName] as? ZASFontName, let size = styleDictionary[ZASStyleKeys.FontSize] as? CGFloat {
			self.font = UIFont(name:name.rawValue, size:size)
		}
		
		if let textColor:HexColor = styleDictionary[ZASStyleKeys.TextColor] as? HexColor {
			self.textColor = UIColor(hexColor: textColor)
		}
	}
}

