//
//  UIColor+ZAStyles.swift
//  ZAStyles
//
//  Created by Zachary Ullevig on 7/11/15.
//  Copyright (c) 2015 Zachary Ullevig. All rights reserved.
//

import UIKit


public typealias HexColor = UInt32

public let ZASSystemBlueHexColor:HexColor = 0x004080

public extension HexColor {
    func applyAlpha(alphaPercentage:Float) -> HexColor {
        // get hex value for given percent of a 255 opaque alpha
        let alpha:HexColor = HexColor(0xFF * alphaPercentage)
        
        // replace the existing alpha value with the newly computed alpha value
        let newHexColor:HexColor = HexColor(self & 0x00FFFFFF + ((alpha << 24) & 0xFF000000))
        
        // return our resulting MCHexColor
        return newHexColor
    }
}

public extension UIColor {
	/**
		Initializes a new UIColor object directly from the hex colors provided by EUI

		:param: hexColor A hex code color definition of 8 characters, 2 each representing in order the Alpha, Red, Green, and Blue color components

		:returns: UIColor object
	*/
	convenience init(hexColor:HexColor) {
		let red:CGFloat = CGFloat((hexColor >> 16) & 0xff) / 255
		let green:CGFloat = CGFloat((hexColor >> 8) & 0xff) / 255
		let blue:CGFloat = CGFloat(hexColor & 0xff) / 255
		let alpha:CGFloat = CGFloat((hexColor >> 24) & 0xff) / 255
		self.init(red:red, green:green, blue:blue, alpha: alpha)
	}
}

