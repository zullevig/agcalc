//
//  UITabBar+ZAStyles.swift
//  ZAStyles
//
//  Created by Zachary Ullevig on 7/11/15.
//  Copyright (c) 2015 Zachary Ullevig. All rights reserved.
//

import UIKit


extension UITabBar {
	override func applyStyle() {
		super.applyStyle()
		
        if let styles = self.assignedSyles() {
			for styleDictionary:[ZASStyleKeys:Any] in styles {
				processStyleDictionary(styleDictionary)
			}
		}
	}
	
    override func clearStyle() {
        //super.clearStyle()  // UITabBars don't like restyling as currently implemented
    }
    
	// TODO: if this can be made an override without getting a not yet supported error, the applyStyle method can be collapsed into the superclass
	private func processStyleDictionary(styleDictionary:[ZASStyleKeys:Any]) {
		var attributeDict:[String:AnyObject] = [:]
        self.layer.masksToBounds = false;

		if let fontName = styleDictionary[ZASStyleKeys.FontName] as? String, let fontSize = styleDictionary[ZASStyleKeys.FontSize] as? CGFloat {
			if let font:UIFont = UIFont(name: fontName, size: fontSize) {
				attributeDict[NSFontAttributeName] = font
			}
		}

		if let textColor = styleDictionary[ZASStyleKeys.TextColor] as? HexColor {
			attributeDict[NSForegroundColorAttributeName] = UIColor(hexColor: textColor)
		}
        
        if let items = self.items {
            for item in items {
                item.setTitleTextAttributes(attributeDict, forState: UIControlState.Normal)
                item.setTitleTextAttributes(attributeDict, forState: UIControlState.Selected)
            }
        }

        
		if let tintColor = styleDictionary[ZASStyleKeys.TintColor] as? HexColor {
            self.backgroundImage = self.getImageWithColor(UIColor(hexColor: tintColor), size: self.frame.size)
		}

        if let gradientColors:[HexColor] = styleDictionary[ZASStyleKeys.GradientColors] as? [HexColor] {
            if gradientColors.count > 1 && self.frame.width > 0 {
                
                if self.backgroundGradient != nil {
                    backgroundGradient!.removeFromSuperlayer()
                }
                self.backgroundGradient = CAGradientLayer()
                
                let colorTop = UIColor(hexColor: gradientColors[0]).CGColor
                let colorBottom = UIColor(hexColor: gradientColors[1]).CGColor
                
                // apply gradient to title bar
                self.backgroundGradient!.colors = [colorTop, colorBottom]
                self.backgroundGradient!.frame = self.bounds
                
                UIGraphicsBeginImageContext(self.bounds.size);
                self.backgroundGradient!.renderInContext(UIGraphicsGetCurrentContext()!)
                let gradientImage = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                
                self.backgroundImage = gradientImage
            }
        }

        if let selectionColor = styleDictionary[ZASStyleKeys.SelectionColor] as? HexColor {
            var size = self.frame.size
            size.width = 100.0
            if let image = self.getImageWithColor(UIColor(hexColor: selectionColor), size: size) {
                self.selectionIndicatorImage = image
            }
        }

        if let selectionColor = styleDictionary[ZASStyleKeys.SelectionIconColor] as? HexColor {
            self.tintColor = UIColor(hexColor: selectionColor)
        }
    }
    
    func getImageWithColor(color: UIColor, size: CGSize) -> UIImage? {
        var image: UIImage? = nil
        
        let rect = CGRectMake(0, 0, size.width, size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }

}
