//
//  UIView+ZAStyles.swift
//  ZAStyles
//
//  Created by Zachary Ullevig on 7/11/15.
//  Copyright (c) 2015 Zachary Ullevig. All rights reserved.
//

import UIKit

// goofy Obj-C nothing pointer for hacking a property into the UIView extension
private var viewStylePropertyKey: UInt8 = 0
private var keyOverridePropertyKey: UInt8 = 0
private var lineColorPropertyKey: UInt8 = 0
private var backgroundGradientPropertyKey: UInt8 = 0


extension UIView {
    
	// this replaces using subclasses for tagging style types in IB
	@IBInspectable var style: String? {
		get {
			return objc_getAssociatedObject(self, &viewStylePropertyKey) as? String
		}
		set(newValue) {
            objc_setAssociatedObject(self, &viewStylePropertyKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
			self.applyStyle()
		}
	}
	
    @IBInspectable var keyOverride: String? {
        get {
            return objc_getAssociatedObject(self, &keyOverridePropertyKey) as? String
        }
        set(newValue) {
            objc_setAssociatedObject(self, &keyOverridePropertyKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
            self.applyStyle()
        }
    }
    
	var lineColor: UIColor? {
		get {
			return objc_getAssociatedObject(self, &lineColorPropertyKey) as? UIColor
		}
		set(newValue) {
            objc_setAssociatedObject(self, &lineColorPropertyKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
		}
	}
	
    var backgroundGradient: CAGradientLayer? {
        get {
            return objc_getAssociatedObject(self, &backgroundGradientPropertyKey) as? CAGradientLayer
        }
        set(newValue) {
            objc_setAssociatedObject(self, &backgroundGradientPropertyKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    class func styleKey() -> String {
        let styleKey:String
        let className = NSStringFromClass(self)
        let classNameParts = className.componentsSeparatedByString(".")
        if classNameParts.count > 0 {
            styleKey = classNameParts[classNameParts.count-1]
        }
        else {
            styleKey = className
        }
        return styleKey
    }
    
	
	/// recursively give all the views with a style assigned the style elements from our styles dictionary
    func applyStyle() {
        for subview:UIView in self.subviews {
            subview.applyStyle()
        }
        
        // remove previous "sticky" styles
        self.clearStyle()
        
        if "\(self.dynamicType)" == "_UIBackdropView" {
            // logging occurance to help understand when this is happening
            print("Ignoring _UIBackdropView")
        }
        else if let styles = self.assignedSyles() {
            for styleItem:ZASStyleItem in styles {
                processStyleDictionary(styleItem)
            }
        }
    }
    
    func clearStyle() {
        if self.backgroundGradient != nil {
            self.backgroundGradient!.removeFromSuperlayer()
            self.backgroundGradient = nil
        }

        self.layer.borderColor = UIColor.clearColor().CGColor
        self.layer.borderWidth = 0
        self.layer.cornerRadius = 0
        self.lineColor = UIColor.clearColor()
        self.layer.shadowColor = UIColor.clearColor().CGColor
        self.layer.shadowOffset = CGSize(width: 0,height: 0)
        self.layer.shadowOpacity = 0
        self.layer.shadowRadius = 0
    }
    
    private func styleKeyForUse() -> String {
        let styleKey:String
        
        if let override = self.keyOverride {
            styleKey = override
        }
        else {
            let className = NSStringFromClass(self.dynamicType)
            let classNameParts = className.componentsSeparatedByString(".")
            if classNameParts.count > 0 {
                styleKey = classNameParts[classNameParts.count-1]
            }
            else {
                styleKey = className
            }
        }
        
        return styleKey
    }
    
    func assignedSyles() -> [[ZASStyleKeys:Any]]? {
		var stylesArray:[[ZASStyleKeys:Any]]? = nil
        
		if let assignedStyle:String = self.style {
            let styles:[String] = assignedStyle.characters.split { $0 == "," }.map { String($0) }
			for style:String in styles {
                let type = self.styleKeyForUse()
                if let brandingDictionary:ZASStyleDictionary = ZAStyleManager.sharedInstance.styleDictionary(type) {
                    if let styleDictionary = brandingDictionary[style] {
                        if stylesArray == nil {
                            stylesArray = [[ZASStyleKeys:Any]]()
                        }
                        
                        stylesArray!.append(styleDictionary)
                    }
                }
			}
		}
		
		return stylesArray
	}
	
	private func processStyleDictionary(styleDictionary:[ZASStyleKeys:Any]) {
		if let backgroundColor:HexColor = styleDictionary[ZASStyleKeys.BackgroundColor] as? HexColor {
			self.backgroundColor = UIColor(hexColor: backgroundColor)
		}
        
        if let gradientColors:[HexColor] = styleDictionary[ZASStyleKeys.GradientColors] as? [HexColor] where !self.isKindOfClass(UINavigationBar) {
            if gradientColors.count > 1 && self.frame.width > 0 {
                
                if self.backgroundGradient != nil {
                    backgroundGradient!.removeFromSuperlayer()
                }
                self.backgroundGradient = CAGradientLayer()
                
                var colorArray:[CGColor] = []
                for color:HexColor in gradientColors {
                    colorArray.append(UIColor(hexColor: color).CGColor)
                }
                
                // apply gradient to title bar
                backgroundGradient!.colors = colorArray
                backgroundGradient!.frame = self.bounds
                self.layer.insertSublayer(backgroundGradient!, atIndex: 0)
            }
        }
		
		if let borderColor = styleDictionary[ZASStyleKeys.BorderColor] as? HexColor {
			self.layer.borderColor = UIColor(hexColor: borderColor).CGColor
		}
		
		if let borderWidth = styleDictionary[ZASStyleKeys.BorderWidth] as? CGFloat {
			self.layer.borderWidth = borderWidth
		}
		
		if let shape = styleDictionary[ZASStyleKeys.Shape] as? ZASViewShape {
            self.layer.masksToBounds = true;

			// ignore any assigned cornerRadius value and create circular ends with the shortest dimension of the view
			switch shape {
                case .Circle:
                    let width = self.frame.width
                    let height = self.frame.height
                    
                    if self.frame.width < self.frame.height {
                        self.bounds = CGRectInset(self.frame, 0.0, (height-width)/2);
                        //self.frame = CGRect(x: x, y: y, width: width, height: width)
                        self.layer.cornerRadius = self.frame.width / 2
                    }
                    else {
                        self.bounds = CGRectInset(self.frame, (width-height)/2, 0.0);
                        //self.frame = CGRect(x: x, y: y, width: height, height: height)
                        self.layer.cornerRadius = self.frame.height / 2
                    }
                    
                case .Pill:
                    if self.frame.width < self.frame.height {
                        self.layer.cornerRadius = self.frame.width / 2
                    }
                    else {
                        self.layer.cornerRadius = self.frame.height / 2
                    }
				
				default:
					// If we don't know what to do, go back to the cornerRadius setting if present
					if let cornerRadius = styleDictionary[ZASStyleKeys.BorderCornerRadius] as? CGFloat {
						self.layer.cornerRadius = cornerRadius
					}
			}
		}
		else if let cornerRadius = styleDictionary[ZASStyleKeys.BorderCornerRadius] as? CGFloat {
			self.layer.cornerRadius = cornerRadius
		}
		
		if let lineColor = styleDictionary[ZASStyleKeys.LineColor] as? HexColor {
			self.lineColor = UIColor(hexColor: lineColor)
		}
		
		if let tintColor = styleDictionary[ZASStyleKeys.TintColor] as? HexColor {
			self.tintColor = UIColor(hexColor: tintColor)
		}

        if let shadowColor = styleDictionary[ZASStyleKeys.ShadowColor] as? HexColor {
            self.layer.shadowColor = UIColor(hexColor: shadowColor).CGColor
        }
        
        if let shadowOffset = styleDictionary[ZASStyleKeys.ShadowOffset] as? CGSize {
            self.layer.shadowOffset = shadowOffset
        }
        
        if let shadowOpacity = styleDictionary[ZASStyleKeys.ShadowOpacity] as? Float {
            self.layer.shadowOpacity = shadowOpacity
        }
        
        if let shadowRadius = styleDictionary[ZASStyleKeys.ShadowRadius] as? CGFloat {
            self.layer.shadowRadius = shadowRadius
        }
    }
	
}
