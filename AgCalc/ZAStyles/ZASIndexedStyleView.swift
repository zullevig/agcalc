//
//  ZASIndexedStyleView.swift
//  ZAStyles
//
//  Created by Zachary Ullevig on 7/11/15.
//  Copyright (c) 2015 Zachary Ullevig. All rights reserved.
//

import UIKit


/**
	Defines a view representing an element from and indexed array with the ability to assign unique colors to each item's view background
*/
class ZASIndexedStyleView:ZASView {

	var backgroundColors:[HexColor]?

	/**
		Checks for a color hexValue assigned at the given array index, and if found, assigns that color to the view's background
		
		:param: index An index into the view's backgroundColors array.  If index is greater than the array size, the index will loop around the array to the beginning as necessary
	*/
	func setBackgroundColorIndex(index:Int) {
		if let hexColorArray = self.backgroundColors {
			let colorIndex = index % hexColorArray.count
			let hexColor = hexColorArray[colorIndex]
			self.backgroundColor = UIColor(hexColor: hexColor)
		}
	}

	override func applyStyle() {
		super.applyStyle()
		
        if let styles = self.assignedSyles() {
			for styleDictionary:[ZASStyleKeys:Any] in styles {
				processStyleDictionary(styleDictionary)
			}
		}
	}
	
	private func processStyleDictionary(styleDictionary:[ZASStyleKeys:Any]) {
		if let arrayBackgroundColor:[HexColor] = styleDictionary[ZASStyleKeys.ArrayBackgroundColor] as? [HexColor] {
			self.backgroundColors = arrayBackgroundColor
			self.setBackgroundColorIndex(0)
		}
	}

}
