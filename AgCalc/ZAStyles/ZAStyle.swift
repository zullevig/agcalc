//
//  ZAStyle.swift
//  ZAStyles
//
//  Created by Zachary Ullevig on 7/11/15.
//  Copyright (c) 2015 Zachary Ullevig. All rights reserved.
//

import UIKit

typealias ZASStyleItem = [ZASStyleKeys:Any]
typealias ZASStyleDictionary = [String:ZASStyleItem]
typealias ZASStyleSheets = [String:ZASStyleDictionary]
typealias ZASStyleSet = [String:ZASStyleSheets]


let ZASDefaultStyleSet = "ZASDefaultStyleSet"


class ZAStyleManager {
    var styleSet = ZASDefaultStyleSet
    var styleSets:ZASStyleSet = [:]
    
    class var sharedInstance: ZAStyleManager {
        return _ZAStyleManager
    }
    
    private init() {
        styleSets[ZASDefaultStyleSet] = [:]
    }
    
    convenience init(styleSheets:ZASStyleSheets) {
        self.init()
        
        for (key, value) in styleSheets {
            self.registerStyle(key, style: value)
        }
    }
    
     func registerStyles(set:String, styleSheets:ZASStyleSheets) {
        for (key, value) in styleSheets {
            self.registerStyle(set, type:key, style: value)
        }
    }
    
    func registerStyle(type:String, style:ZASStyleDictionary) {
        self.registerStyle(ZASDefaultStyleSet, type: type, style: style)
    }
    
    func registerStyle(set:String, type:String, style:ZASStyleDictionary) {
        if self.styleSets[set] == nil {
            self.styleSets[set] = [:]
        }
        
        if self.styleSets[set] != nil {
            self.styleSets[set]![type] = style
        }
    }
    
    func styleDictionary(type:String) -> ZASStyleDictionary? {
        return self.styleSets[self.styleSet]?[type]
    }

    func styleDictionary(set:String, type:String) -> ZASStyleDictionary? {
        return self.styleSets[set]?[type]
    }
}


// enum of style dictionary key names for code completion
public enum ZASStyleKeys:String {
	case ArrayBackgroundColor  = "ArrayBackgroundColor"
	case ArrayBorderColor      = "ArrayBorderColor"
	case BackFontName          = "BackFontName"
	case BackFontSize          = "BackFontSize"
	case BackgroundColor       = "BackgroundColor"
    case BackTextColor         = "BackTextColor"
    case BarButtonFontName     = "BarButtonFontName"
    case BarButtonFontSize     = "BarButtonFontSize"
    case BarButtonTextColor    = "BarButtonTextColor"
	case BarTintColor          = "BarTintColor"
	case BorderColor           = "BorderColor"
	case BorderWidth           = "BorderWidth"
	case BorderCornerRadius    = "BorderCornerRadius"
	case FontName              = "FontName"
	case FontSize              = "FontSize"
    case GradientColors        = "GradientColors"
	case IndexColor            = "IndexColor"
	case ImagePosition         = "ImagePosition"
	case LineColor             = "LineColor"
	case LineWidth             = "LineWidth"
	case ProgressTintColor     = "ProgressTintColor"
    case SelectionColor        = "SelectionColor"
    case SelectionIconColor    = "SelectionIconColor"
    case ShadowColor           = "ShadowColor"
    case ShadowOffset          = "ShadowOffset"
    case ShadowOpacity         = "ShadowOpacity"
    case ShadowRadius          = "ShadowRadius"
	case Shape                 = "Shape"
	case TextColor             = "TextColor"
	case TintColor             = "TintColor"
}

public enum ZASImagePosition {
	case Left
	case Right
}

// used to enforce specific view shapes via branding styles
public enum ZASViewShape:String {
	case Freeform = "freeform"
    case Circle   = "circle"
    case Pill     = "pill"
}

// enum to allow code completion on font names
// more can be added from: http://iosfonts.com
public enum ZASFontName:String {
    case AcademyEngraved                      = "AcademyEngravedLetPlain"
    case AmericanTypewriter                   = "AmericanTypewriter"
    case AmericanTypewriterBold               = "AmericanTypewriter-Bold"
    case AmericanTypewriterCondensed          = "AmericanTypewriter-Condensed"
    case AmericanTypewriterCondensedBold      = "AmericanTypewriter-CondensedBold"
    case AmericanTypewriterCondensedLight     = "AmericanTypewriter-CondensedLight"
    case AmericanTypewriterLight              = "AmericanTypewriter-Light"
    case Arial                                = "ArialMT"
    case ArialBold                            = "Arial-BoldMT"
    case ArialBoldItalic                      = "Arial-BoldItalicMT"
    case ArialItalic                          = "Arial-ItalicMT"
    case ArialRounded                         = "ArialRoundedMTBold"
    case Chalkboard                           = "ChalkboardSE-Regular"
    case ChalkboardBold                       = "ChalkboardSE-Bold"
    case ChalkboardLight                      = "ChalkboardSE-Light"
    case Chalkduster                          = "Chalkduster"
    case Copperplate                          = "Copperplate"
    case CopperplateBold                      = "Copperplate-Bold"
    case CopperplateLight                     = "Copperplate-Light"
    case Courier                              = "CourierNewPSMT"
    case CourierBold                          = "CourierNewPS-BoldMT"
    case CourierBoldItalic                    = "CourierNewPS-BoldItalicMT"
    case CourierItalic                        = "CourierNewPS-ItalicMT"
    case Futura                               = "Futura-Medium"
    case FuturaCondensed                      = "Futura-CondensedMedium"
    case FuturaCondensedBold                  = "Futura-CondensedExtraBold"
    case FuturaItalic                         = "Futura-MediumItalic"
    case Georgia                              = "Georgia"
    case GeorgiaBold                          = "Georgia-Bold"
    case GeorgiaBoldItalic                    = "Georgia-BoldItalic"
    case GeorgiaItalic                        = "Georgia-Italic"
    case GillSans                             = "GillSans"
    case GillSansBold                         = "GillSans-Bold"
    case GillSansBoldItalic                   = "GillSans-BoldItalic"
    case GillSansItalic                       = "GillSans-Italic"
    case GillSansLight                        = "GillSans-Light"
    case GillSansLightItalic                  = "GillSans-LightItalic"
	case Helvetica                            = "HelveticaNeue"
    case HelveticaBold                        = "HelveticaNeue-Bold"
    case HelveticaBoldItalic                  = "HelveticaNeue-BoldItalic"
    case Times                                = "TimesNewRomanPSMT"
    case TimesBold                            = "TimesNewRomanPS-BoldMT"
    case TimesBoldItalic                      = "TimesNewRomanPS-BoldItalicMT"
    case TimesItalic                          = "TimesNewRomanPS-ItalicMT"
    case Verdana                              = "Verdana"
    case VerdanaBold                          = "Verdana-Bold"
    case VerdanaBoldItalic                    = "Verdana-BoldItalic"
    case VerdanaItalic                        = "Verdana-Italic"
}
