//
//  UICollectionViewCell+ZAStyles.swift
//  ZAStyles
//
//  Created by Zachary Ullevig on 7/14/15.
//  Copyright © 2015 Zachary Ullevig. All rights reserved.
//

import UIKit


extension UICollectionViewCell {
    override func applyStyle() {
        super.applyStyle()
        
        if let styles = self.assignedSyles() {
            for styleDictionary:[ZASStyleKeys:Any] in styles {
                processStyleDictionary(styleDictionary)
            }
        }
    }
    
    override func clearStyle() {
        super.clearStyle()
        self.backgroundColor = UIColor.clearColor()
        self.layer.borderColor = UIColor.clearColor().CGColor
        self.layer.borderWidth = 0
        self.layer.cornerRadius = 0
        self.lineColor = UIColor.clearColor()
        self.tintColor = UIColor.clearColor()
    }
    
    // TODO: if this can be made an override without getting a not yet supported error, the applyStyle method can be collapsed into the superclass
    private func processStyleDictionary(styleDictionary:[ZASStyleKeys:Any]) {
        if let backgroundColor:HexColor = styleDictionary[ZASStyleKeys.BackgroundColor] as? HexColor {
            self.backgroundColor = UIColor(hexColor: backgroundColor)
        }
        
        if let borderColor = styleDictionary[ZASStyleKeys.BorderColor] as? HexColor {
            self.layer.borderColor = UIColor(hexColor: borderColor).CGColor
        }
        
        if let borderWidth = styleDictionary[ZASStyleKeys.BorderWidth] as? CGFloat {
            self.layer.borderWidth = borderWidth
        }
        
        if let shape = styleDictionary[ZASStyleKeys.Shape] as? ZASViewShape {
            // ignore any assigned cornerRadius value and create circular ends with the shortest dimension of the view
            switch shape {
            case .Circle:
                let width = self.frame.width
                let height = self.frame.height
                let x = self.frame.origin.x
                let y = self.frame.origin.y

                if self.frame.width < self.frame.height {
                    self.frame = CGRect(x: x, y: y, width: width, height: width)
                    self.layer.cornerRadius = self.frame.width / 2
                }
                else {
                    self.frame = CGRect(x: x, y: y, width: height, height: height)
                    self.layer.cornerRadius = self.frame.height / 2
                }
                
            case .Pill:
                if self.frame.width < self.frame.height {
                    self.layer.cornerRadius = self.frame.width / 2
                }
                else {
                    self.layer.cornerRadius = self.frame.height / 2
                }
                
            default:
                // If we don't know what to do, go back to the cornerRadius setting if present
                if let cornerRadius = styleDictionary[ZASStyleKeys.BorderCornerRadius] as? CGFloat {
                    self.layer.cornerRadius = cornerRadius
                }
            }
        }
        else if let cornerRadius = styleDictionary[ZASStyleKeys.BorderCornerRadius] as? CGFloat {
            self.layer.cornerRadius = cornerRadius
        }
        
        if let lineColor = styleDictionary[ZASStyleKeys.LineColor] as? HexColor {
            self.lineColor = UIColor(hexColor: lineColor)
        }
        
        if let tintColor = styleDictionary[ZASStyleKeys.TintColor] as? HexColor {
            self.tintColor = UIColor(hexColor: tintColor)
        }
    }

}
