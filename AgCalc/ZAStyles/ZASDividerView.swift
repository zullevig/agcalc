//
//  ZASDividerView.swift
//  ZAStyles
//
//  Created by Zachary Ullevig on 7/11/15.
//  Copyright (c) 2015 Zachary Ullevig. All rights reserved.
//

import UIKit


private var lineWidthPropertyKey: UInt8 = 0


@IBDesignable
class ZASDividerView: ZASView {

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.applyStyle()
	}
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		self.applyStyle()
	}

	var lineWidth: CGFloat? {
		get {
			return objc_getAssociatedObject(self, &lineWidthPropertyKey) as? CGFloat
		}
		set(newValue) {
            objc_setAssociatedObject(self, &lineWidthPropertyKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
		}
	}

	override func applyStyle() {
		super.applyStyle()
		
        if let styles = self.assignedSyles() {
			for styleDictionary:[ZASStyleKeys:Any] in styles {
				processStyleDictionary(styleDictionary)
			}
            self.setNeedsDisplay()
		}
	}

	override func drawRect(rect: CGRect) {
		let context = UIGraphicsGetCurrentContext()
		
		if let lineColor = self.lineColor, let lineWidth = self.lineWidth {
			CGContextSetFillColorWithColor(context, lineColor.CGColor);
			let strokedLineRect = CGRectMake(0.0, self.frame.size.height - lineWidth, self.frame.size.width, lineWidth);
			CGContextFillRect(context, strokedLineRect);
		}
	}
	
	private func processStyleDictionary(styleDictionary:[ZASStyleKeys:Any]) {
		if let lineWidth:CGFloat = styleDictionary[ZASStyleKeys.LineWidth] as? CGFloat {
			self.lineWidth = lineWidth
		}
        
        if let gradientColors:[HexColor] = styleDictionary[ZASStyleKeys.GradientColors] as? [HexColor] where !self.isKindOfClass(UINavigationBar) {
            if gradientColors.count > 1 && self.frame.width > 0 {
                self.layer.masksToBounds = true;
                
                if self.backgroundGradient != nil {
                    backgroundGradient!.removeFromSuperlayer()
                }
                self.backgroundGradient = CAGradientLayer()
                
                var colorArray:[CGColor] = []
                for color:HexColor in gradientColors {
                    colorArray.append(UIColor(hexColor: color).CGColor)
                }
                
                var currentFrame = self.bounds
                if let lineWidth:CGFloat = styleDictionary[ZASStyleKeys.LineWidth] as? CGFloat {
                    currentFrame.size.height -= lineWidth
                }

                // apply gradient to title bar
                backgroundGradient!.colors = colorArray
                backgroundGradient!.frame = currentFrame
                self.layer.insertSublayer(backgroundGradient!, atIndex: 0)
            }
        }
    }

}

