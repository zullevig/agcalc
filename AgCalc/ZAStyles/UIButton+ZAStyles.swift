//
//  UIButton+ZAStyles.swift
//  ZAStyles
//
//  Created by Zachary Ullevig on 7/11/15.
//  Copyright (c) 2015 Zachary Ullevig. All rights reserved.
//

import UIKit


// buttons have a couple unique styles to apply at the UIButton class level
extension UIButton {
	override func applyStyle() {
		super.applyStyle()
		
		if let styles = self.assignedSyles() {
			for styleDictionary:[ZASStyleKeys:Any] in styles {
				processStyleDictionary(styleDictionary)
			}
		}
	}
    
    override func clearStyle() {
        super.clearStyle()
        self.titleLabel?.font = UIFont.systemFontOfSize(UIFont.systemFontSize())
        self.setTitleColor(UIColor(hexColor: ZASSystemBlueHexColor), forState: .Normal)
        self.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        self.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        if self.backgroundGradient != nil {
            backgroundGradient!.removeFromSuperlayer()
            self.backgroundGradient = nil
        }
    }
	
	// TODO: if this can be made an override without getting a not yet supported error, the applyStyle method can be collapsed into the superclass
	private func processStyleDictionary(styleDictionary:[ZASStyleKeys:Any]) {
		
		if let name = styleDictionary[ZASStyleKeys.FontName] as? ZASFontName, let size = styleDictionary[ZASStyleKeys.FontSize] as? CGFloat {
			self.titleLabel?.font = UIFont(name:name.rawValue, size:size)
		}
		
		if let textColor:HexColor = styleDictionary[ZASStyleKeys.TextColor] as? HexColor {
			self.setTitleColor(UIColor(hexColor: textColor), forState: .Normal)
		}
		
		// handle right-aligned button icons
		if let image = self.imageView?.image, let imagePosition = styleDictionary[ZASStyleKeys.ImagePosition] as? ZASImagePosition {
			if imagePosition == .Right {
				self.imageEdgeInsets = UIEdgeInsetsMake(0, self.frame.size.width-image.size.width, 0, 0)
				self.titleEdgeInsets = UIEdgeInsetsMake(0, -image.size.width-16, 0, 0)
			}
		}
        
        if let gradientColors:[HexColor] = styleDictionary[ZASStyleKeys.GradientColors] as? [HexColor] where !self.isKindOfClass(UINavigationBar) {
            if gradientColors.count > 1 && self.frame.width > 0 {
                self.layer.masksToBounds = true;

                if self.backgroundGradient != nil {
                    backgroundGradient!.removeFromSuperlayer()
                    self.backgroundGradient = nil
                }
                self.backgroundGradient = CAGradientLayer()
                
                var colorArray:[CGColor] = []
                for color:HexColor in gradientColors {
                    colorArray.append(UIColor(hexColor: color).CGColor)
                }
                
                // apply gradient to title bar
                backgroundGradient!.colors = colorArray
                backgroundGradient!.frame = self.bounds
                self.layer.insertSublayer(backgroundGradient!, atIndex: 0)
            }
        }
	}
}
