//
//  SettingsTableViewController.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 12/5/14.
//  Copyright (c) 2014 Zachary Ullevig. All rights reserved.
//

import UIKit
import CoreData


class SettingsTableViewController: PriceAwareTableViewController, UITextFieldDelegate {
	
    enum TableSection:Int {
        case PremiumFeatures = 0
        case PriceUpdate = 1
        case SilverPieces = 2
        case MediumOfExchange = 3
        case UnitOfMass = 4
    }
    
    enum PriceUpdateRow:Int {
        case SpotBid = 0
        case SpotAsk = 1
        case SpotMid = 2
        case LondonFix = 3
        case ManualUpdate = 4
    }

    enum MediumOfExchangeRow:Int {
        case Silver = 0
        case USDollar = 1
    }
    
    enum UnitOfMassRow:Int {
        case TroyOunce = 0
        case Gram = 1
    }
    
    // MARK: - IBOutlets

    @IBOutlet weak var bidLabel: UILabel!
    @IBOutlet weak var bidTimestampLabel: UILabel!
    @IBOutlet weak var midLabel: UILabel!
    @IBOutlet weak var midTimestampLabel: UILabel!
    @IBOutlet weak var askLabel: UILabel!
    @IBOutlet weak var askTimestampLabel: UILabel!
    @IBOutlet weak var londonFixLabel: UILabel!
    @IBOutlet weak var londonFixTimestampLabel: UILabel!
    
    @IBOutlet weak var bidPriceUpdateModeCell: UITableViewCell!
    @IBOutlet weak var midPriceUpdateModeCell: UITableViewCell!
    @IBOutlet weak var askPriceUpdateModeCell: UITableViewCell!
    @IBOutlet weak var londonFixPriceUpdateModeCell: UITableViewCell!

    @IBOutlet weak var manualPriceUpdateModeCell: UITableViewCell!
    @IBOutlet weak var manualSilverPriceField: UITextField!
    @IBOutlet weak var silverMediumOfExchangeCell: UITableViewCell!
    @IBOutlet weak var usDollarMediumOfExchangeCell: UITableViewCell!
    @IBOutlet weak var troyOunceUnitOfMassCell: UITableViewCell!
    @IBOutlet weak var gramUnitOfMassCell: UITableViewCell!

    
    // MARK: - UIViewController overrides

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.updateTableContent()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    // MARK: - Price Aware ViewController overrides

    override func updatePriceControls() {
        // allow triggering UI updates from any thread
        dispatch_async(dispatch_get_main_queue(),{
			if let latestPrice = UserSettings.sharedInstance.spotPrices, silverPrice = latestPrice[.Silver] {
				self.bidLabel.text = String(format:"$%.2f", silverPrice.bid)
                self.bidTimestampLabel.text = "@\(silverPrice.timestampString())"
				self.midLabel.text = String(format:"$%.2f", silverPrice.price)
                self.midTimestampLabel.text = "@\(silverPrice.timestampString())"
				self.askLabel.text = String(format:"$%.2f", silverPrice.ask)
				self.askTimestampLabel.text = "@\(silverPrice.timestampString())"
			}
			else {
				self.midLabel.text = "Price Unavailable"
                self.midLabel.text = "Price Unavailable"
                self.askLabel.text = "Price Unavailable"
                self.bidTimestampLabel.text = ""
                self.midTimestampLabel.text = ""
                self.askTimestampLabel.text = ""
			}
            
            if let fixPrice = UserSettings.sharedInstance.londonFixedPrices, silverPrice = fixPrice[.Silver] {
                self.londonFixLabel.text = String(format:"$%.2f", silverPrice.price)
                self.londonFixTimestampLabel.text = "@\(silverPrice.timestampString())"
            }
        });
    }
    
    // MARK: - UITableViewController delegate methods

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.manualSilverPriceField.resignFirstResponder()
        
        switch indexPath.section {
        case TableSection.PriceUpdate.rawValue:
            switch indexPath.row {
            case PriceUpdateRow.SpotBid.rawValue:
                UserSettings.sharedInstance.priceUpdateMode = .SpotBid
                
            case PriceUpdateRow.SpotAsk.rawValue:
                UserSettings.sharedInstance.priceUpdateMode = .SpotAsk
                
            case PriceUpdateRow.SpotMid.rawValue:
                UserSettings.sharedInstance.priceUpdateMode = .SpotMid
                
            case PriceUpdateRow.LondonFix.rawValue:
                UserSettings.sharedInstance.priceUpdateMode = .LondonFix
                
            case PriceUpdateRow.ManualUpdate.rawValue:
                UserSettings.sharedInstance.priceUpdateMode = .Manual

            default:
                print("Default")
            }
            
        case TableSection.SilverPieces.rawValue:
            break
            
        case TableSection.MediumOfExchange.rawValue:
            switch indexPath.row {
            case MediumOfExchangeRow.Silver.rawValue:
                UserSettings.sharedInstance.mediumOfExchange = .Silver
                
            case MediumOfExchangeRow.USDollar.rawValue:
                UserSettings.sharedInstance.mediumOfExchange = .USDollar
                
            default:
                print("Default")
            }
            
        case TableSection.UnitOfMass.rawValue:
            switch indexPath.row {
            case UnitOfMassRow.TroyOunce.rawValue:
                UserSettings.sharedInstance.unitOfMass = .TroyOunce
                
            case UnitOfMassRow.Gram.rawValue:
                UserSettings.sharedInstance.unitOfMass = .Gram
                
            default:
                print("Default")
            }
            
        default:
            print("Default")
        }
        
        tableView.deselectRowAtIndexPath(indexPath, animated:false)
        self.updateTableContent()
    }
    
    
    // MARK: - UITextField delegate methods

    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        var result:Bool = false
        if range.location == 0 && string.characters.count == 0 {
            result = true
        }
        else if let fieldText:NSString = textField.text {
            let fullString:NSString = fieldText.stringByReplacingCharactersInRange(range, withString: string)
            let replaceNumber = NSNumberFormatter().numberFromString(fullString as String)
            
            if replaceNumber != nil && replaceNumber?.floatValue < 100000 {
                result = true
            }
        }
        
        return result
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        self.updateManualPrice()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.manualSilverPriceField.resignFirstResponder()

        self.updateManualPrice()
        return true
    }
    
    
    // MARK: - General methods

    func updateManualPrice() {
        if let fieldText = self.manualSilverPriceField.text, let parsedNumber = NSNumberFormatter().numberFromString(fieldText) {
            let managedContext = coreDataStack.currentManagedObjectContext()
            
            // save manual silver price
            if let managedObject = NSEntityDescription.insertNewObjectForEntityForName("ManualPrice", inManagedObjectContext: managedContext) as? ManualPrice {
                managedObject.config(.Silver, price: parsedNumber.floatValue)
                UserSettings.sharedInstance.manualPrice = managedObject
            }
        }
    }
    
    func updateTableContent() {
        switch UserSettings.sharedInstance.mediumOfExchange {
        case .Silver:
            self.silverMediumOfExchangeCell.accessoryType = .Checkmark
            self.usDollarMediumOfExchangeCell.accessoryType = .None
            
        case .USDollar:
            self.silverMediumOfExchangeCell.accessoryType = .None
            self.usDollarMediumOfExchangeCell.accessoryType = .Checkmark
        }
        
        switch UserSettings.sharedInstance.unitOfMass {
        case .TroyOunce:
            self.troyOunceUnitOfMassCell.accessoryType = .Checkmark
            self.gramUnitOfMassCell.accessoryType = .None
            
        case .Gram:
            self.troyOunceUnitOfMassCell.accessoryType = .None
            self.gramUnitOfMassCell.accessoryType = .Checkmark
         }
        
        switch UserSettings.sharedInstance.priceUpdateMode {
        case .SpotBid:
            self.bidPriceUpdateModeCell.accessoryType = .Checkmark
            self.askPriceUpdateModeCell.accessoryType = .None
            self.midPriceUpdateModeCell.accessoryType = .None
            self.londonFixPriceUpdateModeCell.accessoryType = .None
            self.manualPriceUpdateModeCell.accessoryType = .None
            
        case .SpotAsk:
            self.bidPriceUpdateModeCell.accessoryType = .None
            self.askPriceUpdateModeCell.accessoryType = .Checkmark
            self.midPriceUpdateModeCell.accessoryType = .None
            self.londonFixPriceUpdateModeCell.accessoryType = .None
            self.manualPriceUpdateModeCell.accessoryType = .None
            
        case .SpotMid:
            self.bidPriceUpdateModeCell.accessoryType = .None
            self.askPriceUpdateModeCell.accessoryType = .None
            self.midPriceUpdateModeCell.accessoryType = .Checkmark
            self.londonFixPriceUpdateModeCell.accessoryType = .None
            self.manualPriceUpdateModeCell.accessoryType = .None
            
        case .LondonFix:
            self.bidPriceUpdateModeCell.accessoryType = .None
            self.askPriceUpdateModeCell.accessoryType = .None
            self.midPriceUpdateModeCell.accessoryType = .None
            self.londonFixPriceUpdateModeCell.accessoryType = .Checkmark
            self.manualPriceUpdateModeCell.accessoryType = .None
            
        case .Manual:
            self.bidPriceUpdateModeCell.accessoryType = .None
            self.askPriceUpdateModeCell.accessoryType = .None
            self.midPriceUpdateModeCell.accessoryType = .None
            self.londonFixPriceUpdateModeCell.accessoryType = .None
            self.manualPriceUpdateModeCell.accessoryType = .Checkmark
        }
        
        if let manualPrices = UserSettings.sharedInstance.manualPrices, silverPrice = manualPrices[.Silver] {
            self.manualSilverPriceField.text = String(format:"%.2f", silverPrice.price)
        }
        else {
            self.manualSilverPriceField.text = ""
        }
    }
    
}
