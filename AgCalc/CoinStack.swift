//
//  CoinStack.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 12/27/14.
//  Copyright (c) 2014 Zachary Ullevig. All rights reserved.
//

import Foundation


struct CoinStack {
    private var stackedCoins:[Coin:Int] = [Coin:Int]()
    
    mutating func addCoin(coin:Coin) {
        if self.stackedCoins[coin] == nil {
            stackedCoins[coin] = 1
        }
        else {
            stackedCoins[coin] = stackedCoins[coin]! + 1
        }
    }
    
    mutating func removeCoin(coin:Coin) {
        if stackedCoins[coin] != nil {
            if stackedCoins[coin]! == 1 {
                stackedCoins.removeValueForKey(coin)
            }
            else {
                stackedCoins[coin] = stackedCoins[coin]! - 1
            }
        }
    }
    
    mutating func removeAllCoins() {
        stackedCoins.removeAll(keepCapacity: true)
    }
    
    mutating func setCountForCoin(coin:Coin, count:Int) {
        if count == 0 {
            stackedCoins.removeValueForKey(coin)
        }
        else if count > 0 {
            stackedCoins[coin] = count
        }
        else {
            print("unexpected negative value")
        }
    }
    
    func countForCoin(coin:Coin) -> Int {
        var result:Int = 0
        if self.stackedCoins[coin] != nil {
            result = self.stackedCoins[coin]!
        }
        return result
    }
    
    func stackOunces(type:CommodityType) -> Float {
        var result:Float = 0
        
        for (coin, count): (Coin, Int) in self.stackedCoins {
            result += Float(count) * coin.troyOz(type)
        }

        return result
    }
    
    func stackGrams(type:CommodityType) -> Float {
        var result:Float = 0
        
        for (coin, count): (Coin, Int) in self.stackedCoins {
            result += Float(count) * coin.grams(type)
        }
        
        return result
    }
    
    func stackValue(prices:[CommodityType : CommodityPrice]) -> Float {
        var result: Float = 0
        
        for (type,price) in prices {
            result += self.stackOunces(type) * price.price
        }
        
        return result
    }
}