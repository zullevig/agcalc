//
//  SilverPiecesTableViewCell.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 12/22/14.
//  Copyright (c) 2014 Zachary Ullevig. All rights reserved.
//

import UIKit

class SilverPiecesTableViewCell: UITableViewCell {

    @IBOutlet weak var coinImageView: UIImageView!
    @IBOutlet weak var coinNameLabel: UILabel!
    @IBOutlet weak var activeSwitch: UISwitch!
    @IBOutlet weak var silverContentLabel: UILabel!
    @IBOutlet weak var meltValueLabel: UILabel!
    
    weak var coin:Coin? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func toggleActive(sender: AnyObject) {
        if let cellCoin = coin {
            cellCoin.active = NSNumber(bool:!cellCoin.active.boolValue)
        }
    }

    func configureCell(coin:Coin) {
        self.coin = coin
        
        if let image = coin.image {
            self.coinImageView.image = UIImage(named:image)
        }
        
        self.coinNameLabel.text = coin.name
        self.activeSwitch.on = coin.active.boolValue
        self.silverContentLabel.text = "\(coin.grams(.Silver))g"

        if let cellCoin = self.coin {
            let labelDetails:(text:String,font:UIFont?) = UserSettings.sharedInstance.meltValueLabel(cellCoin, mediumOfExchange: .USDollar)
            self.meltValueLabel.text = labelDetails.text
            self.meltValueLabel.font = labelDetails.font
        }
    }
}
