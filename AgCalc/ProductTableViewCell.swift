//
//  ProductTableViewCell.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 7/19/15.
//  Copyright © 2015 Zachary Ullevig. All rights reserved.
//

import UIKit
import StoreKit


class ProductTableViewCell: UITableViewCell  {
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var productDescription: UITextView!
    @IBOutlet weak var buyButton: UIButton!
    @IBOutlet weak var purchasedLabel: UILabel!
    
    weak var product:SKProduct? = nil
    weak var productController:PurchaseViewController? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func buy(sender: AnyObject) {
        if let product = self.product, productController = self.productController {
            productController.purchase(product)
        }

        // Use to manually test turning off ads
        // UserSettings.sharedInstance.adMode = AdMode.Off
        // self.productController?.productTable.reloadData()
    }
}
