//
//  FixPrice.swift
//  
//
//  Created by Zachary Ullevig on 5/8/16.
//
//

import Foundation
import CoreData


class FixPrice: CommodityPrice {

// Insert code here to add functionality to your managed object subclass
    
    override var price: Float {
        get {
            var result:Float = 0

            if self.pmPrice > 0 {
                result = self.pmPrice
            }
            else if self.amPrice > 0 {
                result = self.amPrice
            }
            else {
                result = dayPrice
            }
 
            return result
        }
    }

    func config(type:CommodityType, timestamp:NSTimeInterval, price:Float) {
        self.type = type.managedType()
        self.amPrice = 0
        self.pmPrice = 0
        self.dayPrice = price
        self.timestamp = timestamp
    }
    
    func config(type:CommodityType, timestamp:NSTimeInterval, amPrice:Float, pmPrice:Float) {
        self.type = type.managedType()
        self.amPrice = amPrice
        self.pmPrice = pmPrice
        self.dayPrice = 0
        self.timestamp = timestamp
    }
    
}
