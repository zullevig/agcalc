//
//  IssuersTableViewController.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 12/5/14.
//  Copyright (c) 2014 Zachary Ullevig. All rights reserved.
//

import UIKit


class IssuersTableViewController: UITableViewController {
	
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        appDelegate.reloadCachedValues()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        let result:Int
        
        if let currentIssuers = appDelegate.issuers {
            result = currentIssuers.count
        }
        else {
            result = 0
        }
        
        return result
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("issuerCellReuseIdentifier", forIndexPath: indexPath) as! IssuersTableViewCell

        // Configure the cell...
        if let currentIssuers = appDelegate.issuers {
            if indexPath.row < currentIssuers.count {
                let issuer = currentIssuers[indexPath.row]
                cell.issuer = issuer
                cell.tableView = self.tableView
                
                if let image = issuer.image {
                    cell.issuerImageView.image = UIImage(named:image)
                }
                
                cell.issuerLabel.text = issuer.name
                cell.activeSwitch.on = issuer.active.boolValue
                cell.selectCoinsView.hidden = !issuer.active.boolValue
                
                cell.dividerView.layer.shadowColor = UIColor.blackColor().CGColor
                cell.dividerView.layer.shadowOffset = CGSizeMake(0, 2)
                cell.dividerView.layer.shadowOpacity = 0.3
                cell.dividerView.layer.shadowRadius = 2
            }
        }

        return cell
    }

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        var height:CGFloat = 73
        
        if let currentIssuers = appDelegate.issuers {
            if indexPath.row < currentIssuers.count {
                let issuer = currentIssuers[indexPath.row]
                if issuer.active.boolValue == false {
                    height -= 25
                }
            }
        }

        return height
    }
    
    // MARK: - Navigation
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        var result = false
        if let indexPath = self.tableView.indexPathForSelectedRow {
            if let currentIssuers = appDelegate.issuers {
                if indexPath.row < currentIssuers.count {
                    let issuer = currentIssuers[indexPath.row]
                    result = issuer.active.boolValue
                }
            }
        }
        return result
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        let silverPiecesViewController = segue.destinationViewController as! SilverPiecesTableViewController
        if let indexPath = self.tableView.indexPathForSelectedRow {
            if let currentIssuers = appDelegate.issuers {
                if indexPath.row < currentIssuers.count {
                    let issuer = currentIssuers[indexPath.row]
                    silverPiecesViewController.issuer = issuer
                }
            }
        }
    }
}

