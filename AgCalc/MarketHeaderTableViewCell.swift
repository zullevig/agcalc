//
//  MarketHeaderTableViewCell.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 5/2/16.
//  Copyright © 2016 Zachary Ullevig. All rights reserved.
//

import UIKit

class MarketHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var marketLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
