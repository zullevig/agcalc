//
//  GridCollectionViewLayout.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 12/25/14.
//  Copyright (c) 2014 Zachary Ullevig. All rights reserved.
//

import UIKit

class GridCollectionViewLayout: UICollectionViewLayout {    
    let cellKey = "cellKey"

    var layoutInfo = [String:[NSIndexPath:UICollectionViewLayoutAttributes]]()

    var contentWidth:CGFloat = 0
    let minCellWidth:CGFloat = 135
    var cellHeight:CGFloat = 135
    var cellWidth:CGFloat = 150 
    let headerHeight:CGFloat = 35
    let headerSpacing:CGFloat = 10
    var numberOfColumns:Int = 2
    let cellSpacing:CGFloat = 1 // not fully implemented
    
        
    override func prepareLayout() {
        var newLayoutInfo = [String:[NSIndexPath:UICollectionViewLayoutAttributes]]()
        var cellLayoutInfo = [NSIndexPath:UICollectionViewLayoutAttributes]()
        var headerLayoutInfo = [NSIndexPath:UICollectionViewLayoutAttributes]()
        
        self.calculateSizesForViewWidth()

		let sectionCount = appDelegate.activeCoinGroups.count
		var nextYOffset:CGFloat = 0
		var lastYOffset:CGFloat = 0
		
		for section in 0 ..< sectionCount {
			// setup section header
			if section > 0 {
				nextYOffset += headerSpacing
			}
			
			var indexPath = NSIndexPath(forItem: 0, inSection: section)
			let headerAttributes = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind:UICollectionElementKindSectionHeader, withIndexPath: indexPath)
			headerAttributes.frame = self.frameForHeader(nextYOffset);
			headerLayoutInfo[indexPath] = headerAttributes
			
			// setup section cells
            if let itemCount = self.collectionView?.numberOfItemsInSection(section) {
                for item in 0 ..< itemCount {
                    indexPath = NSIndexPath(forItem: item, inSection: section)
                    
                    let itemAttributes = UICollectionViewLayoutAttributes(forCellWithIndexPath: indexPath)
                    itemAttributes.frame = self.frameForCell(indexPath, yOffset:nextYOffset);
                    lastYOffset = itemAttributes.frame.origin.y + itemAttributes.frame.size.height
                    cellLayoutInfo[indexPath] = itemAttributes;
                }
            }
			
			nextYOffset = lastYOffset
		}
		
		newLayoutInfo[cellKey] = cellLayoutInfo;
		newLayoutInfo[UICollectionElementKindSectionHeader] = headerLayoutInfo;
		self.layoutInfo = newLayoutInfo;
    }
    
    private func calculateSizesForViewWidth() {
        // use collection view's width for content width
        if let owningView = self.collectionView {
            self.contentWidth = owningView.frame.size.width
        }
        
        let maxColumns = Int(self.contentWidth / (minCellWidth+1))
        self.numberOfColumns = maxColumns
        self.cellWidth = (self.contentWidth - (CGFloat(maxColumns+1) * self.cellSpacing)) / CGFloat(maxColumns)
    }
    
    private func frameForHeader(yOffset:CGFloat) -> CGRect {
        // use collection view's width for content width
        let contentWidth:CGFloat = self.collectionView!.frame.size.width
        return CGRectMake(0, yOffset, contentWidth, headerHeight)
    }
    
    private func frameForCell(indexPath:NSIndexPath, yOffset:CGFloat) -> CGRect {
        let item = indexPath.item        
        let row = item / self.numberOfColumns
        let column = item % self.numberOfColumns
        
        let originX:CGFloat = cellWidth * CGFloat(column) + cellSpacing
        let originY:CGFloat = cellHeight * CGFloat(row) + yOffset + headerHeight + cellSpacing
        
        return CGRectMake(originX, originY, cellWidth, cellHeight)
    }
    
    
    // Returns the width and height of the collection view’s contents.
    override func collectionViewContentSize() -> CGSize {
        // get number of groups and rows within the groups
        var groupCount = 0
        var totalCoinRows = 0

		groupCount = appDelegate.activeCoinGroups.count

		for coinGroup in appDelegate.activeCoinGroups {
			let activeCoins = coinGroup.activeCoins()
			totalCoinRows += activeCoins.count / self.numberOfColumns
            if (activeCoins.count % self.numberOfColumns > 0) {
                totalCoinRows += 1
            }
		}

		let combinedHeaderHeight:CGFloat = (CGFloat(groupCount) * self.headerHeight) + (CGFloat(groupCount-1) * self.headerSpacing)
        let combinedCellHeight:CGFloat = CGFloat(totalCoinRows) * (self.cellHeight + cellSpacing)
        let contentHeight:CGFloat = combinedHeaderHeight + combinedCellHeight
        
        return CGSizeMake(self.contentWidth, contentHeight)
    }
    
    
    override func layoutAttributesForElementsInRect(rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var allAttributes:[UICollectionViewLayoutAttributes] = []
        
        // Iterate through dictionary of element layouts
        for (_, elementsInfo) in self.layoutInfo {
            for (_, attributes) in elementsInfo {
                if CGRectIntersectsRect(rect, attributes.frame) {
                    allAttributes.append(attributes)
                }
            }
        }
        
        return allAttributes
    }
    
    override func shouldInvalidateLayoutForBoundsChange(newBounds: CGRect) -> Bool {
        var result = false
        if newBounds.width != self.contentWidth {
            result = true
        }
        return result
    }

}

