//
//  SpotPrice+CoreDataProperties.swift
//  
//
//  Created by Zachary Ullevig on 5/8/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension SpotPrice {

    @NSManaged var ask: Float
    @NSManaged var bid: Float

}
