//
//  KitcoPriceRequest.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 12/5/14.
//  Copyright (c) 2014 Zachary Ullevig. All rights reserved.
//

import Foundation
import CoreData


// Source data for web scrubbing
// http://www.kitco.com/texten/texten.html


let KitcoPricesUpdatedNotification:String = "KitcoPricesUpdatedNotification"


class KitcoPriceRequest {

    private let _kitcoLondonStartKeys:[String] = ["London", "LONDON", "london"]
    private let _kitcoNewYorkStartKeys:[String] = ["New York", "NEW YORK", "new york", "NY", "ny"]
    private let _kitcoEurasiaStartKeys:[String] = ["Asia", "ASIA", "asia", "Europe", "EUROPE", "europe", "Eurasia", "EURASIA", "eurasia"]
    
    private let _kitcoTableMaxChars:Int = 1000

    private var _londonFixedPrices: [FixPrice] = []
    private var _nySpotPrices: [SpotPrice] = []
    private var _eurasiaSpotPrices: [SpotPrice] = []
    
    private var isNewYorkOpen: Bool = false
    private var isEurasiaOpen: Bool = false
    
    private lazy var feedURL:NSURL? = {
        return NSURL(string: "http://www.kitco.com/texten/texten.html")
    }()
    

    enum RequestError: ErrorType {
        case ParsingFailed(String)
        case Unexpected
    }
    

    func requestCurrentPrices(priceRequestComplete:([CommodityPrice]?, [ErrorType]?)->()) {
        if let feedURL = self.feedURL {
            // Send Asynchronous request using NSURLConnection
            let request = NSURLRequest(URL: feedURL)
            let queue: NSOperationQueue = NSOperationQueue()
            NSURLConnection.sendAsynchronousRequest(request, queue: queue, completionHandler:{ (response:NSURLResponse?, responseData:NSData?, error: NSError?) -> Void in
                
                if error != nil {
                    print(error!.description)
                    priceRequestComplete(nil, [error as! ErrorType])
                }
                else if let data = responseData, let responseStr = NSString(data:data, encoding:NSUTF8StringEncoding) as? String {
                    let (londonFixed, nySpot, eurasiaSpot) = self.parseMarketBlocks(responseStr)
                    var errors:[ErrorType] = []
                    
                    if let marketString = londonFixed {
                        do {
                            try self.parseLondonFixedPrices(marketString)
                        }
                        catch let RequestError.ParsingFailed(message) {
                            errors.append(RequestError.ParsingFailed(message))
                        }
                        catch _ {
                            errors.append(RequestError.Unexpected)
                        }
                    }
                    
                    if let marketString = nySpot {
                        do {
                            try self.parseNYSpotPrices(marketString)
                        }
                        catch let RequestError.ParsingFailed(message) {
                            errors.append(RequestError.ParsingFailed(message))
                        }
                        catch _ {
                            errors.append(RequestError.Unexpected)
                        }
                    }

                    if let marketString = eurasiaSpot {
                        do {
                            try self.parseEurasiaSpotPrices(marketString)
                        }
                        catch let RequestError.ParsingFailed(message) {
                            errors.append(RequestError.ParsingFailed(message))
                        }
                        catch _ {
                            errors.append(RequestError.Unexpected)
                        }
                    }
                    
                    if errors.count > 0 {
                        priceRequestComplete(nil, errors)
                    }

                    // construct callback's results array
                    var currentPrices:[CommodityPrice] = []
                    
                    // Add any london fixed prices found
                    for price in self._londonFixedPrices {
                        currentPrices.append(price)
                    }

                    // Add appropriate spot prices found
                    if self.isNewYorkOpen {
                        // TODO: improve - having trouble with Price protocol as a generic type
                        for price in self._nySpotPrices {
                            currentPrices.append(price)
                        }
                    }
                    else {
                        // TODO: improve - having trouble with Price protocol as a generic type
                        for price in self._eurasiaSpotPrices {
                            currentPrices.append(price)
                        }
                    }
                    priceRequestComplete(currentPrices, nil)
                    
                    NSNotificationCenter.defaultCenter().postNotificationName(KitcoPricesUpdatedNotification, object: nil)
                }
                else {
                    let parseError = NSError(domain: "Price request's response data is nil or not convertable to string", code: 0, userInfo: nil)
                    priceRequestComplete(nil, [parseError])
                }
            }) // NSURLConnection.sendAsynchronousRequest
        }
    }

    
    private func parseMarketBlocks(rawString:String) -> (londonFixed:String?, nySpot:String?, eurasiaSpot:String?) {
        var londonBlock:String? = nil
        var nyBlock:String? = nil
        var eurasiaBlock:String? = nil
        
        // find start of London Fix block
        for startKey in self._kitcoLondonStartKeys {
            let londonSplit = rawString.componentsSeparatedByString(startKey)
            if londonSplit.count > 1 {
                londonBlock = londonSplit[1]
                break
            }
        }

        // find start of New York block
        for startKey in self._kitcoNewYorkStartKeys {
            let nySplit = rawString.componentsSeparatedByString(startKey)
            if nySplit.count > 1 {
                nyBlock = nySplit[1]
                break
            }
        }

        // find start of Eurasia block
        for startKey in self._kitcoEurasiaStartKeys {
            let eurasiaSplit = rawString.componentsSeparatedByString(startKey)
            if eurasiaSplit.count > 1 {
                eurasiaBlock = eurasiaSplit[1]
                break
            }
        }
        
        return (londonBlock, nyBlock, eurasiaBlock)
    }
    
    
    /* 
        https://en.wikipedia.org/wiki/Gold_fixing
    
        London Fix          GOLD          SILVER       PLATINUM           PALLADIUM
                         AM       PM                  AM       PM         AM       PM
        --------------------------------------------------------------------------------
        Jul 10,2015   1162.40   1159.30   15.4500   1033.00   1032.00   650.00   655.00
        Jul 09,2015   1162.10   1164.25   15.3800   1038.00   1032.00   657.00   654.00
        --------------------------------------------------------------------------------
    */
    private func parseLondonFixedPrices(rawString:NSString) throws {
        var goldAMFix:Float = 0
        var goldPMFix:Float = 0
        var silverFix:Float = 0
        var platinumAMFix:Float = 0
        var platinumPMFix:Float = 0
        var palladiumAMFix:Float = 0
        var palladiumPMFix:Float = 0
        var timestamp:NSTimeInterval = 0

        var parseNextLine:Bool = false
        var parsingSuccess:Bool = false
        var needSilver:Bool = true
        var needGold:Bool = true
        var needPlatinum:Bool = true
        var needPalladium:Bool = true

        rawString.enumerateLinesUsingBlock { (line:String, stop:UnsafeMutablePointer<ObjCBool>) -> Void in
            if line == "" {
                stop.initialize(true)
            }
            else if parseNextLine {
                let nsLine = line.condenseWhitespace() as NSString
                let fixDateString:NSString = nsLine.substringToIndex(12).stringByTrimmingCharactersInSet(NSCharacterSet(charactersInString: " "))
                let fixPriceString:NSString = nsLine.substringFromIndex(13)
                
                let date = NSDate(londonFixDateString:fixDateString as String)
                timestamp = date.timeIntervalSinceReferenceDate
                
                let values:[NSString] = fixPriceString.componentsSeparatedByString(" ")
                if needGold {
                    if let value:Float = values[0].floatValue where value > 0 {
                        goldAMFix = value
                        needGold = false
                    }
                    if let value:Float = values[1].floatValue where value > 0 {
                        goldPMFix = value
                        needGold = false
                    }
                }

                if needSilver {
                    if let value:Float = values[2].floatValue where value > 0 {
                        silverFix = value
                        needSilver = false
                    }
                }

                if needPlatinum {
                    if let value:Float = values[3].floatValue where value > 0 {
                        platinumAMFix = value
                        needPlatinum = false
                    }
                    if let value:Float = values[4].floatValue where value > 0 {
                        platinumPMFix = value
                        needPlatinum = false
                    }
                }
                
                if needPalladium {
                    if let value:Float = values[5].floatValue where value > 0 {
                        palladiumAMFix = value
                        needPalladium = false
                    }
                    if let value:Float = values[6].floatValue where value > 0 {
                        palladiumPMFix = value
                        needPalladium = false
                    }
                }
    
                if !needSilver && !needGold && !needPlatinum && !needPalladium {
                    parsingSuccess = true
                    stop.initialize(true)
                }
            }
            else if line.hasSuffix("---") {
                parseNextLine = true
            }
        }
        
        if !parsingSuccess {
            throw RequestError.ParsingFailed("Error parsing London Fix prices")
        }
        else {
            var priceArray:[FixPrice] = []

            let managedContext = coreDataStack.currentManagedObjectContext()
            
            // save silver prices
            if let managedObject = NSEntityDescription.insertNewObjectForEntityForName("FixPrice", inManagedObjectContext: managedContext) as? FixPrice {
                managedObject.config(.Silver, timestamp: timestamp, price: silverFix)
                priceArray.append(managedObject)
            }
            
            // save gold prices
            if let managedObject = NSEntityDescription.insertNewObjectForEntityForName("FixPrice", inManagedObjectContext: managedContext) as? FixPrice {
                managedObject.config(.Gold, timestamp: timestamp, amPrice: goldAMFix, pmPrice: goldPMFix)
                priceArray.append(managedObject)
            }
            
            // save platinum prices
            if let managedObject = NSEntityDescription.insertNewObjectForEntityForName("FixPrice", inManagedObjectContext: managedContext) as? FixPrice {
                managedObject.config(.Platinum, timestamp: timestamp, amPrice: platinumAMFix, pmPrice: platinumPMFix)
                priceArray.append(managedObject)
            }
            
            // save palladium prices
            if let managedObject = NSEntityDescription.insertNewObjectForEntityForName("FixPrice", inManagedObjectContext: managedContext) as? FixPrice {
                managedObject.config(.Palladium, timestamp: timestamp, amPrice: palladiumAMFix, pmPrice: palladiumPMFix)
                priceArray.append(managedObject)
            }

            self._londonFixedPrices = priceArray
        }
    }
    
    
    /*
        New York Spot Price
        MARKET IS CLOSED
        Will open in
        ----------------------------------------------------------------------
        Metals          Bid        Ask           Change        Low       High
        ----------------------------------------------------------------------
        Gold         1162.80     1163.80     +3.50  +0.30%	 1157.00  1166.00
        Silver         15.62       15.72     +0.23  +1.49%	   15.33    15.76
        Platinum     1032.00     1037.00     +9.00  +0.88%	 1021.00  1039.00
        Palladium     649.00      654.00    +14.00  +2.20%	  642.00   658.00
        ----------------------------------------------------------------------
        Last Update on Jul 10, 2015 at 17:15.04
        ----------------------------------------------------------------------
    */
    private func parseNYSpotPrices(rawString:NSString) throws {
        var goldBidPrice:Float = 0
        var goldAskPrice:Float = 0
        var silverBidPrice:Float = 0
        var silverAskPrice:Float = 0
        var platinumBidPrice:Float = 0
        var platinumAskPrice:Float = 0
        var palladiumBidPrice:Float = 0
        var palladiumAskPrice:Float = 0
        var timestamp:NSTimeInterval = 0

        let marketOpenRange = rawString.rangeOfString("MARKET IS OPEN")
        if marketOpenRange.location != NSNotFound {
            self.isNewYorkOpen = true
        }
        else {
            self.isNewYorkOpen = false
        }
        
        // TODO: This can likely be streamlined quite a bit
        
        let goldStartRange = rawString.rangeOfString("Gold")
        if goldStartRange.location == NSNotFound {
            throw RequestError.ParsingFailed("NY Gold Parsing Error")
        }
        else {
            let rawString = rawString.substringFromIndex(goldStartRange.location)
            let scanner:NSScanner = NSScanner(string: rawString)
            scanner.scanUpToCharactersFromSet(NSCharacterSet.decimalDigitCharacterSet(), intoString: nil)
            scanner.scanFloat(&goldBidPrice)
            scanner.scanFloat(&goldAskPrice)
        }
        
        let silverStartRange = rawString.rangeOfString("Silver")
        if silverStartRange.location == NSNotFound {
            throw RequestError.ParsingFailed("NY Silver Parsing Error")
        }
        else {
            let rawString = rawString.substringFromIndex(silverStartRange.location)
            let scanner:NSScanner = NSScanner(string: rawString)
            scanner.scanUpToCharactersFromSet(NSCharacterSet.decimalDigitCharacterSet(), intoString: nil)
            scanner.scanFloat(&silverBidPrice)
            scanner.scanFloat(&silverAskPrice)
        }
        
        let platinumStartRange = rawString.rangeOfString("Platinum")
        if platinumStartRange.location == NSNotFound {
            throw RequestError.ParsingFailed("NY Platinum Parsing Error")
        }
        else {
            let rawString = rawString.substringFromIndex(platinumStartRange.location)
            let scanner:NSScanner = NSScanner(string: rawString)
            scanner.scanUpToCharactersFromSet(NSCharacterSet.decimalDigitCharacterSet(), intoString: nil)
            scanner.scanFloat(&platinumBidPrice)
            scanner.scanFloat(&platinumAskPrice)
        }
        
        let palladiumStartRange = rawString.rangeOfString("Palladium")
        if palladiumStartRange.location == NSNotFound {
            throw RequestError.ParsingFailed("NY Palladium Parsing Error")
        }
        else {
            let rawString = rawString.substringFromIndex(palladiumStartRange.location)
            let scanner:NSScanner = NSScanner(string: rawString)
            scanner.scanUpToCharactersFromSet(NSCharacterSet.decimalDigitCharacterSet(), intoString: nil)
            scanner.scanFloat(&palladiumBidPrice)
            scanner.scanFloat(&palladiumAskPrice)
        }
        
        var updateRange = rawString.rangeOfString("Last Update on ")
        if updateRange.location == NSNotFound {
            throw RequestError.ParsingFailed("NY Update Parsing Error")
        }
        else {
            updateRange.location = updateRange.location + updateRange.length;
            
            var updateString:NSString = rawString.substringFromIndex(updateRange.location)
            let endUpdateRange = updateString.rangeOfString(".")
            updateRange.length = endUpdateRange.location + 3
            updateString = rawString.substringWithRange(updateRange)
            
            let date = NSDate(kitcoShortDateString:updateString as String)
            timestamp = date.timeIntervalSinceReferenceDate
        }
        
        var priceArray:[SpotPrice] = []

        let managedContext = coreDataStack.currentManagedObjectContext()
        
        // save silver prices
        if let managedObject = NSEntityDescription.insertNewObjectForEntityForName("SpotPrice", inManagedObjectContext: managedContext) as? SpotPrice {
            managedObject.config(.Silver, timestamp: timestamp, ask: silverAskPrice, bid: silverBidPrice)
            priceArray.append(managedObject)
        }
        
        // save gold prices
        if let managedObject = NSEntityDescription.insertNewObjectForEntityForName("SpotPrice", inManagedObjectContext: managedContext) as? SpotPrice {
            managedObject.config(.Gold, timestamp: timestamp, ask: goldAskPrice, bid: goldBidPrice)
            priceArray.append(managedObject)
        }
        
        // save platinum prices
        if let managedObject = NSEntityDescription.insertNewObjectForEntityForName("SpotPrice", inManagedObjectContext: managedContext) as? SpotPrice {
            managedObject.config(.Platinum, timestamp: timestamp, ask: platinumAskPrice, bid: platinumBidPrice)
            priceArray.append(managedObject)
        }
        
        // save palladium prices
        if let managedObject = NSEntityDescription.insertNewObjectForEntityForName("SpotPrice", inManagedObjectContext: managedContext) as? SpotPrice {
            managedObject.config(.Palladium, timestamp: timestamp, ask: palladiumAskPrice, bid: palladiumBidPrice)
            priceArray.append(managedObject)
        }
        
        self._nySpotPrices = priceArray
    }
    
    
    /*
        Asia / Europe Spot Price
        MARKET IS CLOSED
        Will open in 46 hours 30 minutes
        ----------------------------------------------------------------------
        Metals                      Bid          Ask      Change from NY close
        ----------------------------------------------------------------------
        Gold                      1162.80      1163.80     +3.50   +0.30%
        Silver                      15.62        15.72     +0.23   +1.49%
        Platinum                  1032.00      1037.00     +9.00   +0.88%
        Palladium                  649.00       654.00    +14.00   +2.20%
        ----------------------------------------------------------------------
        Last Update on Jul 10, 2015 at 17:15.04
        ----------------------------------------------------------------------
    */
    private func parseEurasiaSpotPrices(rawString:NSString) throws {
        var goldBidPrice:Float = 0
        var goldAskPrice:Float = 0
        var silverBidPrice:Float = 0
        var silverAskPrice:Float = 0
        var platinumBidPrice:Float = 0
        var platinumAskPrice:Float = 0
        var palladiumBidPrice:Float = 0
        var palladiumAskPrice:Float = 0

        let marketOpenRange = rawString.rangeOfString("MARKET IS OPEN")
        if marketOpenRange.location != NSNotFound {
            self.isEurasiaOpen = true
        }
        else {
            self.isEurasiaOpen = false
        }

        let goldStartRange = rawString.rangeOfString("Gold")
        if goldStartRange.location == NSNotFound {
            throw RequestError.ParsingFailed("Eurasia Gold Parsing Error")
        }
        else {
            let rawString = rawString.substringFromIndex(goldStartRange.location)
            let scanner:NSScanner = NSScanner(string: rawString)

            scanner.scanUpToCharactersFromSet(NSCharacterSet.decimalDigitCharacterSet(), intoString: nil)
            scanner.scanFloat(&goldBidPrice)
            scanner.scanFloat(&goldAskPrice)
        }

        let silverStartRange = rawString.rangeOfString("Silver")
        if silverStartRange.location == NSNotFound {
            throw RequestError.ParsingFailed("Eurasia Silver Parsing Error")
        }
        else {
            let rawString = rawString.substringFromIndex(silverStartRange.location)
            let scanner:NSScanner = NSScanner(string: rawString)

            scanner.scanUpToCharactersFromSet(NSCharacterSet.decimalDigitCharacterSet(), intoString: nil)
            scanner.scanFloat(&silverBidPrice)
            scanner.scanFloat(&silverAskPrice)
        }

        let platinumStartRange = rawString.rangeOfString("Platinum")
        if platinumStartRange.location == NSNotFound {
            throw RequestError.ParsingFailed("Eurasia Platinum Parsing Error")
        }
        else {
            let rawString = rawString.substringFromIndex(platinumStartRange.location)
            let scanner:NSScanner = NSScanner(string: rawString)
            scanner.scanUpToCharactersFromSet(NSCharacterSet.decimalDigitCharacterSet(), intoString: nil)
            scanner.scanFloat(&platinumBidPrice)
            scanner.scanFloat(&platinumAskPrice)
        }
        
        let palladiumStartRange = rawString.rangeOfString("Palladium")
        if palladiumStartRange.location == NSNotFound {
            throw RequestError.ParsingFailed("NY Palladium Parsing Error")
        }
        else {
            let rawString = rawString.substringFromIndex(palladiumStartRange.location)
            let scanner:NSScanner = NSScanner(string: rawString)
            scanner.scanUpToCharactersFromSet(NSCharacterSet.decimalDigitCharacterSet(), intoString: nil)
            scanner.scanFloat(&palladiumBidPrice)
            scanner.scanFloat(&palladiumAskPrice)
        }
        
        var updateRange = rawString.rangeOfString("Last Update on ")
        var timestamp:NSTimeInterval = 0
        if updateRange.location == NSNotFound {
            throw RequestError.ParsingFailed("Eurasia Update Parsing Error")
        }
        else {
            updateRange.location = updateRange.location + updateRange.length;

            var updateString:NSString = rawString.substringFromIndex(updateRange.location)
            let endUpdateRange = updateString.rangeOfString(".")
            updateRange.length = endUpdateRange.location + 3
            updateString = rawString.substringWithRange(updateRange)
            
            let date = NSDate(kitcoShortDateString:updateString as String)
            timestamp = date.timeIntervalSinceReferenceDate
        }
        
        var priceArray:[SpotPrice] = []
        
        let managedContext = coreDataStack.currentManagedObjectContext()
        
        // save silver prices
        if let managedObject = NSEntityDescription.insertNewObjectForEntityForName("SpotPrice", inManagedObjectContext: managedContext) as? SpotPrice {
            managedObject.config(.Silver, timestamp: timestamp, ask: silverAskPrice, bid: silverBidPrice)
            priceArray.append(managedObject)
        }
        
        // save gold prices
        if let managedObject = NSEntityDescription.insertNewObjectForEntityForName("SpotPrice", inManagedObjectContext: managedContext) as? SpotPrice {
            managedObject.config(.Gold, timestamp: timestamp, ask: goldAskPrice, bid: goldBidPrice)
            priceArray.append(managedObject)
        }
        
        // save platinum prices
        if let managedObject = NSEntityDescription.insertNewObjectForEntityForName("SpotPrice", inManagedObjectContext: managedContext) as? SpotPrice {
            managedObject.config(.Platinum, timestamp: timestamp, ask: platinumAskPrice, bid: platinumBidPrice)
            priceArray.append(managedObject)
        }
        
        // save palladium prices
        if let managedObject = NSEntityDescription.insertNewObjectForEntityForName("SpotPrice", inManagedObjectContext: managedContext) as? SpotPrice {
            managedObject.config(.Palladium, timestamp: timestamp, ask: palladiumAskPrice, bid: palladiumBidPrice)
            priceArray.append(managedObject)
        }
        
        self._eurasiaSpotPrices = priceArray
    }
}
