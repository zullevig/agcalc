//
//  PurchaseViewController.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 7/19/15.
//  Copyright © 2015 Zachary Ullevig. All rights reserved.
//

import UIKit
import StoreKit


let iapSharedSecret = "cfc7eac5f84642d7abaa4ccfc10fd6de"

enum ProductKeys:String {
    case RemoveAds = "agcalc.iap.removeads"
}

extension SKProduct {
    func localizedPrice() -> String {
        let formatter = NSNumberFormatter()
        formatter.numberStyle = .CurrencyStyle
        formatter.locale = self.priceLocale
        return formatter.stringFromNumber(self.price)!
    }
}


class PurchaseViewController: UIViewController, SKProductsRequestDelegate, SKPaymentTransactionObserver, UITableViewDataSource, UITableViewDelegate {
    
    var productList = [SKProduct]()
    
    @IBOutlet weak var productTable: UITableView!
    @IBOutlet weak var unavailableLabel: UILabel!
    @IBOutlet weak var unavailableView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // create header view
        if let restorePurchasesHeaderView = UINib(nibName: "RestorePurchasesHeaderView", bundle: nil).instantiateWithOwner(nil, options: nil)[0] as? RestorePurchasesHeaderView {
            restorePurchasesHeaderView.productController = self
            self.productTable.tableHeaderView = restorePurchasesHeaderView
        }

        self.unavailableView.hidden = false

        // Set in app purchases connection with Apple servers
        if(SKPaymentQueue.canMakePayments()) {
            // IAP is enabled, loading
            let productIDs:Set = Set([ProductKeys.RemoveAds.rawValue])
            let request: SKProductsRequest = SKProductsRequest(productIdentifiers: productIDs)

            request.delegate = self
            request.start()
        }
        else {
            self.unavailableLabel.text = "Please enable In App Purchase in Settings"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func restorePurchases() {
        print("Restore Purchases")
        
        self.unavailableLabel.text = "Restoring purchases from the App Store…"
        self.unavailableView.hidden = false
        SKPaymentQueue.defaultQueue().addTransactionObserver(self)
        SKPaymentQueue.defaultQueue().restoreCompletedTransactions()
    }
    
    // MARK: - UITableView methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("productCellIdentifier", forIndexPath: indexPath) as! ProductTableViewCell
        
        // Configure the cell...
        let product = self.productList[indexPath.row]
        cell.productTitle.text = product.localizedTitle
        cell.productPrice.text = product.localizedPrice()
        cell.productDescription.text = product.localizedDescription
        cell.product = product
        cell.productController = self
        
        if UserSettings.sharedInstance.adMode == AdMode.Off {
            cell.buyButton.hidden = true
            cell.purchasedLabel.hidden = false
        }
        
        return cell
    }
    
    
    // MARK: - SKProductsRequestDelegate methods
    
    func purchase(product:SKProduct) {
        print("buy " + product.productIdentifier)
        let pay = SKPayment(product: product)
        SKPaymentQueue.defaultQueue().addTransactionObserver(self)
        SKPaymentQueue.defaultQueue().addPayment(pay as SKPayment)
    }
    
    // request a list of available products
    func productsRequest(request: SKProductsRequest, didReceiveResponse response: SKProductsResponse) {
        print("product request")
        let products = response.products
        
        for product in products {
            print("product added")
            print(product.productIdentifier)
            print(product.localizedTitle)
            print(product.localizedDescription)
            print(product.price)
            productList.append(product)
        }
        
        self.unavailableView.hidden = true
        self.productTable.hidden = false
        self.productTable.reloadData()
    }
    
    // restore purchase state for products reported as purchased
    func paymentQueueRestoreCompletedTransactionsFinished(queue: SKPaymentQueue) {
        print("transactions restored")
        self.unavailableView.hidden = true

        for transaction in queue.transactions {
            let prodID = transaction.payment.productIdentifier
            
            switch prodID {
                case ProductKeys.RemoveAds.rawValue:
                    print("remove ads purchase success")
                    removeAds()
                    
                default:
                    print("unexpected product id")
            }
        }
    }
    
    // handle updates to transactions
    func paymentQueue(queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        print("add paymnet")
        
        for transaction in transactions {
            let prodID = transaction.payment.productIdentifier

            switch transaction.transactionState {
                
            case .Purchased:
                print("buy ok; unlock \(prodID) here")
                switch prodID {
                    case ProductKeys.RemoveAds.rawValue:
                        print("remove ads")
                        removeAds()
                    default:
                        print("unexpected product id")
                }

                self.unavailableView.hidden = true
                queue.finishTransaction(transaction)
                break;
                
            case .Failed:
                print("buy error")
                self.unavailableView.hidden = true
                queue.finishTransaction(transaction)
                break;
                
            default:
                print("default")
                break;
            }
        }
    }
    
    func finishTransaction(trans:SKPaymentTransaction) {
        print("finish trans")
        SKPaymentQueue.defaultQueue().finishTransaction(trans)
    }
    
    func paymentQueue(queue: SKPaymentQueue, removedTransactions transactions: [SKPaymentTransaction]) {
        print("remove trans");
    }

    private func removeAds() {
        if UserSettings.sharedInstance.adMode == AdMode.On {
            UserSettings.sharedInstance.adMode = AdMode.Off
        }
        self.productTable.reloadData()
    }
}
