//
//  PricesViewController.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 4/15/16.
//  Copyright © 2016 Zachary Ullevig. All rights reserved.
//

import UIKit

class PricesViewController: PriceAwareViewController {

    @IBOutlet weak var tableView: UITableView!
    
    enum TableSection {
        case SpotPrices
        case FixedPrices
    }
    
    enum TableCell:String {
        case MarketHeader = "MarketHeaderCell"
        case SpotPriceHeader = "SpotPriceHeaderCell"
        case SpotPriceRow = "SpotPriceCell"
        case FixedPriceHeader = "FixedPriceHeaderCell"
        case FixedPriceRow = "FixedPriceCell"
    }
    
    lazy var sections:[Int:TableSection] = {
        return [0:.SpotPrices, 1:.FixedPrices]
    }()
    
    lazy var cells:[TableSection:[Int:TableCell]] = {
        return [.SpotPrices:[0:.SpotPriceHeader, 1:.SpotPriceRow, 2:.SpotPriceRow, 3:.SpotPriceRow, 4:.SpotPriceRow],
                .FixedPrices:[0:.FixedPriceHeader, 1:.FixedPriceRow, 2:.FixedPriceRow, 3:.FixedPriceRow, 4:.FixedPriceRow]]
    }()
    
    lazy var metals:[Int:CommodityType] = {
        return [1:.Silver, 2:.Gold, 3:.Platinum, 4:.Palladium]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    // MARK: - Notification handlers

    override func priceUpdated(notification: NSNotification?) {
        dispatch_async(dispatch_get_main_queue(),{
            self.tableView.reloadData()
        })
    }

    
    // MARK: - UITableViewDataSource and UITableViewDelegate methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.sections.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let result:Int

        if let section = self.sections[section], let cells = self.cells[section] {
            result = cells.count
        }
        else {
            result = 0
        }
        return result
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:UITableViewCell

        if let section = self.sections[indexPath.section], let cells = self.cells[section], let tableCell = cells[indexPath.row] {
            cell = tableView.dequeueReusableCellWithIdentifier(tableCell.rawValue)! as UITableViewCell
            
            if tableCell == .SpotPriceRow {
                if let valueCell = cell as? SpotPriceTableViewCell, let commodity = metals[indexPath.row] {
                    valueCell.commodity = commodity
                }
            }
            else if tableCell == .FixedPriceRow {
                if let valueCell = cell as? FixedPriceTableViewCell, let commodity = metals[indexPath.row] {
                    valueCell.commodity = commodity
                }
            }
        }
        else {
            cell = UITableViewCell()
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cellIdentifier = TableCell.MarketHeader.rawValue
        
        // get and configure cell
        let cell:MarketHeaderTableViewCell = self.tableView.dequeueReusableCellWithIdentifier(cellIdentifier)! as! MarketHeaderTableViewCell
        
        
        if self.sections[section] == .FixedPrices {
            cell.marketLabel.text = "London Fixed Prices"
        }
        else {
            cell.marketLabel.text = "Spot Prices"
        }
        
        return cell
    }

//    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 40.0
//    }
}
