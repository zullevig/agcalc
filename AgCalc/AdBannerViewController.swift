//
//  AdBannerViewController.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 12/26/14.
//  Copyright (c) 2014 Zachary Ullevig. All rights reserved.
//

import UIKit
import iAd


class AdBannerViewController: UIViewController, ADBannerViewDelegate {

    @IBOutlet weak var containerBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var adBannerBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var adBannerView: ADBannerView!
    @IBOutlet weak var containerView: UIView!
    
    
    var bannerIsVisible : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // register with UserSettings for triggering ad removal
        UserSettings.sharedInstance.adBannerViewController = self

        self.adBannerView.hidden = true
        self.adBannerBottomConstraint.constant = -self.adBannerView.frame.height
        
        if UserSettings.sharedInstance.adMode == AdMode.Off {
            disableAds()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func bannerViewDidLoadAd(banner: ADBannerView!) {
        if UserSettings.sharedInstance.adMode == AdMode.Off {
            // prevent an ad load if ads have been disabled
            disableAds()
        }
        else {
            self.adBannerView.hidden = false
            self.containerBottomConstraint.constant = banner.frame.height
            self.adBannerBottomConstraint.constant = 0

            UIView.animateWithDuration(0.5, delay: 0.0, options: .CurveEaseOut, animations: {
                self.view.layoutIfNeeded()
                }, completion: nil)
        }
    }

    func bannerView(banner: ADBannerView!, didFailToReceiveAdWithError error: NSError!) {
        hideAdBanner()
    }
    
    func hideAdBanner() {
        self.adBannerView.hidden = true
        self.containerBottomConstraint.constant = 0
        self.adBannerBottomConstraint.constant = -self.adBannerView.frame.height
        
        UIView.animateWithDuration(0.2, delay: 0.0, options: .CurveEaseOut, animations: {
            self.view.layoutIfNeeded()
            }, completion: nil)
    }
    
    func disableAds() {
        if UserSettings.sharedInstance.adMode == AdMode.Off {
            if self.adBannerView != nil, let _ = self.adBannerView.superview {
                hideAdBanner()
                self.adBannerView.removeFromSuperview()
                self.adBannerView = nil
            }
        }
    }

}
