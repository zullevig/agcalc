//
//  String+extension.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 5/8/16.
//  Copyright © 2016 Zachary Ullevig. All rights reserved.
//

import Foundation


extension String {
    func condenseWhitespace() -> String {
        let components = self.componentsSeparatedByCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).filter{$0.isEmpty == false}
        return components.joinWithSeparator(" ")
    }
}

