//
//  SilverPiecesTableViewController.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 12/5/14.
//  Copyright (c) 2014 Zachary Ullevig. All rights reserved.
//

import UIKit

class SilverPiecesTableViewController: UITableViewController {
    
    weak var issuer:Issuer? = nil


    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        appDelegate.reloadCachedValues()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        var result = 0
        
        if let issuer = self.issuer {
            if let coinGroups = issuer.coinGroups {
                result = coinGroups.count
            }
        }
        
        return result
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        var result = 0
        
        if let issuer = self.issuer, let coinGroups = issuer.coinGroups, let coins = coinGroups[section].coins {
            result = coins.count
        }
        
        return result
    }

    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var result: String? = nil
        
        if let issuer = self.issuer {
            if let coinGroups = issuer.coinGroups {
                result = coinGroups[section].name
            }
        }
        
        return result
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("silverPiecesCellReuseIdentifier", forIndexPath: indexPath) as! SilverPiecesTableViewCell

        // Configure the cell...
        if let issuer = self.issuer, let coinGroups = issuer.coinGroups {
            let coin = coinGroups[indexPath.section].sortedCoins()[indexPath.row]
            cell.configureCell(coin)
        }

        return cell
    }
}
