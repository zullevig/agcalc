//
//  CalculatorViewController.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 12/6/14.
//  Copyright (c) 2014 Zachary Ullevig. All rights reserved.
//

import UIKit


class CalculatorViewController: PriceAwareViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    let headerReuseIdentifier = "coinGroupSectionIdentifier"
    var coinStack:CoinStack = CoinStack()
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var refreshButton: UIButton!
    @IBOutlet weak var refreshSpinner: UIActivityIndicatorView!
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var timestampLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var coinCollectionView: UICollectionView!
    @IBOutlet weak var gridCollectionViewLayout: GridCollectionViewLayout!
    
    @IBOutlet weak var footerView: UIView!

    // MARK: - UIViewController overrides

    override func viewDidLoad() {
        super.viewDidLoad()
                
        let headerViewNIB = UINib(nibName: "CoinGroupCollectionHeaderView", bundle: nil)
        self.coinCollectionView?.registerNib(headerViewNIB, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerReuseIdentifier)
        self.gridCollectionViewLayout?.cellHeight = 135.0
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.updateTotalValue()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Price Aware ViewController overrides

    override func updatePriceControls() {
        // allow triggering UI updates from any thread
        dispatch_async(dispatch_get_main_queue(),{
            let (silverPrice, timestampString) = UserSettings.sharedInstance.priceForCalculation(.Silver)
            if silverPrice > 0 {
                self.priceLabel.text = String(format:"$%.2f / oz t", silverPrice)
                if UserSettings.sharedInstance.priceUpdateMode == .Manual {
                    self.timestampLabel.text = "Manual Price Assigned"
                }
				else {
                    self.timestampLabel.text = "@\(timestampString)"
                }
            }
            else {
                // no price available
                if UserSettings.sharedInstance.priceUpdateMode == PriceUpdateMode.Manual {
                    self.priceLabel.text = "Price Not Set"
               }
                else {
                    self.priceLabel.text = "Price Unavailable"
                }
                self.timestampLabel.text = ""
            }
            
            self.coinCollectionView.reloadData()
            self.updateTotalValue()
            
            self.refreshSpinner.stopAnimating()
            self.refreshButton.hidden = false
        });
    }


    // MARK: - Total Value Methods
    
    func updateTotalValue() {
        // get appropriage value string
        if UserSettings.sharedInstance.mediumOfExchange == .USDollar {
            if let prices = UserSettings.sharedInstance.pricesForCalculations() {
                self.totalLabel.text = String(format:"$%.2f", self.coinStack.stackValue(prices))
            }
            else {
                self.totalLabel.text = "Unknown"
            }
        }
        else if UserSettings.sharedInstance.mediumOfExchange == .Silver {
            if UserSettings.sharedInstance.unitOfMass == .Gram {
                self.totalLabel.text = String(format:"%.4fg AG", self.coinStack.stackGrams(.Silver))
            }
            else if UserSettings.sharedInstance.unitOfMass == .TroyOunce {
                self.totalLabel.text = String(format:"%.4foz AG", self.coinStack.stackOunces(.Silver))
            }
        }
        else {
            print("Error: unexpected MediumOfExchange setting")
            self.totalLabel.text = "Unknown"
        }
    }

    
    // MARK: - UICollectionView delegate methods

    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return appDelegate.activeCoinGroups.count
    }

    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var result = 0
		if section < appDelegate.activeCoinGroups.count {
			result = appDelegate.activeCoinGroups[section].activeCoins().count
		}
        return result
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("calculatorCoinCellIdentifier", forIndexPath: indexPath) as! CalculatorCollectionViewCell
		
		if indexPath.section < appDelegate.activeCoinGroups.count {
            let coins = appDelegate.activeCoinGroups[indexPath.section].activeCoins()
			let coin = coins[indexPath.item]
			let quantity = self.coinStack.countForCoin(coin)
			cell.configureCell(coin, quantity:quantity, parent:self, indexPath:indexPath)
            cell.applyStyle()
        }
        
        return cell
    }
    
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        let view = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionHeader, withReuseIdentifier: headerReuseIdentifier, forIndexPath: indexPath) as! CoinGroupCollectionHeaderView
		if indexPath.section < appDelegate.activeCoinGroups.count {
            let coins = appDelegate.activeCoinGroups[indexPath.section].activeCoins()
            if let issuer = coins[0].issuer {
                view.configure(issuer)
            }
            view.coinGroupNameLabel.text = appDelegate.activeCoinGroups[indexPath.section].name
        }
        
        return view
    }
    
    
    // MARK: - IBActions

    @IBAction func clearCoinStack(sender: UIButton) {
        self.coinStack.removeAllCoins()
        self.coinCollectionView.reloadData()
        self.updateTotalValue()
    }
    
    @IBAction func refreshPrices(sender: UIButton) {
        self.refreshButton.hidden = true
        self.refreshSpinner.startAnimating()
        UserSettings.sharedInstance.refreshPrices(nil)
    }
}

