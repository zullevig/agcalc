//
//  AgStyle.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 7/11/15.
//  Copyright (c) 2015 Zachary Ullevig. All rights reserved.
//

import UIKit


// style dictionaries are broken down by type and separately registered to
// avoid a swift limitation where the compiler is unable to process static
// data initializations beyond a certain size/complexity.  Improvements in
// the swift compiler could simplify the style registration process.

let _ZAStyleManager = ZAStyleManager(styleSheets:[
    ZASView.styleKey()               : agViewStyles,
    ZASButton.styleKey()             : agButtonStyles,
    ZASDividerView.styleKey()        : agDividerViewStyles,
    ZASImageView.styleKey()          : agImageViewStyles,
    ZASLabel.styleKey()              : agLabelStyles,
    ZASNavigationBar.styleKey()      : agNavStyles,
    ZASProgressView.styleKey()       : agProgressViewStyles,
    ZASSegmentedControl.styleKey()   : agSegmentedControlStyles,
    ZASStepper.styleKey()            : agStepperStyles,
    ZASTabBar.styleKey()             : agTabBarStyles,
    ZASTextField.styleKey()          : agTextFieldStyles,
    ])



// MARK: - Color Definitions

enum AgHexColor:HexColor {
    case BOLDTEST       = 0xFFFF0000
    
    case Blue           = 0xFF0075D6
    case DarkBlueGray   = 0xFF363F4C
    
	case Black          = 0xFF000000
	case DarkGray       = 0xFF69747a
	case MediumGray     = 0xFF666666
	case LightGray      = 0xFFcccccc
	case DustGray       = 0xFFf7f7f7
	case White          = 0xFFFFFFFF
	case Clear          = 0x00000000
	
	func applyAlpha(alphaPercentage:Float) -> HexColor {
		return self.rawValue.applyAlpha(alphaPercentage)
	}
}


// MARK: - -- Style Dictionaries --

// MARK: View Syles
let agViewStyles:ZASStyleDictionary = [
    "DropunderView" : [
        .BackgroundColor	: AgHexColor.DustGray.rawValue,
        .BorderColor		: AgHexColor.DarkGray.rawValue,
        .BorderCornerRadius	: CGFloat(5),
        .BorderWidth		: CGFloat(1),
    ],
    
    "FooterShadowView" : [
        .ShadowColor        : AgHexColor.Black.rawValue,
        .ShadowOffset       : CGSizeMake(0, -3),
        .ShadowOpacity      : Float(0.5),
        .ShadowRadius       : CGFloat(3),
    ],
    
    "HeaderShadowView" : [
        .ShadowColor        : AgHexColor.Black.rawValue,
        .ShadowOffset       : CGSizeMake(0, 3),
        .ShadowOpacity      : Float(0.5),
        .ShadowRadius       : CGFloat(3),
    ],
    
    "CellShadowView" : [
        .ShadowColor        : AgHexColor.Black.rawValue,
        .ShadowOffset       : CGSizeMake(0, 2),
        .ShadowOpacity      : Float(0.5),
        .ShadowRadius       : CGFloat(1),
    ],
    
    "PriceHeaderView" : [
        .GradientColors     : [AgHexColor.White.rawValue, AgHexColor.LightGray.rawValue],
    ],
    
    "TableHeaderView" : [
        .BackgroundColor	: AgHexColor.White.rawValue,
        .BorderColor		: AgHexColor.DarkGray.rawValue,
        .BorderCornerRadius	: CGFloat(0),
        .BorderWidth		: CGFloat(1),
    ],
    
    "WaitPopupView" : [
        .BackgroundColor	: AgHexColor.DustGray.rawValue,
        .BorderColor		: AgHexColor.DarkGray.rawValue,
        .BorderCornerRadius	: CGFloat(10),
        .BorderWidth		: CGFloat(1),
    ],
    
    "WaitPopupBackgroundView" : [
        .BackgroundColor	: AgHexColor.Black.rawValue.applyAlpha(0.4),
    ],
    
    "CircleView" : [
        .Shape              : ZASViewShape.Circle,
    ],
    
    "CollectionCell" : [
        .BackgroundColor	: AgHexColor.White.rawValue,
        .BorderColor		: AgHexColor.DarkBlueGray.rawValue,
        .BorderCornerRadius	: CGFloat(5),
        .BorderWidth		: CGFloat(2),
    ],

    "ContentView" : [
        .BorderColor		: AgHexColor.MediumGray.rawValue,
        .BorderCornerRadius	: CGFloat(5),
        .BorderWidth		: CGFloat(1),
    ],

]


// MARK: Divider View Syles
let agDividerViewStyles:ZASStyleDictionary = [
    "DividerLineView" : [
        .BackgroundColor	: AgHexColor.Clear.rawValue,
        .LineColor			: AgHexColor.LightGray.rawValue,
        .LineWidth			: CGFloat(0.5),
    ],
]


// MARK: Button Syles
let agButtonStyles:ZASStyleDictionary = [
    "StandardButton" : [
        .BackgroundColor	: AgHexColor.Clear.rawValue,
        .TextColor			: AgHexColor.Blue.rawValue,
    ],

    "CheckboxButton" : [
        .BackgroundColor	: AgHexColor.DarkBlueGray.rawValue.applyAlpha(0.1),
        .BorderColor		: AgHexColor.DarkBlueGray.rawValue,
        .BorderCornerRadius	: CGFloat(0),
        .BorderWidth		: CGFloat(1),
        .ShadowColor        : AgHexColor.Black.rawValue,
        .ShadowOffset       : CGSizeMake(1, 1),
        .ShadowOpacity      : Float(0.2),
        .ShadowRadius       : CGFloat(0.5),
    ],
    
    "DropunderButton" : [
        .BackgroundColor	: AgHexColor.Clear.rawValue,
        .TextColor			: AgHexColor.Blue.rawValue,
    ],
]


// MARK: Label Styles
let agLabelStyles:ZASStyleDictionary = [
    "StandardLabel" : [
        .BackgroundColor	: AgHexColor.Clear.rawValue,
        .FontName			: ZASFontName.CourierBold,
        .FontSize			: CGFloat(14),
        .TextColor			: AgHexColor.DarkBlueGray.rawValue,
    ],
    
    "HeavyLabel" : [
        .BackgroundColor	: AgHexColor.Clear.rawValue,
        .FontName			: ZASFontName.HelveticaBold,
        .FontSize			: CGFloat(13),
        .TextColor			: AgHexColor.DarkBlueGray.rawValue,
    ],
    
    "SectionHeaderLargeLabel" : [
        .BackgroundColor	: AgHexColor.Clear.rawValue,
        .FontName			: ZASFontName.GillSans,
        .FontSize			: CGFloat(17),
        .TextColor			: AgHexColor.DarkBlueGray.rawValue,
    ],
    
    "CellHeaderLargeLabel" : [
        .BackgroundColor	: AgHexColor.Clear.rawValue,
        .FontName			: ZASFontName.GillSans,
        .FontSize			: CGFloat(17),
        .TextColor			: AgHexColor.DarkBlueGray.rawValue,
    ],
    
    "CellHeaderLabel" : [
        .BackgroundColor	: AgHexColor.Clear.rawValue,
        .FontName			: ZASFontName.GillSans,
        .FontSize			: CGFloat(12),
        .TextColor			: AgHexColor.DarkBlueGray.rawValue,
    ],
    
    "CellSubheaderLabel" : [
        .BackgroundColor	: AgHexColor.Clear.rawValue,
        .FontName			: ZASFontName.GillSans,
        .FontSize			: CGFloat(10),
        .TextColor			: AgHexColor.DarkBlueGray.rawValue,
    ],
    
    "DataLabel" : [
        .BackgroundColor	: AgHexColor.Clear.rawValue,
        .FontName			: ZASFontName.Courier,
        .FontSize			: CGFloat(14),
        .TextColor			: AgHexColor.DarkBlueGray.rawValue,
    ],
    
    "DataSmallLabel" : [
        .BackgroundColor	: AgHexColor.Clear.rawValue,
        .FontName			: ZASFontName.Courier,
        .FontSize			: CGFloat(12),
        .TextColor			: AgHexColor.DarkBlueGray.rawValue,
    ],
    
    "DataHeaderLabel" : [
        .BackgroundColor	: AgHexColor.Clear.rawValue,
        .FontName			: ZASFontName.GillSans,
        .FontSize			: CGFloat(14),
        .TextColor			: AgHexColor.DarkBlueGray.rawValue,
    ],
    
    "DataHeaderSmallLabel" : [
        .BackgroundColor	: AgHexColor.Clear.rawValue,
        .FontName			: ZASFontName.GillSans,
        .FontSize			: CGFloat(10),
        .TextColor			: AgHexColor.DarkBlueGray.rawValue,
    ],
    
    "WaitPopupLabel" : [
        .BackgroundColor	: AgHexColor.Clear.rawValue,
        .FontName			: ZASFontName.Helvetica,
        .FontSize			: CGFloat(15),
        .TextColor			: AgHexColor.DarkBlueGray.rawValue,
    ],
]


// MARK: Text Field Styles
let agTextFieldStyles:ZASStyleDictionary = [
    "DataEntryField" : [
        .BackgroundColor	: AgHexColor.White.rawValue,
        .TextColor			: AgHexColor.Blue.rawValue,
    ],
]

// MARK: ImageView Syles
let agImageViewStyles:ZASStyleDictionary = [
    "CoinIcon" : [
        .BackgroundColor	: AgHexColor.Clear.rawValue,
        .ShadowColor        : AgHexColor.Black.rawValue,
        .ShadowOffset       : CGSizeMake(0, 0),
        .ShadowOpacity      : Float(0.7),
        .ShadowRadius       : CGFloat(3),
    ],
    
    "FlagIcon" : [
        .BackgroundColor	: AgHexColor.Clear.rawValue,
        .ShadowColor        : AgHexColor.Black.rawValue,
        .ShadowOffset       : CGSizeMake(1, 1),
        .ShadowOpacity      : Float(0.5),
        .ShadowRadius       : CGFloat(2),
    ],
]


// MARK: Segmented Control Styles
let agSegmentedControlStyles:ZASStyleDictionary = [
    :
]

// MARK: Progress View Control Styles
let agProgressViewStyles:ZASStyleDictionary = [
    :
]

// MARK: Stepper Syles
let agStepperStyles:ZASStyleDictionary = [
    "CountStepper" : [
        .BackgroundColor	: AgHexColor.DustGray.rawValue,
        .BorderCornerRadius	: CGFloat(5),
        .ShadowColor        : AgHexColor.Black.rawValue,
        .ShadowOffset       : CGSizeMake(1, 1),
        .ShadowOpacity      : Float(0.2),
        .ShadowRadius       : CGFloat(0.5),
        .TintColor          : AgHexColor.DarkBlueGray.rawValue
    ],
]


// MARK: Navigation Syles
let agNavStyles:ZASStyleDictionary = [
    "NavBar" : [
        // FIXME: BackFontName & BackFontSize killed by shadow settings if in different class
        .BackFontName		: ZASFontName.GillSans.rawValue,
        .BackFontSize		: CGFloat(16),
        
        .BackTextColor      : AgHexColor.Blue.rawValue,
        .FontName			: ZASFontName.GillSans.rawValue,
        .FontSize			: CGFloat(20),
        .GradientColors     : [AgHexColor.White.rawValue, AgHexColor.LightGray.rawValue],
        .ShadowColor        : AgHexColor.Black.rawValue,
        .ShadowOffset       : CGSizeMake(0, 3),
        .ShadowOpacity      : Float(0.5),
        .ShadowRadius       : CGFloat(3),
        .TextColor			: AgHexColor.DarkBlueGray.rawValue,
    ],
]


// MARK: Navigation Syles
let agTabBarStyles:ZASStyleDictionary = [
    "TabBar" : [
        // FIXME: BackFontName & BackFontSize killed by shadow settings if in different class
        .FontName			: ZASFontName.HelveticaBold.rawValue,
        .FontSize			: CGFloat(12),
        
        .GradientColors     : [AgHexColor.DarkBlueGray.rawValue, AgHexColor.Black.rawValue],
        .SelectionColor     : AgHexColor.White.rawValue.applyAlpha(0.2),
        .SelectionIconColor : AgHexColor.White.rawValue,
        .ShadowColor        : AgHexColor.Black.rawValue,
        .ShadowOffset       : CGSizeMake(0, -3),
        .ShadowOpacity      : Float(0.5),
        .ShadowRadius       : CGFloat(3),
        .TextColor			: AgHexColor.White.rawValue,
    ],
]
