//
//  AppDelegate.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 12/5/14.
//  Copyright (c) 2014 Zachary Ullevig. All rights reserved.
//

import UIKit
import ZACoreDataStack
import CoreData

let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
let coreDataStack: ZACoreDataStack = ZACoreDataStack(model: "DataModel")


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow? = nil
    
    var activeIssuers:[Issuer] = []
    var activeCoinGroups:[CoinGroup] = []
    var issuers:[Issuer]? = nil
    var coinGroups:[CoinGroup]? = nil
    
    // TODO: do real errors
    enum AgError: ErrorType {
        case AgGenericError
    }
    
    
    // MARK: - Shared core data access
    
    func resetData() {
        let groupRequest = NSFetchRequest(entityName: "CoinGroup")
        let issuerRequest = NSFetchRequest(entityName: "Issuer")
        
        do {
            guard let groups = try coreDataStack.currentManagedObjectContext().executeFetchRequest(groupRequest) as? [CoinGroup] else {
                // TODO: handle error
                throw AgError.AgGenericError
            }
            for group in groups {
                coreDataStack.currentManagedObjectContext().deleteObject(group as NSManagedObject)
            }
        } catch _ {
            // TODO: handle error
        }
        // TEMPORARY
        do {
            guard let issuers = try coreDataStack.currentManagedObjectContext().executeFetchRequest(issuerRequest) as? [Issuer] else {
                // TODO: handle error
                throw AgError.AgGenericError
            }
            for issuer in issuers {
                coreDataStack.currentManagedObjectContext().deleteObject(issuer as NSManagedObject)
            }
        } catch _ {
            // TODO: handle error
        }
        
        coreDataStack.saveContext()
    }
    
    func reloadCoinData() {
        self.reloadIssuers()
        self.reloadCoinGroups()
        
        let managedObjectContext = coreDataStack.currentManagedObjectContext()
        let coinData = CoinsParser().loadCoinsJSON(managedObjectContext, existingIssuers: self.issuers, existingGroups: self.coinGroups)
        self.issuers = coinData.issuers
        self.coinGroups = coinData.groups
        coreDataStack.saveContext()
        
        self.issuers?.sortInPlace({ (first:Issuer, second:Issuer) -> Bool in
            let isFirst:Bool
            if first.sequence.integerValue < second.sequence.integerValue {
                isFirst = true
            }
            else {
                isFirst = false
            }
            return isFirst
        })
        
        self.coinGroups?.sortInPlace({ (first:CoinGroup, second:CoinGroup) -> Bool in
            let isFirst:Bool
            if first.sequence.integerValue < second.sequence.integerValue {
                isFirst = true
            }
            else {
                isFirst = false
            }
            return isFirst
        })
    }
    
    func reloadIssuers() {
        let request = NSFetchRequest(entityName: "Issuer")
        request.predicate = NSPredicate(format: "releaseState == 'Live'")
        
        let managedObjectContext = coreDataStack.currentManagedObjectContext()
        do {
            guard let fetchResults = try managedObjectContext.executeFetchRequest(request) as? [Issuer] else {
                // TODO: make real error
                throw AgError.AgGenericError
            }
            
            self.issuers = fetchResults
        }
        catch {
            // TODO: handle error
        }
    }
    
    func reloadCoinGroups() {
        let request = NSFetchRequest(entityName: "CoinGroup")
        let managedObjectContext = coreDataStack.currentManagedObjectContext()
        do {
            guard let fetchResults = try managedObjectContext.executeFetchRequest(request) as? [CoinGroup] else {
                // TODO: make real error
                throw AgError.AgGenericError
            }
            self.coinGroups = fetchResults
        }
        catch {
            // TODO: handle error
        }
    }
    
    func reloadCachedValues() {
        if let currentIssuers = self.issuers {
            self.activeIssuers = currentIssuers.filter { $0.active.boolValue == true }
            self.activeCoinGroups = []
            
            for issuer in self.activeIssuers {
                if let issuerCoinGroups = issuer.coinGroups {
                    for group in issuerCoinGroups {
                        if group.activeCoins().count > 0 {
                            self.activeCoinGroups += [group]
                        }
                    }
                }
            }
        }
    }
    
    /// Load the coin with given id from the database
    func coin(withID:Int) -> Coin? {
        var result:Coin? = nil
        let request = NSFetchRequest(entityName: "Coin")
        request.predicate = NSPredicate(format: "coinId == \(withID)")
        
        do {
            guard let coins = try coreDataStack.currentManagedObjectContext().executeFetchRequest(request) as? [Coin] else {
                // TODO: handle error
                throw AgError.AgGenericError
            }
            result = coins[0]
        }
        catch _ {
            // TODO: handle error
        }
        
        return result
    }
    
    
    // MARK: - Application delegate methods
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Enable this to start with a clean data set
        //self.resetData()
        
        UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: false)
        
        // TODO: Don't reload unless there has been a data update
        self.reloadCoinData()
        self.reloadCachedValues()
        return true
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        coreDataStack.saveContext()
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        coreDataStack.saveContext()
    }
}

