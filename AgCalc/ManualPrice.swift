//
//  ManualPrice.swift
//  
//
//  Created by Zachary Ullevig on 5/8/16.
//
//

import Foundation
import CoreData


class ManualPrice: CommodityPrice {

// Insert code here to add functionality to your managed object subclass

    override var price: Float {
        get {
            return self.userPrice
        }
    }
    
    func config(type:CommodityType, price:Float) {
        self.type = type.managedType()
        self.userPrice = price
        self.timestamp = NSDate().timeIntervalSince1970
    }
}
