//
//  Coin.swift
//  
//
//  Created by Zachary Ullevig on 5/8/16.
//
//

import Foundation
import CoreData

@objc(Coin)
class Coin: NSManagedObject {

// Insert code here to add functionality to your managed object subclass
    // https://secure.wikimedia.org/wikipedia/en/wiki/Troy_ounce
    let gramPerTroyOz:Float = 31.1034768
    let troyOzPerGram:Float = 1.0 / 31.1034768
    
    // TODO: do real errors
    enum AgError: ErrorType {
        case AgGenericError
    }

    func load(json:NSDictionary, coinGroup:CoinGroup) {
        //print("fromDictionary: \n\(json)")
        
        if let active: AnyObject = json["@active"] {
            self.active = NSNumber(integer:active.integerValue)
        }
        
        if let idValue: AnyObject = json["@id"] {
            self.coinId = NSNumber(integer:idValue.integerValue)
        }
        
        if let coinWeight: AnyObject = json["@coinWeight"] {
            self.coinWeight = NSNumber(float:coinWeight.floatValue)
        }
        
        if let firstYear: AnyObject = json["@firstYear"] {
            self.firstYear = NSNumber(integer:firstYear.integerValue)
        }
        
        if let lastYear: AnyObject = json["@lastYear"] {
            self.lastYear = NSNumber(integer:lastYear.integerValue)
        }
        
        if let silverFraction: AnyObject = json["@silverFraction"] {
            self.silverFraction = NSNumber(float:silverFraction.floatValue)
        }
        
        if let goldFraction: AnyObject = json["@goldFraction"] {
            self.goldFraction = NSNumber(float:goldFraction.floatValue)
        }
        
        if let name: String = json["@name"] as? String {
            self.name = name
        }
        
        if let image: String = json["@image"] as? String {
            self.image = image
        }
        
        self.coinGroup = coinGroup
        
        
        if let issuerIDNumber: AnyObject = json["@issuer"] {
            let issuerID = issuerIDNumber.integerValue
            let predicateString = "issuerID == \(issuerID)"
            let fetchRequest = NSFetchRequest(entityName: "Issuer")
            
            // Set the predicate on the fetch request
            let predicate = NSPredicate(format: predicateString)
            fetchRequest.predicate = predicate
            
            do {
                guard let fetchResults = try managedObjectContext?.executeFetchRequest(fetchRequest) as? [Issuer] else {
                    // TODO: throw a real error
                    throw AgError.AgGenericError
                }
                if fetchResults.count > 0 {
                    let issuer = fetchResults[0]
                    self.issuer = issuer
                }
            }
            catch {
                // TODO: handle error condition
            }
        }
    }
    
    func gramsToTroyOz(grams:Float) -> Float {
        return grams * self.troyOzPerGram
    }
    
    
    func grams(type:CommodityType) -> Float {
        switch type {
        case .Gold:
            return self.coinWeight.floatValue * self.goldFraction.floatValue
            
        case .Silver:
            return self.coinWeight.floatValue * self.silverFraction.floatValue
            
        default:
            // CommodityType not yet supported
            return 0
        }
    }
    
    func troyOz(type:CommodityType) -> Float {
        return self.gramsToTroyOz(self.grams(type))
    }
}
