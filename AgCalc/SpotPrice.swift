//
//  SpotPrice.swift
//  
//
//  Created by Zachary Ullevig on 5/8/16.
//
//

import Foundation
import CoreData


class SpotPrice: CommodityPrice {

// Insert code here to add functionality to your managed object subclass

    override var price: Float {
        get {
            var result:Float = 0
            switch UserSettings.sharedInstance.priceUpdateMode {
            case .SpotBid:
                result = self.bid
                
            case .SpotAsk:
                result = self.ask
                
            case .SpotMid:
                result = (self.ask + self.bid) / 2
                
            default:
                result = (self.ask + self.bid) / 2
            }
            
            return result
        }
    }
    

    func config(type:CommodityType, timestamp:NSTimeInterval, ask:Float, bid:Float) {
        self.type = type.managedType()
        self.ask = ask
        self.bid = bid
        self.timestamp = timestamp
    }

}
