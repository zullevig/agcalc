//
//  FixedPriceTableViewCell.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 5/2/16.
//  Copyright © 2016 Zachary Ullevig. All rights reserved.
//

import UIKit

class FixedPriceTableViewCell: UITableViewCell {

    @IBOutlet weak var metalNameLabel: UILabel!
    @IBOutlet weak var amPriceLabel: UILabel!
    @IBOutlet weak var pmPriceLabel: UILabel!
    
    var commodity:CommodityType? = nil {
        didSet {
            if let commodityType = self.commodity {
                metalNameLabel.text = commodityType.name()
                amPriceLabel.text = ""
                pmPriceLabel.text = ""
                
                if let spotPrices = UserSettings.sharedInstance.londonFixedPrices, let price = spotPrices[commodityType] {
                    if commodityType == .Silver {
                        amPriceLabel.text = String(format:"$%.2f", price.dayPrice)
                    }
                    else {
                        amPriceLabel.text = String(format:"$%.2f", price.amPrice)
                        pmPriceLabel.text = String(format:"$%.2f", price.pmPrice)
                    }
                }
            }
        }
    }
}
