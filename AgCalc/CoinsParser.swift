//
//  CoinsParser.swift
//  AgCalc
//
//  Created by Zachary Ullevig on 12/7/14.
//  Copyright (c) 2014 Zachary Ullevig. All rights reserved.
//

import Foundation
import CoreData


class CoinsParser {

    // TODO: do real errors
    enum AgError: ErrorType {
        case AgGenericError
    }

    // NOTE: passing in existing data arrays is a hack around a Swift bug failing to access the appDelegate global
    func loadCoinsJSON(managedContext:NSManagedObjectContext, existingIssuers:[Issuer]?, existingGroups:[CoinGroup]?) -> (issuers:[Issuer], groups:[CoinGroup]) {
        var resultingIssuers:[Issuer] = []
        var resultingGroups:[CoinGroup] = []
        
        if let existing = existingIssuers {
            resultingIssuers = existing
        }
        if let existing = existingGroups {
            resultingGroups = existing
        }
        
        if let path = NSBundle.mainBundle().pathForResource("Coins", ofType: "json"), let jsonData = NSData(contentsOfFile: path) {
            do {
                guard let jsonResult = try NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary else {
                    throw AgError.AgGenericError
                }
                // Parse Coin Issuers
                if let issuers:NSArray = jsonResult["issuers"] as? NSArray {
                    for item in issuers {
                        if let issuerDictionary = item as? NSDictionary {
                            
                            if let idValue: AnyObject = issuerDictionary["@id"] {
                                let filteredIssuers:[Issuer]? = resultingIssuers.filter({$0.issuerID == idValue.integerValue})
                                if filteredIssuers?.count == 0 {
                                    if let issuer = NSEntityDescription.insertNewObjectForEntityForName("Issuer", inManagedObjectContext: managedContext) as? Issuer {
                                        issuer.load(issuerDictionary)
                                        resultingIssuers.append(issuer)
                                    }
                                }
                                else {
                                    // TODO: update existing issuer's values
                                }
                            }
                        }
                    }
                }

                // Parse Coin Groups
                if let groups:NSArray = jsonResult["coinGroups"] as? NSArray {
                    for item in groups {
                        if let groupDictionary = item as? NSDictionary {
                            
                            if let idValue: AnyObject = groupDictionary["@id"] {
                                let filteredGroups:[CoinGroup]? = resultingGroups.filter({$0.groupId == idValue.integerValue})
                                if filteredGroups?.count == 0 {
                                    if let group:CoinGroup = NSEntityDescription.insertNewObjectForEntityForName("CoinGroup", inManagedObjectContext: managedContext) as? CoinGroup {
                                        group.load(groupDictionary)
                                        resultingGroups.append(group)
                                    }
                                }
                                else {
                                    // TODO: update existing group's values
                                    
                                    // load new coins
                                    if let group = filteredGroups?.first, let coinsJSONArray: NSArray = groupDictionary["coins"] as? NSArray, let coins = group.coins {
                                        group.loadCoins(coinsJSONArray, existingCoins: coins.allObjects as? [Coin])
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch {
                
            }
        }
        
        return (resultingIssuers, resultingGroups)
    }
    
}