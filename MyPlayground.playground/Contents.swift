//: Playground - noun: a place where people can play

import UIKit

class MyClass {
    class var entityName: String {
        get {
            return String(self)
        }
    }
}

MyClass.entityName




NSDate().timeIntervalSince1970

var title:String? = nil
var nonnullString = title ?? "Default String"


let dateString:String = "April 4, 2016"
var desc = "Call starting soon: \(title ?? "Default String") starts at \(dateString)"


var str = "Hello, playground"
str.characters.endIndex
let steps:Int = 300

// TODO: move to shared library
extension String {
    func condenseWhitespace() -> String {
        let components = self.componentsSeparatedByCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).filter{$0.isEmpty == false}
        return components.joinWithSeparator(" ")
    }
}



str = "Kitco               Inc. Text Only Market Page London Fix          GOLD          SILVER       PLATINUM           PALLADIUM New York Spot Price Gold         1216.20     1217.20     -3.40  -0.28% 1215.60  1224.50  Asia / Europe Spot Price ver       15.20         15.30     -0.01  -   File created at 12:40:30 on Fri March 25 2016"

let fixStr = str.condenseWhitespace()
fixStr


let londonBlock:String?
let londonSplit = str.componentsSeparatedByString("London")
londonSplit
if londonSplit.count > 1 {
    londonBlock = londonSplit[1]
}

let nyBlock:String?
let nySplit = str.componentsSeparatedByString("New York")
if nySplit.count > 1 {
    nyBlock = nySplit[1]
}


var strRange:Range<String.Index>? = nil
if var startRange = str.rangeOfString("play") {


    let fullName = "First Last"
    let fullNameArr = fullName.characters.split{$0 == " "}.map(String.init)
    fullNameArr[0] // First
    fullNameArr[1] // Last
    
    
//    if str.characters.endIndex < steps {
//        
//    }
    
    startRange.endIndex = str.characters.endIndex
    
    
    strRange = startRange.startIndex ..< startRange.startIndex.advancedBy(5)
}
