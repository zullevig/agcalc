# README #

What is this coin worth. Trade with value.

### What is it: ###

This is the source code for the iOS version of the [Silver Calculator App](http://whatisthiscoinworth.com)

Do you have any U.S., Canadian, Mexican, or French coins? Some of those coins are up to 100% silver in weight and now sell for up to 30 U.S. dollars.

This calculator will help you convert between silver pieces, U.S. dollars, Euros, Canadian dollars, Mexican pesos, and Bitcoin.

Find out the prices of bullion pieces and "junk silver" coins from various mints including:

* pre-1965 US dimes, quarters, half dollars, and dollars
* 1920-1967 Canadian dimes, quarters, half dollars, and dollars;
* American Silver Eagles;
* Canadian Maple Leafs;
* Mexican Libertades; and,
* Shire Silver, and more


